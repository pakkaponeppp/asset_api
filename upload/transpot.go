package upload

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/acoshift/hrpc/v3"
	"gitlab.com/pakkaponeppp/asset_api/auth"
)

type errorResponse struct {
	Error string `json:"error"`
}

var (
	errMethodNotAllowed      = errors.New("auth: method not allowed")
	errForbidden             = errors.New("auth: forbidden")
	errMultiToken            = errors.New("auth: MultiToken")
	errBadRequest            = errors.New("auth: bad request body")
	errUnauthorized          = errors.New("Unauthorized")
	errRequestEntityTooLarge = errors.New("request body is too large")
)

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Content-Type", "application/json")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

}

// MakeHandler for ticket domain
func MakeHandler(s Service) http.Handler {

	m := hrpc.Manager{
		Validate:     true,
		Decoder:      requestDecoder,
		Encoder:      responseEncoder,
		ErrorEncoder: errorEncoder,
	}
	mux := http.NewServeMux()
	mux.Handle("/asset", m.Handler(MakeUploadImage(s)))

	return (mux)
}
func mustLogin(s Service) func(http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			user := auth.GetUserAccess(r.Context())
			if user == nil {
				errorEncoder(w, r, auth.ErrTokenNotFound)
				return
			}
			enableCors(&w)
			h.ServeHTTP(w, r)

		})
	}
}

func jsonDecoder(r *http.Request, v interface{}) error {

	return json.NewDecoder(r.Body).Decode(v)
}

func jsonEncoder(w http.ResponseWriter, status int, v interface{}) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(status)
	return json.NewEncoder(w).Encode(v)
}

func responseEncoder(w http.ResponseWriter, r *http.Request, v interface{}) {
	fmt.Println("v =", v)
	jsonEncoder(w, http.StatusOK, v)
}

func errorEncoder(w http.ResponseWriter, r *http.Request, err error) {
	encoder := jsonEncoder
	status := http.StatusInternalServerError
	fmt.Println("auth transport ,check error : ", err.Error())
	if err == errUnauthorized {
		fmt.Println("year equal to ...")
	} else {
		fmt.Println("not ...")
	}

	switch err {
	case errMethodNotAllowed:
		status = http.StatusMethodNotAllowed
	case errRequestEntityTooLarge:
		status = http.StatusRequestEntityTooLarge
	case errForbidden:
		status = http.StatusForbidden
	case errBadRequest:
		status = http.StatusBadRequest
	case errors.New("Unauthorized"):
		status = http.StatusUnauthorized
	}

	if r.Method == http.MethodOptions {
		encoder(w, http.StatusNoContent, nil)
	} else {
		encoder(w, status, &errorResponse{err.Error()})
	}
}

func requestDecoder(r *http.Request, v interface{}) error {

	if r.Method != http.MethodPost {
		return errMethodNotAllowed
	}
	fmt.Println("v =", r)
	return jsonDecoder(r, v)
}
