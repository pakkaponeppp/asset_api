package upload

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"time"

	"cloud.google.com/go/storage"
	"github.com/chai2010/webp"
	"github.com/disintegration/imaging"
	"github.com/google/uuid"
	"github.com/h2non/filetype/matchers"
)

//Service asset uploadimage interface
type Service interface {
	UploadImageService(image multipart.File, Type string, handle *multipart.FileHeader) (interface{}, error)
}

type uploadservice struct {
	client     *storage.Client
	repo       Repository
	basketName string
	baseURL    string
}

var loc, _ = time.LoadLocation("Asia/Bangkok")
var name = time.Now().In(loc).Format("2006-01-02")
var stack = &Stack{
	Time:  name,
	Stack: 1,
}

// NewService upload image service
func NewService(client *storage.Client, upload Repository, basketName string) (Service, error) {
	s := uploadservice{client, upload, basketName, "https://firebasestorage.googleapis.com/v0/b/np-storage-it.appspot.com/o"}
	return &s, nil
}

func checkimage(image []byte) string {

	if matchers.Jpeg(image) {
		return "jpeg"
	} else if matchers.Png(image) {
		return "png"
	} else if matchers.Webp(image) {
		return "webp"
	} else {
		return ""
	}
}

func (s *uploadservice) UploadImageService(image multipart.File, Type string, handle *multipart.FileHeader) (interface{}, error) {

	resp, err := s.UploadImage(image, Type, handle)
	if err != nil {
		return nil, err
	}
	return resp, nil

}

func (s *uploadservice) UploadImage(image multipart.File, Type string, handle *multipart.FileHeader) (interface{}, error) {
	ctx := context.Background()

	var filename, typefile string = "", ""

	loc, _ := time.LoadLocation("Asia/Bangkok")
	id := uuid.New()
	name := time.Now().In(loc).Format("02-01-2006:15:04:05")
	if stack.Time == name {
		stack.Stack++
		stack.Time = name

	} else {
		stack.Stack = 1
		stack.Time = name
	}

	filename = "IT_ASSET_" + stack.Time + "-" + fmt.Sprint(stack.Stack)

	buf := bytes.NewBuffer(nil)
	if _, err := io.Copy(buf, image); err != nil {
		return nil, err
	}

	typefile = checkimage(buf.Bytes())

	bufResize := bytes.NewBuffer(nil)

	if typefile == "jpeg" {
		Img, err := imaging.Decode(buf, imaging.AutoOrientation(true))
		if err != nil {
			return nil, err
		}
		imaging.Encode(bufResize, imaging.Resize(Img, 0, 500, imaging.Lanczos), imaging.JPEG)
	} else if typefile == "png" {
		Img, err := imaging.Decode(buf, imaging.AutoOrientation(true))
		if err != nil {
			return nil, err
		}
		imaging.Encode(bufResize, imaging.Resize(Img, 0, 500, imaging.Lanczos), imaging.PNG)
	} else if typefile == "webp" {
		img, err := webp.Decode(buf)
		if err != nil {
			return nil, err
		}
		webp.Encode(bufResize, imaging.Resize(img, 0, 500, imaging.Lanczos), nil)
	} else if typefile == "" {
		err := errors.New("รูปภาพไม่ถูกต้อง")
		return nil, err
	}

	err := image.Close()
	if err != nil {
		return nil, err
	}

	bh := s.client.Bucket(s.basketName)
	if _, err := bh.Attrs(ctx); err != nil {
		return nil, err
	}

	obj := bh.Object(filename)
	w := obj.NewWriter(ctx)
	w.ObjectAttrs.Metadata = map[string]string{"firebaseStorageDownloadTokens": id.String()}

	defer w.Close()

	w.ACL = append(w.ACL, storage.ACLRule{Entity: storage.AllUsers, Role: storage.RoleReader})
	w.CacheControl = "public, max-age=31536000"
	if _, err := io.Copy(w, bufResize); err != nil {
		return nil, nil
	}
	asset := ModelUploadImage{
		Type: typefile,
		Name: filename,
		URL:  s.baseURL + "/" + filename + "?alt=media",
	}
	if err := w.Close(); err != nil {
		return nil, err
	}

	return asset, nil

}
