package upload

import (
	"context"
	"errors"
	"fmt"
	"net/http"
)

var (
	errBodyTooLarge = errors.New("ขนาดไฟล์ใหญ่เกินไป")
)

//MakeUploadImage Endpoint
func MakeUploadImage(s Service) interface{} {

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, r *http.Request) (*response, error) {

		// if r.Method != http.MethodPost {
		// 	return nil, errMethodNotAllowed
		// }

		const MaxSize int64 = 10 * 1024 * 1024

		r.Body = http.MaxBytesReader(nil, r.Body, MaxSize) // 10 Mb

		file, handler, err := r.FormFile("file")

		Type := r.FormValue("type")

		if err != nil {
			return &response{Result: "false", Message: "ขนาดไฟล์ไม่ถูกต้อง"}, nil
		}

		resp, err := s.UploadImageService(file, Type, handler)
		if err != nil {
			fmt.Println("อัพโหลดไม่สำเร็จ")
			return &response{Result: "false", Message: err.Error()}, nil
		}
		defer file.Close()

		fmt.Printf("Uploaded File: %+v\n ", handler.Header)

		return &response{
			Result:  "success",
			Message: "อัพโหลดสำเร็จ",
			Data:    resp,
		}, nil

	}
}
