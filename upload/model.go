package upload

//ModelUploadImage Model
type ModelUploadImage struct {
	Type string `json:"type"`
	Name string `json:"name"`
	URL  string `json:"url"`
}

//Stack Model
type Stack struct {
	Time  string `json:"time"`
	Stack int64  `json:"stack"`
}
