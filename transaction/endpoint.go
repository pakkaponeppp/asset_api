package transaction

import (
	"context"
)

//MakeCreateRequest Endpoint
func makeCreateRequestByPass(s Service) interface{} {

	type request ModelTransactionBypass

	type response struct {
		Result  string `json:"response"`
		Message string `json:"message"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		model := ModelTransactionBypass{
			Name:           req.Name,
			PositionName:   req.PositionName,
			AssetItemID:    req.AssetItemID,
			ItemsQty:       1,
			MarkName:       req.MarkName,
			Lat:            req.Lat,
			Lng:            req.Lng,
			Message:        req.Message,
			ConfirmedImage: req.ConfirmedImage,
		}

		_, err := s.CreateRequestServiceBypass(&model)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "เพิ่มข้อมูลสำเร็จ",
		}, nil
	}
}

//MakeCreateRequest Endpoint
func makeCreateRequest(s Service) interface{} {

	type request ModelTransaction

	type response struct {
		Result  string `json:"response"`
		Message string `json:"message"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		model := ModelTransaction{
			UserCode: req.UserCode,
			AssetID:  req.AssetID,
			ItemsQty: req.ItemsQty,
			MarkName: req.MarkName,
			Lat:      req.Lat,
			Lng:      req.Lng,
			Message:  req.Message,
		}

		_, err := s.CreateRequestService(&model)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "เพิ่มข้อมูลสำเร็จ",
		}, nil
	}
}

//MakeCreateRequest Endpoint
func makeCreateRepair(s Service) interface{} {

	type request ModelTransaction

	type response struct {
		Result  string `json:"response"`
		Message string `json:"message"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		model := ModelTransaction{
			UserCode:        req.UserCode,
			AssetID:         req.AssetID,
			AssetItemID:     req.AssetItemID,
			LocationPinedID: req.LocationPinedID,
			Reporter:        req.Reporter,
			Message:         req.Message,
		}

		_, err := s.CreateRepairService(&model)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "เพิ่มข้อมูลสำเร็จ",
		}, nil
	}
}

//MakeCreateRequest Endpoint
func makeCreateRepairBypass(s Service) interface{} {

	type request ModelTransaction

	type response struct {
		Result  string `json:"response"`
		Message string `json:"message"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		model := ModelTransaction{
			UserCode:        req.UserCode,
			AssetID:         req.AssetID,
			AssetItemID:     req.AssetItemID,
			LocationPinedID: req.LocationPinedID,
			Reporter:        req.Reporter,
			Message:         req.Message,
		}

		_, err := s.CreateRepairService(&model)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "เพิ่มข้อมูลสำเร็จ",
		}, nil
	}
}

//MakeCreateRequest Endpoint
func makeCreateReturn(s Service) interface{} {

	type request struct {
		Usercode    string `json:"user_code"`
		AssetItemID int64  `json:"asset_item_id"`
		Message     string `json:"message"`
	}

	type response struct {
		Result  string `json:"response"`
		Message string `json:"message"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		_, err := s.CreateReturnService(req.Usercode, req.AssetItemID, req.Message)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "เพิ่มข้อมูลสำเร็จ",
		}, nil
	}
}

//MakeCreateRequest Endpoint
func makeCreateReturnBypass(s Service) interface{} {

	type request struct {
		Name         string `json:"name"`
		PositionName string `json:"position_name"`
		AssetItemID  int64  `json:"asset_item_id"`
		Message      string `json:"message"`
	}

	type response struct {
		Result  string `json:"response"`
		Message string `json:"message"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		_, err := s.CreateReturnServiceBypass(req.Name, req.PositionName, req.AssetItemID, req.Message)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "เพิ่มข้อมูลสำเร็จ",
		}, nil
	}
}

//MakeCreateRequest Endpoint
func makeCreateSwitchUserBypass(s Service) interface{} {

	type request ModelTransactionBypass

	type response struct {
		Result  string `json:"response"`
		Message string `json:"message"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		model := ModelTransactionBypass{
			Name:           req.Name,
			PositionName:   req.PositionName,
			AssetItemID:    req.AssetItemID,
			ItemsQty:       1,
			MarkName:       req.MarkName,
			Lat:            req.Lat,
			Lng:            req.Lng,
			Message:        req.Message,
			ConfirmedImage: req.ConfirmedImage,
		}

		_, err := s.CreateSwitchUserBypassService(&model)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}

		return &response{
			Result:  "success",
			Message: "เพิ่มข้อมูลสำเร็จ",
		}, nil
	}
}

//MakeCreateRequest Endpoint
func makeAcceptRequest(s Service) interface{} {

	type request struct {
		ID      int64   `json:"id"`
		AssetID int64   `json:"asset_id"`
		Items   []int64 `json:"items"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.AcceptRequestService(req.ID, req.AssetID, req.Items)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "เพิ่มข้อมูลสำเร็จ",
			Data:    resp,
		}, nil
	}
}

//MakeCreateRequest Endpoint
func makeAcceptReturn(s Service) interface{} {

	type request struct {
		ID          int64 `json:"id"`
		AssetID     int64 `json:"asset_id"`
		AssetItemID int64 `json:"asset_item_id"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.AcceptReturnService(req.ID, req.AssetID, req.AssetItemID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "เพิ่มข้อมูลสำเร็จ",
			Data:    resp,
		}, nil
	}
}

//makeQRcodeID Endpoint
func makeQRcodeID(s Service) interface{} {

	type request struct {
		Items string `json:"items"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.QRcodeIDService(req.Items)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "เพิ่มข้อมูลสำเร็จ",
			Data:    resp,
		}, nil
	}
}

//MakeCreateRequest Endpoint
func makeDeclineRequest(s Service) interface{} {

	type request struct {
		ID           int64  `json:"id"`
		ErrorMeseage string `json:"error_message"`
	}

	type response struct {
		Result  string `json:"response"`
		Message string `json:"message"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		_, err := s.DeclineRequestService(req.ID, req.ErrorMeseage)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "เพิ่มข้อมูลสำเร็จ",
		}, nil
	}
}

//makeGetTransactionRequestEndpoint Endpoint
func makeListTransactionRequestEndpoint(s Service) interface{} {

	type request struct {
		ID        int64  `json:"id"`
		ProcessID int64  `json:"process_id"`
		UserCode  string `json:"user_code"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.ListTransactionService(req.ID, req.ProcessID, req.UserCode)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}

//makeGetTransactionRequestEndpoint Endpoint
func makeGetTransactionRequestEndpoint(s Service) interface{} {

	type request struct{ ID int64 }

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.GetTransactionRequestService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}

//makeGetTransactionRequestEndpoint Endpoint
func makeGetQtyTransaction(s Service) interface{} {

	type request struct {
		Usercode string `json:"id"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.GetQtyTransactionService(req.Usercode)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}

//makeGetDetailTransaction Endpoint
func makeGetDetailTransaction(s Service) interface{} {

	type request struct{ ID int64 }

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.GetDetailTransactionService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}

//makeGetDetailTransaction Endpoint
func makeGetAssetItemRepairEndpoint(s Service) interface{} {

	type request struct{ ID int64 }

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
		Status  int64       `json:"status"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, status, err := s.GetAssetItemRepairService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error(), Status: status}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
			Status:  status,
		}, nil
	}
}

//makeGetDetailTransaction Endpoint
func makeListItemsUser(s Service) interface{} {

	type request struct {
		Usercode string `json:"user_code"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.ListItemsUserService(req.Usercode)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}

//makeCancelTransactionUser Endpoint
func makeCancelTransactionUser(s Service) interface{} {

	type request struct{ ID int64 }

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.CancelTransactionUserService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}

//makeCancelTransactionUser Endpoint
func makeCancelReturnUser(s Service) interface{} {

	type request struct {
		ID          int64
		AssetItemID int64 `json:"asset_item_id"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.CancelAllUserService(req.ID, req.AssetItemID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}

//makeCancelTransactionUser Endpoint
func makeCancelReportUser(s Service) interface{} {

	type request struct {
		ID          int64
		AssetItemID int64 `json:"asset_item_id"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.CancelReportUserService(req.ID, req.AssetItemID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}

//makeCancelTransactionUser Endpoint
func makeManageRepair(s Service) interface{} {

	type request struct {
		ID          int64
		Status      int64  `json:"status"`
		AssetItemID int64  `json:"asset_item_id"`
		Message     string `json:"response_message"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.ManageRepairService(req.ID, req.Status, req.AssetItemID, req.Message)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}
