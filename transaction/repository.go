package transaction

//Repository transaction
type Repository interface {
	CreateRequestRepo(model *ModelTransaction) (interface{}, error)
	CreateRequestBypassRepo(model *ModelTransactionBypass) (interface{}, error)
	CreateSwitchUserBypassRepo(model *ModelTransactionBypass) (interface{}, error)
	CreateRepairRepo(model *ModelTransaction) (interface{}, error)
	CreateReturnRepo(Usercode string, AssetItemID int64, Message string) (interface{}, error)
	CreateReturnRepoBypass(Name string, PositionName string, AssetItemID int64, Message string) (interface{}, error)
	CancelAllUserRepo(ID int64, AssetItemID int64) (interface{}, error)
	CancelReportUserRepo(ID int64, AssetItemID int64) (interface{}, error)
	AcceptRequestRepo(ID int64, AssetID int64, Items []int64) (interface{}, error)
	AcceptReturnRepo(ID int64, AssetID int64, AssetItemID int64) (interface{}, error)
	QRcodeIDRepo(Items string) (interface{}, error)
	DeclineRequestRepo(ID int64, ErrorMeseage string) (interface{}, error)
	ListTransactionRepo(ID int64, ProcessID int64, UserCode string) (interface{}, error)
	ListItemsUserRepo(Usercode string) (interface{}, error)
	CancelTransactionUserRepo(ID int64) (interface{}, error)
	ManageRepairRepo(ID int64, Status int64, AssetItemID int64, Message string) (interface{}, error)
	GetTransactionRequestRepo(ID int64) (interface{}, error)
	GetQtyTransactionRepo(Usercode string) (interface{}, error)
	GetDetailTransactionRepo(ID int64) (interface{}, error)
	GetAssetItemRepairRepo(ID int64) (interface{}, int64, error)
}
