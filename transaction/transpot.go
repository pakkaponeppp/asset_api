package transaction

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/acoshift/hrpc/v3"
	"gitlab.com/pakkaponeppp/asset_api/auth"
)

type errorResponse struct {
	Response string `json:"response"`
	Message  string `json:"message"`
}

var (
	errMethodNotAllowed = errors.New("invoice: method not allowed")
)

// MakeHandler for ticket domain
func MakeHandler(s Service) http.Handler {

	m := hrpc.Manager{
		Validate:     true,
		Decoder:      requestDecoder,
		Encoder:      responseEncoder,
		ErrorEncoder: errorEncoder,
	}
	mux := http.NewServeMux()
	mux.Handle("/get/repair", m.Handler(makeGetAssetItemRepairEndpoint(s)))
	mux.Handle("/qrcode", m.Handler(makeQRcodeID(s)))
	mux.Handle("/create/request/bp", m.Handler(makeCreateRequestByPass(s)))
	mux.Handle("/create/repair/bp", m.Handler(makeCreateRepairBypass(s)))
	mux.Handle("/create/return/bp", m.Handler(makeCreateReturnBypass(s)))
	mux.Handle("/create/change/bp", m.Handler(makeCreateSwitchUserBypass(s)))
	mux.Handle("/", m.Handler(MakeAccountHandler(s)))

	return (mux)
}

// MakeAccountHandler require token
func MakeAccountHandler(s Service) http.Handler {
	m := hrpc.Manager{
		Validate:     true,
		Decoder:      requestDecoder,
		Encoder:      responseEncoder,
		ErrorEncoder: errorEncoder,
	}
	mux := http.NewServeMux()

	//CREATE//
	mux.Handle("/create/request", m.Handler(makeCreateRequest(s)))
	mux.Handle("/create/repair", m.Handler(makeCreateRepair(s)))
	mux.Handle("/create/return", m.Handler(makeCreateReturn(s)))
	mux.Handle("/accept/request", m.Handler(makeAcceptRequest(s)))
	mux.Handle("/accept/return", m.Handler(makeAcceptReturn(s)))
	mux.Handle("/decline/request", m.Handler(makeDeclineRequest(s)))
	mux.Handle("/list/request", m.Handler(makeListTransactionRequestEndpoint(s)))
	mux.Handle("/list/request/user", m.Handler(makeListItemsUser(s)))
	mux.Handle("/get/request", m.Handler(makeGetTransactionRequestEndpoint(s)))
	mux.Handle("/get/qtytransaction", m.Handler(makeGetQtyTransaction(s)))
	mux.Handle("/get/transaction", m.Handler(makeGetDetailTransaction(s)))

	mux.Handle("/cancel/request", m.Handler(makeCancelTransactionUser(s)))
	mux.Handle("/cancel/return", m.Handler(makeCancelReturnUser(s)))
	mux.Handle("/cancel/report", m.Handler(makeCancelReportUser(s)))
	mux.Handle("/manage/repair", m.Handler(makeManageRepair(s)))

	return mustLogin(s)(mux)
}
func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Content-Type", "application/json")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

}
func mustLogin(s Service) func(http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			user := auth.GetUserAccess(r.Context())
			if user == nil {
				errorEncoder(w, r, auth.ErrTokenNotFound)
				return
			}
			enableCors(&w)
			h.ServeHTTP(w, r)

		})
	}
}

func jsonDecoder(r *http.Request, v interface{}) error {

	return json.NewDecoder(r.Body).Decode(v)
}

func jsonEncoder(w http.ResponseWriter, status int, v interface{}) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(status)

	return json.NewEncoder(w).Encode(v)
}

func responseEncoder(w http.ResponseWriter, r *http.Request, v interface{}) {
	fmt.Println("v =", v)

	jsonEncoder(w, http.StatusOK, v)
}

func errorEncoder(w http.ResponseWriter, r *http.Request, err error) {
	encoder := jsonEncoder

	var status = http.StatusOK

	fmt.Println("Error Encode = ", err.Error())
	switch err.Error() {

	default:
		status = http.StatusOK
	}

	encoder(w, status, &errorResponse{"false", err.Error()})
}

func requestDecoder(r *http.Request, v interface{}) error {
	if r.Method != http.MethodPost {
		return errMethodNotAllowed
	}
	fmt.Println("v =", r)
	return jsonDecoder(r, v)
}
