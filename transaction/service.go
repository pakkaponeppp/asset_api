package transaction

//Service transaction
type Service interface {
	CreateRequestService(model *ModelTransaction) (interface{}, error)
	CreateRequestServiceBypass(model *ModelTransactionBypass) (interface{}, error)
	CreateRepairService(model *ModelTransaction) (interface{}, error)
	CreateReturnService(Usercode string, AssetItemID int64, Message string) (interface{}, error)
	CreateReturnServiceBypass(Name string, PositionName string, AssetItemID int64, Message string) (interface{}, error)
	CreateSwitchUserBypassService(model *ModelTransactionBypass) (interface{}, error)
	CancelAllUserService(ID int64, AssetItemID int64) (interface{}, error)
	CancelReportUserService(ID int64, AssetItemID int64) (interface{}, error)
	AcceptRequestService(ID int64, AssetID int64, Items []int64) (interface{}, error)
	AcceptReturnService(ID int64, AssetID int64, AssetItemID int64) (interface{}, error)
	QRcodeIDService(Items string) (interface{}, error)
	DeclineRequestService(ID int64, ErrorMeseage string) (interface{}, error)
	ListTransactionService(ID int64, ProcessID int64, UserCode string) (interface{}, error)
	ListItemsUserService(Usercode string) (interface{}, error)
	GetTransactionRequestService(ID int64) (interface{}, error)
	GetQtyTransactionService(Usercode string) (interface{}, error)
	GetDetailTransactionService(ID int64) (interface{}, error)
	GetAssetItemRepairService(ID int64) (interface{}, int64, error)
	CancelTransactionUserService(ID int64) (interface{}, error)
	ManageRepairService(ID int64, Status int64, AssetItemID int64, Message string) (interface{}, error)
}

type service struct {
	repo Repository
}

// NewService creates new auth service
func NewService(repo Repository) (Service, error) {
	s := service{repo}
	return &s, nil
}

func (s *service) CreateRequestService(model *ModelTransaction) (interface{}, error) {

	_, err := s.repo.CreateRequestRepo(model)
	if err != nil {
		return nil, err
	}
	return nil, nil

}
func (s *service) CreateRequestServiceBypass(model *ModelTransactionBypass) (interface{}, error) {

	_, err := s.repo.CreateRequestBypassRepo(model)
	if err != nil {
		return nil, err
	}
	return nil, nil

}
func (s *service) CreateSwitchUserBypassService(model *ModelTransactionBypass) (interface{}, error) {

	_, err := s.repo.CreateSwitchUserBypassRepo(model)
	if err != nil {
		return nil, err
	}
	return nil, nil

}
func (s *service) CreateRepairService(model *ModelTransaction) (interface{}, error) {

	_, err := s.repo.CreateRepairRepo(model)
	if err != nil {
		return nil, err
	}
	return nil, nil

}
func (s *service) CreateReturnService(Usercode string, AssetItemID int64, Message string) (interface{}, error) {

	_, err := s.repo.CreateReturnRepo(Usercode, AssetItemID, Message)
	if err != nil {
		return nil, err
	}
	return nil, nil

}
func (s *service) CreateReturnServiceBypass(Name string, PositionName string, AssetItemID int64, Message string) (interface{}, error) {

	_, err := s.repo.CreateReturnRepoBypass(Name, PositionName, AssetItemID, Message)
	if err != nil {
		return nil, err
	}
	return nil, nil

}
func (s *service) AcceptRequestService(ID int64, AssetID int64, Items []int64) (interface{}, error) {

	resp, err := s.repo.AcceptRequestRepo(ID, AssetID, Items)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (s *service) AcceptReturnService(ID int64, AssetID int64, AssetItemID int64) (interface{}, error) {

	resp, err := s.repo.AcceptReturnRepo(ID, AssetID, AssetItemID)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (s *service) QRcodeIDService(Items string) (interface{}, error) {

	resp, err := s.repo.QRcodeIDRepo(Items)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (s *service) DeclineRequestService(ID int64, ErrorMessage string) (interface{}, error) {

	_, err := s.repo.DeclineRequestRepo(ID, ErrorMessage)
	if err != nil {
		return nil, err
	}
	return nil, nil

}

func (s *service) ListTransactionService(ID int64, ProcessID int64, UserCode string) (interface{}, error) {

	resp, err := s.repo.ListTransactionRepo(ID, ProcessID, UserCode)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (s *service) GetTransactionRequestService(ID int64) (interface{}, error) {

	resp, err := s.repo.GetTransactionRequestRepo(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (s *service) GetQtyTransactionService(Usercode string) (interface{}, error) {

	resp, err := s.repo.GetQtyTransactionRepo(Usercode)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (s *service) GetDetailTransactionService(ID int64) (interface{}, error) {

	resp, err := s.repo.GetDetailTransactionRepo(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (s *service) GetAssetItemRepairService(ID int64) (interface{}, int64, error) {

	resp, status, err := s.repo.GetAssetItemRepairRepo(ID)
	if err != nil {
		return nil, 0, err
	}
	return resp, status, nil

}
func (s *service) ListItemsUserService(Usercode string) (interface{}, error) {

	resp, err := s.repo.ListItemsUserRepo(Usercode)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (s *service) CancelTransactionUserService(ID int64) (interface{}, error) {

	resp, err := s.repo.CancelTransactionUserRepo(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (s *service) CancelAllUserService(ID int64, AssetItemID int64) (interface{}, error) {

	resp, err := s.repo.CancelAllUserRepo(ID, AssetItemID)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (s *service) CancelReportUserService(ID int64, AssetItemID int64) (interface{}, error) {

	resp, err := s.repo.CancelReportUserRepo(ID, AssetItemID)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (s *service) ManageRepairService(ID int64, Status int64, AssetItemID int64, Message string) (interface{}, error) {

	resp, err := s.repo.ManageRepairRepo(ID, Status, AssetItemID, Message)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
