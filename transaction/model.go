package transaction

//ModelTransaction Model
type ModelTransaction struct {
	ID              int64   `json:"id,omitempty" db:"id"`
	UserCode        string  `json:"user_code,omitempty" db:"user_code"`
	MarkName        string  `json:"mark_name,omitempty" db:"mark_name"`
	AssetID         int64   `json:"asset_id,omitempty" db:"asset_id"`
	AssetItemID     int64   `json:"asset_item_id" db:"asset_item_id"`
	ItemsQty        int64   `json:"items_qty,omitempty" db:"items_qty"`
	ProcessID       int64   `json:"process_id,omitempty" db:"process_id"`
	LocationPinedID int64   `json:"location_pined_id,omitempty" db:"location_pined_id"`
	Message         string  `json:"message,omitempty" db:"message"`
	ResponseMessage string  `json:"response_message,omitempty" db:"response_message"`
	Lat             float64 `json:"lat,omitempty" db:"lat"`
	Lng             float64 `json:"lng,omitempty" db:"lng"`
	CreatedAt       string  `json:"created_at,omitempty" db:"created_at"`
	UpdatedAt       string  `json:"updated_at,omitempty" db:"updated_at"`
	NotifyStatus    int64   `json:"notify_status" db:"notify_status"`
	Reporter        string  `json:"reporter,omitempty" db:"reporter"`
}

//ModelTransactionBypass Model
type ModelTransactionBypass struct {
	ID              int64   `json:"id" db:"id"`
	UserCode        string  `json:"user_code" db:"user_code"`
	Name            string  `json:"name" db:"name"`
	PositionName    string  `json:"position_name" db:"position_name"`
	MarkName        string  `json:"mark_name" db:"mark_name"`
	AssetID         int64   `json:"asset_id" db:"asset_id"`
	AssetItemID     int64   `json:"asset_item_id" db:"asset_item_id"`
	ItemsQty        int64   `json:"items_qty" db:"items_qty"`
	ProcessID       int64   `json:"process_id" db:"process_id"`
	LocationPinedID int64   `json:"location_pined_id" db:"location_pined_id"`
	Message         string  `json:"message" db:"message"`
	ResponseMessage string  `json:"response_message" db:"response_message"`
	Lat             float64 `json:"lat" db:"lat"`
	Lng             float64 `json:"lng" db:"lng"`
	CreatedAt       string  `json:"created_at" db:"created_at"`
	UpdatedAt       string  `json:"updated_at" db:"updated_at"`
	NotifyStatus    int64   `json:"notify_status" db:"notify_status"`
	Reporter        string  `json:"reporter" db:"reporter"`
	ConfirmedImage  string  `json:"confirmed_image" db:"confirmed_image"`
}

//ModelTransactionList Model
type ModelTransactionList struct {
	ID                int64                `json:"id,omitempty" db:"id"`
	UserCode          string               `json:"user_code,omitempty" db:"user_code"`
	AssetID           int64                `json:"asset_id,omitempty" db:"asset_id"`
	AssetItemID       int64                `json:"asset_item_id,omitempty" db:"asset_item_id"`
	ItemsQty          int64                `json:"items_qty" db:"items_qty"`
	Items             []ModelAssetItem     `json:"items,omitempty"`
	AssetName         string               `json:"asset_name,omitempty" db:"asset_name"`
	Description       string               `json:"description,omitempty" db:"description"`
	WarrantyDate      string               `json:"warranty_date,omitempty" db:"warranty_date"`
	Name              string               `json:"name,omitempty" db:"name"`
	MarkName          string               `json:"mark_name,omitempty" db:"mark_name"`
	ProcessID         int64                `json:"process_id,omitempty" db:"process_id"`
	LocationPinedID   int64                `json:"location_pined_id,omitempty" db:"location_pined_id"`
	Message           string               `json:"message" db:"message"`
	ResponseMessage   string               `json:"response_message" db:"response_message"`
	CreatedAt         string               `json:"created_at,omitempty" db:"created_at"`
	UpdatedAt         string               `json:"updated_at,omitempty" db:"updated_at"`
	NotifyStatus      int64                `json:"notify_status" db:"notify_status"`
	TransactionStatus int64                `json:"transaction_status" db:"transaction_status"`
	Reporter          string               `json:"reporter,omitempty" db:"reporter"`
	ConfirmedImage    string               `json:"confirmed_image" db:"confirmed_image"`
	Position          []ModelLocationPined `json:"position,omitempty"`
}

//ModelLineNotify Model
type ModelLineNotify struct {
	ID              int64  `json:"id,omitempty" db:"id"`
	Name            string `json:"name,omitempty" db:"name"`
	AssetName       string `json:"asset_name,omitempty" db:"asset_name"`
	UserCode        string `json:"user_code,omitempty" db:"user_code"`
	URL             string `json:"url,omitempty" db:"url"`
	ItemsQty        int64  `json:"items_qty" db:"items_qty"`
	LocationPinedID int64  `json:"location_pined_id,omitempty" db:"location_pined_id"`
	ProcessID       int64  `json:"process_id,omitempty" db:"process_id"`
	Process         string `json:"process_name,omitempty" db:"process_name"`
	ResponseMessage string `json:"response_message,omitempty" db:"response_message"`
	CreatedAt       string `json:"created_at,omitempty" db:"created_at"`
	NotifyStatus    int64  `json:"notify_status" db:"notify_status"`
	Reporter        string `json:"reporter,omitempty" db:"reporter"`
}

//ModelGetDetail Model
type ModelGetDetail struct {
	ID              int64            `json:"id,omitempty" db:"id"`
	AssetID         int64            `json:"asset_id,omitempty" db:"asset_id"`
	AssetName       string           `json:"asset_name,omitempty" db:"asset_name"`
	ItemsQty        int64            `json:"items_qty" db:"items_qty"`
	Description     string           `json:"description,omitempty" db:"description"`
	Barcode         string           `json:"barcode,omitempty" db:"barcode"`
	CategoryName    string           `json:"category_name,omitempty" db:"category_name"`
	SubCategoryName string           `json:"sub_category_name,omitempty" db:"sub_category_name"`
	ProcessID       int64            `json:"process_id,omitempty" db:"process_id"`
	Message         string           `json:"message,omitempty" db:"message"`
	ResponseMessage string           `json:"response_message,omitempty" db:"response_message"`
	CreatedAt       string           `json:"created_at,omitempty" db:"created_at"`
	UpdatedAt       string           `json:"updated_at,omitempty" db:"updated_at"`
	URL             string           `json:"url,omitempty" db:"url"`
	ConfirmedImage  string           `json:"confirmed_image" db:"confirmed_image"`
	AssetItems      []ModelAssetItem `json:"items"`
}

//ModelUpdateReq Model
type ModelUpdateReq struct {
	ResponseMessage string `json:"response_message,omitempty" db:"response_message"`
}

//ModelLocationPined Model
type ModelLocationPined struct {
	Latitude   float64 `json:"lat,omitempty" db:"lat"`
	Longtitude float64 `json:"lng,omitempty" db:"lng"`
}

//ModelAssetItem Model
type ModelAssetItem struct {
	ID               int64                `json:"id" db:"id"`
	AssetItemID      int64                `json:"asset_item_id,omitempty" db:"asset_item_id"`
	SerialNumber     string               `json:"serial_number" db:"serial_number"`
	AssetID          int64                `json:"asset_id" db:"asset_id"`
	Status           int64                `json:"status" db:"status"`
	Available        int64                `json:"available" db:"available"`
	LocationID       int64                `json:"location_id" db:"location_id"`
	Detail           string               `json:"detail" db:"detail"`
	Contact          string               `json:"contact" db:"contact"`
	Conditions       int64                `json:"conditions" db:"conditions"`
	Active           int64                `json:"active" db:"active"`
	BuyingDate       string               `json:"buying_date" db:"buying_date"`
	Prices           float64              `json:"prices" db:"prices"`
	CalculatedPrices float64              `json:"calculated_prices" db:"calculated_prices"`
	StartedWarranty  string               `json:"started_warranty" db:"started_warranty"`
	WarrantyMonth    int64                `json:"warranty_month" db:"warranty_month"`
	WarrantyDate     string               `json:"warranty_date" db:"warranty_date"`
	CreatedAt        string               `json:"created_at" db:"created_at"`
	UpdatedAt        string               `json:"updated_at" db:"updated_at"`
	AssetName        string               `json:"asset_name,omitempty" db:"asset_name"`
	URL              string               `json:"url,omitempty" db:"url"`
	ImageURL         string               `json:"image_url,omitempty" db:"image_url"`
	Description      string               `json:"description,omitempty" db:"description"`
	CategoryName     string               `json:"category_name,omitempty" db:"category_name"`
	SubCategoryName  string               `json:"sub_category_name,omitempty" db:"sub_category_name"`
	Barcode          string               `json:"barcode,omitempty" db:"barcode"`
	LocationPinedID  int64                `json:"location_pined_id,omitempty" db:"location_pined_id"`
	Position         []ModelLocationPined `json:"position"`
}

//ModelStatusItem Model
type ModelStatusItem struct {
	ID          int64  `json:"id"`
	ProcessID   int64  `json:"process_id" db:"process_id"`
	ProcessName string `json:"process_name" db:"process_name"`
}

//ModelAssetItemRepair Model
type ModelAssetItemRepair struct {
	ID              int64                `json:"id" db:"id"`
	AssetID         int64                `json:"asset_id,omitempty" db:"asset_id"`
	AssetName       string               `json:"asset_name" db:"asset_name"`
	Description     string               `json:"description" db:"description"`
	Detail          string               `json:"detail" db:"detail"`
	LocationPinedID int64                `json:"location_pined_id,omitempty" db:"location_pined_id"`
	SerialNumber    string               `json:"serial_number" db:"serial_number"`
	Name            string               `json:"name,omitempty" db:"name"`
	UserCode        string               `json:"user_code,omitempty" db:"user_code"`
	CreatedAt       string               `json:"created_at" db:"created_at"`
	Reporter        string               `json:"reporter,omitempty" db:"reporter"`
	MarkName        string               `json:"mark_name,omitempty" db:"mark_name"`
	ConfirmedImage  string               `json:"confirmed_image" db:"confirmed_image"`
	Position        []ModelLocationPined `json:"position"`
}

//ModelQty Model
type ModelQty struct {
	Qty int64 `json:"qty" db:"qty"`
}

//ModelQtyTransaction Model
type ModelQtyTransaction struct {
	QtyProcess1_2 int64 `json:"qty_1_2" db:"qty_1_2"`
	QtyProcess3   int64 `json:"qty_3" db:"qty_3"`
}
