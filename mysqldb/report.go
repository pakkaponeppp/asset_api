package mysqldb

import (
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/pakkaponeppp/asset_api/report"
	"upper.io/db.v3/lib/sqlbuilder"
	"upper.io/db.v3/mysql"
)

//NewReportRepository asset_api
func NewReportRepository(Bdb *sqlx.DB) report.Repository {

	bdb := Bdb.DB

	bidb, err := mysql.New(bdb)
	if err != nil {
		return nil
	}

	r := reportRepository{bidb, Bdb}
	return &r
}

type reportRepository struct {
	db  sqlbuilder.Database
	dbx *sqlx.DB
}

func (r *reportRepository) CreateReportRepo(model *report.ModelCreateCheckedReport) (interface{}, error) {
	_, err := r.createReport(model)
	if err != nil {
		return nil, err
	}

	return nil, nil

}

func (r *reportRepository) createReport(model *report.ModelCreateCheckedReport) (interface{}, error) {

	r.db.InsertInto("report").Values(map[string]interface{}{
		"report_name": model.ReportName,
		// "items_id":    model.ListItem,
		"user_code":  model.Usercode,
		"created_at": time.Now().Add(time.Minute * 10),
	})

	return nil, nil

}
func (r *reportRepository) createReportItems(model *report.ModelCreateCheckedReport) (interface{}, error) {

	x := model.ListItem

	fmt.Print(x)

	r.db.InsertInto("report").Values(map[string]interface{}{
		"report_name": model.ReportName,
		"items_id":    model.ListItem,
		"user_code":   model.Usercode,
		"created_at":  time.Now().Add(time.Minute * 10),
	})

	return nil, nil

}
