package mysqldb

import (
	"errors"
	"fmt"
	"log"
	"sort"
	"strings"
	"sync"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/utahta/go-linenotify"
	"gitlab.com/pakkaponeppp/asset_api/transaction"
	"upper.io/db.v3/lib/sqlbuilder"
	"upper.io/db.v3/mysql"
)

//NewTransactionRepository asset_api
func NewTransactionRepository(Bdb *sqlx.DB) transaction.Repository {

	bdb := Bdb.DB

	bidb, err := mysql.New(bdb)
	if err != nil {
		return nil
	}

	r := transactionRepository{bidb, Bdb}
	return &r
}

type transactionRepository struct {
	db  sqlbuilder.Database
	dbx *sqlx.DB
}

////////////////////////////////////////////////////       Repo        ////////////////////////////////////////////////////

func (r *transactionRepository) CreateRequestRepo(model *transaction.ModelTransaction) (interface{}, error) {
	_, err := r.createRequestAsset(model)
	if err != nil {
		return nil, err
	}

	return nil, nil

}
func (r *transactionRepository) CreateRequestBypassRepo(model *transaction.ModelTransactionBypass) (interface{}, error) {
	_, err := r.createRequestAssetBypass(model)
	if err != nil {
		return nil, err
	}

	return nil, nil

}
func (r *transactionRepository) CreateSwitchUserBypassRepo(model *transaction.ModelTransactionBypass) (interface{}, error) {

	_, err := r.CreateReturnBypass(model.Name, model.PositionName, model.AssetItemID, model.Message)
	if err != nil {
		return nil, err
	}

	_, err = r.createRequestAssetBypass(model)
	if err != nil {
		return nil, err
	}

	return nil, nil

}
func (r *transactionRepository) CreateRepairRepo(model *transaction.ModelTransaction) (interface{}, error) {
	_, err := r.createRepairAsset(model)
	if err != nil {
		return nil, err
	}

	return nil, nil

}
func (r *transactionRepository) CreateReturnRepo(Usercode string, AssetItemID int64, Message string) (interface{}, error) {
	_, err := r.CreateReturn(Usercode, AssetItemID, Message)
	if err != nil {
		return nil, err
	}

	return nil, nil

}
func (r *transactionRepository) CreateReturnRepoBypass(Name string, PositionName string, AssetItemID int64, Message string) (interface{}, error) {
	_, err := r.CreateReturnBypass(Name, PositionName, AssetItemID, Message)
	if err != nil {
		return nil, err
	}

	return nil, nil

}
func (r *transactionRepository) AcceptRequestRepo(ID int64, AssetID int64, Items []int64) (interface{}, error) {
	resp, err := r.acceptRequestAsset(ID, AssetID, Items)
	if err != nil {
		return nil, err
	}

	return resp, nil

}
func (r *transactionRepository) AcceptReturnRepo(ID int64, AssetID int64, AssetItemID int64) (interface{}, error) {
	resp, err := r.acceptReturn(ID, AssetID, AssetItemID)
	if err != nil {
		return nil, err
	}

	return resp, nil

}
func (r *transactionRepository) QRcodeIDRepo(Items string) (interface{}, error) {
	resp, err := r.QRcodeID(Items)
	if err != nil {
		return nil, err
	}

	return resp, nil

}
func (r *transactionRepository) DeclineRequestRepo(ID int64, ErrorMeseage string) (interface{}, error) {
	_, err := r.declineRequestAsset(ID, ErrorMeseage)
	if err != nil {
		return nil, err
	}

	return nil, nil

}

func (r *transactionRepository) ListTransactionRepo(ID int64, ProcessID int64, UserCode string) (interface{}, error) {

	resp, err := r.listTransaction(ID, ProcessID, UserCode)

	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *transactionRepository) GetTransactionRequestRepo(ID int64) (interface{}, error) {

	resp, err := r.getTransactionRequest(ID)

	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *transactionRepository) GetQtyTransactionRepo(Usercode string) (interface{}, error) {

	resp, err := r.GetQtyTransaction(Usercode)

	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (r *transactionRepository) GetAssetItemRepairRepo(ID int64) (interface{}, int64, error) {

	resp, status, err := r.GetAssetItemRepair(ID)

	if err != nil {
		return nil, status, err
	}

	return resp, status, nil

}
func (r *transactionRepository) GetDetailTransactionRepo(ID int64) (interface{}, error) {

	resp, err := r.GetDetailTransaction(ID)

	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (r *transactionRepository) ListItemsUserRepo(Usercode string) (interface{}, error) {

	resp, err := r.ListItemsUser(Usercode)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (r *transactionRepository) CancelTransactionUserRepo(ID int64) (interface{}, error) {

	resp, err := r.CancelTransactionUser(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (r *transactionRepository) CancelAllUserRepo(ID int64, AssetItemID int64) (interface{}, error) {

	resp, err := r.CancelReturnUser(ID, AssetItemID)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (r *transactionRepository) CancelReportUserRepo(ID int64, AssetItemID int64) (interface{}, error) {

	resp, err := r.CancelReportUser(ID, AssetItemID)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (r *transactionRepository) ManageRepairRepo(ID int64, Status int64, AssetItemID int64, Message string) (interface{}, error) {

	resp, err := r.ManageRepair(ID, Status, AssetItemID, Message)
	if err != nil {
		return nil, err
	}
	return resp, nil

}

//------------------------------------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------------------------------------//

func (r *transactionRepository) ManageRepair(ID int64, Status int64, AssetItemID int64, Message string) (interface{}, error) {

	switch Status {
	case 2:
		go r.updateTransactionStatus2(ID)
		go r.updateMessage(ID, Message)
		go r.updateStatusItems3(AssetItemID)
	case 4:
		go r.updateStatusItems3(AssetItemID)
		go r.updateMessage(ID, Message)
		go r.StoreInAssetDamage(ID, AssetItemID)
		go r.updateStatusItems2(AssetItemID)
	default:
		return nil, errors.New("คำสั่งไม่ถูกต้อง")
	}

	return nil, nil

}
func (r *transactionRepository) acceptReturn(ID int64, AssetID int64, AssetItemID int64) (interface{}, error) {

	var item int64 = 0

	sql1 := ` select a.id from asset_item a where a.id = ` + fmt.Sprint(AssetItemID) + ` and a.asset_id = ` + fmt.Sprint(AssetID) + ` and a.active = 1 and a.status = 1 and a.available = 0`

	rs := r.dbx.QueryRow(sql1)

	err := rs.Scan(&item)

	if err != nil {
		return nil, errors.New("สินทรัพย์ไม่ถูกต้อง")
	}

	go r.updateTransactionStatus2(ID)
	go r.updateStatusItems(ID)
	go r.updateAvailable1(AssetItemID)
	if err != nil {
		return nil, err
	}
	go r.updateStatusItems2(AssetItemID)

	return nil, nil

}
func (r *transactionRepository) StoreInAssetDamage(ID int64, AssetItemID int64) (interface{}, error) {

	var item int64 = 0

	sql1 := ` select a.id from asset_item a where a.id = ` + fmt.Sprint(AssetItemID) + ` and a.active = 1 and a.status = 1 and a.available = 0`

	rs := r.dbx.QueryRow(sql1)

	err := rs.Scan(&item)

	if err != nil {
		return nil, errors.New("สินทรัพย์ไม่ถูกต้อง")
	}

	go r.updateTransactionStatus4(ID)
	go r.updateAvailableAndStatus(AssetItemID)

	return nil, nil

}

func (r *transactionRepository) updateStatusItems(ID int64) (interface{}, error) {
	q := r.db.Update("items").
		Set("status", 2).
		Where("transaction_id", ID)

	_, err := q.Exec()
	if err != nil {
		return nil, err
	}

	return nil, nil
}
func (r *transactionRepository) updateStatusItems2(AssetItemID int64) (interface{}, error) {
	q := r.db.Update("items").
		Set("status", 2).
		Where("asset_item_id", AssetItemID).And("process_id", 1).And("status", 1)

	_, err := q.Exec()
	if err != nil {
		return nil, err
	}

	return nil, nil
}
func (r *transactionRepository) updateStatusItems4(AssetItemID int64) (interface{}, error) {
	q := r.db.Update("items").
		Set("status", 2).
		Where("asset_item_id", AssetItemID).And("process_id", 3).And("status", 1)

	_, err := q.Exec()
	if err != nil {
		return nil, err
	}

	return nil, nil
}
func (r *transactionRepository) updateMessage(ID int64, Message string) (interface{}, error) {
	q := r.db.Update("transaction").
		Set("response_message", Message).
		Where("id = ?", ID).And("process_id", 3)

	_, err := q.Exec()
	if err != nil {
		return nil, err
	}

	return nil, nil
}
func (r *transactionRepository) updateStatusItems3(AssetItemID int64) (interface{}, error) {
	q := r.db.Update("items").
		Set("status", 2).
		Where("asset_item_id", AssetItemID).And("process_id", 3).And("status", 1)

	_, err := q.Exec()
	if err != nil {
		return nil, err
	}

	return nil, nil
}
func (r *transactionRepository) updateAvailable1(AssetItemID int64) (interface{}, error) {
	q := r.db.Update("asset_item").
		Set("available", 1).
		Set("updated_at", time.Now().Add(time.Minute*10)).
		Where("id", AssetItemID)

	_, err := q.Exec()
	if err != nil {
		return nil, err
	}

	return nil, nil
}
func (r *transactionRepository) updateAvailableAndStatus(AssetItemID int64) (interface{}, error) {
	q := r.db.Update("asset_item").
		Set("available", 1).
		Set("status", 0).
		Set("updated_at", time.Now().Add(time.Minute*10)).
		Where("id", AssetItemID)

	_, err := q.Exec()
	if err != nil {
		return nil, err
	}

	return nil, nil
}
func (r *transactionRepository) CreateReturn(Usercode string, AssetItemID int64, Message string) (interface{}, error) {

	_, err := r.checkTransactionReturnQty(AssetItemID)
	if err != nil {
		return nil, err
	}

	data, err := r.ListItemsUserID(Usercode, AssetItemID)

	if err != nil {
		return nil, err
	}

	q := r.db.InsertInto("transaction").
		Values(map[string]interface{}{
			"user_code":          Usercode,
			"asset_id":           data.AssetID,
			"items_qty":          1,
			"location_pined_id":  data.LocationPinedID,
			"process_id":         2,
			"message":            Message,
			"transaction_status": 1,
			"created_at":         time.Now().Add(time.Minute * 10),
		})

	result, err := q.Exec()
	if err != nil {
		log.Println(err.Error())
	}

	tsID, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}
	go r.InsertItemsTransactionReturn(tsID, data.ID)
	go r.pushNotify(tsID)

	return nil, nil

}

func (r *transactionRepository) getlocationPinedID(ID int64) (int64, error) {

	var pinedID int64 = 0

	q := `select a.location_pined_id from transaction a
	left join items b on a.id = b.transaction_id
	where b.asset_item_id = ? and b.process_id = 1 and b.status = 1 `

	rs := r.dbx.QueryRowx(q, ID)
	err := rs.Scan(&pinedID)
	if err != nil {
		return 0, errors.New("ถอนไปแล้ว")
	}
	return pinedID, nil
}

func (r *transactionRepository) CreateReturnBypass(Name string, PositionName string, AssetItemID int64, Message string) (interface{}, error) {

	assetID, err := r.getAssetID0(AssetItemID)

	if err != nil {
		return nil, err
	}
	pID, _ := r.getlocationPinedID(AssetItemID)

	if err != nil {
		return nil, err
	}

	_, err = r.checkTransactionReturnQty(AssetItemID)
	if err != nil {
		return nil, err
	}
	_, usercode, _ := r.getName(Name, PositionName)

	q := r.db.InsertInto("transaction").
		Values(map[string]interface{}{
			"user_code":          usercode,
			"asset_id":           assetID,
			"items_qty":          1,
			"location_pined_id":  pID,
			"process_id":         2,
			"message":            Message,
			"transaction_status": 1,
			"created_at":         time.Now().Add(time.Minute * 10),
		})

	result, err := q.Exec()
	if err != nil {
		log.Println(err.Error())
	}

	tsID, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}
	_, err = r.InsertItemsTransactionReturn(tsID, AssetItemID)
	if err != nil {
		return nil, err
	}

	_, err = r.acceptReturn(tsID, assetID, AssetItemID)
	if err != nil {
		return nil, err
	}

	return nil, nil

}
func (r *transactionRepository) CancelTransactionUser(ID int64) (interface{}, error) {

	q := r.db.Update("transaction").
		Set("transaction_status", 4).
		Set("updated_at", time.Now().Add(time.Minute*10)).
		Where("id", ID)
	_, err := q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	return nil, nil

}
func (r *transactionRepository) CancelReturnUser(ID int64, AssetItemID int64) (interface{}, error) {

	go r.CancelTransactionUser(ID)
	q := r.db.Update("items").
		Set("status", 2).
		Where("asset_item_id = ", AssetItemID).And("process_id", 2).And("status", 1)

	_, err := q.Exec()
	if err != nil {
		return nil, err
	}
	return nil, nil

}
func (r *transactionRepository) CancelReportUser(ID int64, AssetItemID int64) (interface{}, error) {

	go r.CancelTransactionUser(ID)
	q := r.db.Update("items").
		Set("status", 2).
		Where("asset_item_id = ", AssetItemID).And("process_id", 3).And("status", 1)

	_, err := q.Exec()
	if err != nil {
		return nil, err
	}
	return nil, nil

}
func (r *transactionRepository) GetItemStatus(ID int64, Usercode string) (int64, error) {

	sql1 := `select b.id,
		ifnull(b.process_id,0) as process_id from items a 
			left join transaction b on a.transaction_id = b.id 
				where a.status = 1 and b.transaction_status = 1 and b.user_code = '` + Usercode + `' and a.asset_item_id = ` + fmt.Sprint(ID)
	model := transaction.ModelStatusItem{}

	rs := r.dbx.QueryRowx(sql1)
	err := rs.StructScan(&model)

	if err != nil {
		return 1, err
	}

	return model.ProcessID, nil

}

func (r *transactionRepository) ListItemsUser(Usercode string) (interface{}, error) {

	sql1 := ` select b.id,
	ifnull(b.asset_id,0) as asset_id ,
	ifnull(b.serial_number,'') as serial_number,
	ifnull(b.location_id,0) as location_id,
	ifnull(b.detail,'') as detail,
	ifnull(b.contact,'') as contact,
	ifnull(b.conditions,0) as conditions,
	ifnull(b.buying_date,0) as buying_date,
	ifnull(b.prices,0.0) as prices,
	ifnull(b.calculated_prices,0.0) as calculated_prices,
	ifnull(b.started_warranty,0) as started_warranty,
	ifnull(b.warranty_month,0) as  warranty_month,
	ifnull(b.warranty_date,0) as warranty_date,
	ifnull(b.created_at,0) as  created_at,
	ifnull(b.updated_at,0) as updated_at,
	ifnull(b.image_url,'') as image_url,
	ifnull(c.name,'') as asset_name,
	ifnull(c.url,'') as url,
	ifnull(c.description,'') as description,
	ifnull(d.name,'') as category_name,
	ifnull(f.sname,'') as sub_category_name,
	ifnull(c.barcode,'') as barcode,
	ifnull(a.location_pined_id,0) as location_pined_id
		from transaction a 
			left join items e on a.id = e.transaction_id
			left join asset_item b on e.asset_item_id = b.id 
			left join asset c on b.asset_id = c.id
			left join category d on c.category_id = d.id
			left join sub_category f on c.sub_category_id = f.id
				where a.process_id = 1 and c.active = 1 and b.active = 1 and  a.transaction_status = 2 and a.user_code = '` + Usercode + `' and e.process_id = 1 and e.status = 1 and b.available = 0  `

	rs, err := r.dbx.Queryx(sql1)
	if err != nil {
		return nil, err
	}

	models := []transaction.ModelAssetItem{}

	for rs.Next() {
		model := transaction.ModelAssetItem{}
		err = rs.StructScan(&model)
		if err != nil {
			return nil, err
		}

		status, _ := r.GetItemStatus(model.ID, Usercode)

		model.Status = status

		position, err := r.getPosition(model.LocationPinedID)

		if err != nil {
			return nil, err
		}

		model.Position = position

		models = append(models, model)

	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil

}
func (r *transactionRepository) ListItemsUserID(Usercode string, AssetItemID int64) (*transaction.ModelAssetItem, error) {

	sql1 := ` select b.id,
	ifnull(b.asset_id,0) as asset_id ,
	ifnull(b.serial_number,'') as serial_number,
	ifnull(b.location_id,0) as location_id,
	ifnull(b.detail,'') as detail,
	ifnull(b.contact,'') as contact,
	ifnull(b.conditions,0) as conditions,
	ifnull(b.buying_date,0) as buying_date,
	ifnull(b.prices,0.0) as prices,
	ifnull(b.calculated_prices,0.0) as calculated_prices,
	ifnull(b.started_warranty,0) as started_warranty,
	ifnull(b.warranty_month,0) as  warranty_month,
	ifnull(b.warranty_date,0) as warranty_date,
	ifnull(b.created_at,0) as  created_at,
	ifnull(b.updated_at,0) as updated_at,
	ifnull(c.name,'') as asset_name,
	ifnull(c.url,'') as url,
	ifnull(c.description,'') as description,
	ifnull(d.name,'') as category_name,
	ifnull(f.sname,'') as sub_category_name,
	ifnull(c.barcode,'') as barcode,
	ifnull(a.location_pined_id,0) as location_pined_id
	from transaction a 
	left join items e on a.id = e.transaction_id
	left join asset_item b on e.asset_item_id = b.id 
	left join asset c on b.asset_id = c.id
	left join category d on c.category_id = d.id
	left join sub_category f on c.sub_category_id = f.id
	 where a.process_id = 1 and a.transaction_status = 2 and user_code = ? and e.process_id = 1 and e.status = 1 and b.available = 0 and b.id = ` + fmt.Sprint(AssetItemID) + ` `

	rs := r.dbx.QueryRowx(sql1, Usercode)

	model := transaction.ModelAssetItem{}
	err := rs.StructScan(&model)
	if err != nil {
		return nil, err
	}

	position, err := r.getPosition(model.LocationPinedID)

	if err != nil {
		return nil, err
	}

	model.Position = position

	return &model, nil

}
func (r *transactionRepository) GetAssetItemRepair(ID int64) (*transaction.ModelAssetItemRepair, int64, error) {

	var status int64 = 0

	sql1 := ` select b.id,
	ifnull(c.id,'') as asset_id,
	ifnull(c.name,'') as asset_name,
	ifnull(c.description,'') as description,
	ifnull(b.detail,'') as detail,
	ifnull(a.location_pined_id,'') as location_pined_id,
	ifnull(b.serial_number,'') as serial_number,
	ifnull(d.name,'') as name,
	ifnull(a.user_code,'') as user_code,
	ifnull(a.created_at,'') as created_at,
	ifnull(a.confirmed_image,'') as confirmed_image,
	ifnull(f.mark_name,'') as mark_name
	 from items e 
		left join transaction a on e.transaction_id = a.id
		left join asset_item b on e.asset_item_id = b.id
		left join asset c on a.asset_id = c.id
		left join user d on a.user_code = d.user_code
		left join location_pined f on a.location_pined_id = f.id
	 		where e.asset_item_id = ? and e.process_id = 1 and e.status = 1`

	model := transaction.ModelAssetItemRepair{}
	rs := r.dbx.QueryRowx(sql1, ID)
	err := rs.StructScan(&model)
	if err != nil {

		_, status, err := r.checkavailable(ID)

		return nil, status, err
	}

	if status == 0 {

		_, status, err := r.checkReturnReq(ID)
		if err != nil {
			status = 3
			return nil, status, err
		}

		position, err := r.getPosition(model.LocationPinedID)

		model.Position = position

		return &model, status, err

	}

	return &model, status, nil

}
func (r *transactionRepository) checkReturnReq(ID int64) (interface{}, int64, error) {

	var available int64 = 0

	sql1 := `select case when (select a.status from items a where a.asset_item_id = ` + fmt.Sprint(ID) + ` and a.status = 1 and a.process_id = 2) = 1 then 4
	when (select a.status from items a where a.asset_item_id = ` + fmt.Sprint(ID) + ` and a.status = 1 and a.process_id = 3) = 1 then 7

	else 3 end as available`

	rs := r.dbx.QueryRowx(sql1)
	err := rs.Scan(&available)

	if err != nil {
		return nil, 0, errors.New("รอการอนุมัติ")
	}

	return nil, available, nil
}

func (r *transactionRepository) checkavailable(ID int64) (interface{}, int64, error) {

	var available int64 = 0

	sql1 := `select case when (select a.available from asset_item a where a.id = ` + fmt.Sprint(ID) + ` and a.status = 1) = 1 then 1
	when (select a.available from asset_item a where a.id = ` + fmt.Sprint(ID) + ` and a.status = 0) = 1 then 6

	else 2 end as available`

	rs := r.dbx.QueryRowx(sql1)
	err := rs.Scan(&available)

	if err != nil {

		return nil, available, err
	}

	return nil, available, nil
}

func (r *transactionRepository) GetDetailTransaction(ID int64) (*transaction.ModelGetDetail, error) {

	sql1 := ` select a.id,
	ifnull(a.asset_id,0) as asset_id,
	ifnull(b.name,'') as asset_name,
	ifnull(a.items_qty,0) as items_qty,
	ifnull(b.description,0) as description,
	ifnull(b.barcode,0) as barcode,
	ifnull(c.name,0) as category_name,
	ifnull(e.sname,0) as sub_category_name,
	ifnull(a.process_id,0) as process_id,
	ifnull(a.message,0) as message,
	ifnull(a.response_message ,0) as response_message ,
	ifnull(a.created_at,0) as created_at,
	ifnull(a.updated_at,0) as updated_at,
	ifnull(b.url,0) as url,
	ifnull(a.confirmed_image,'') as confirmed_image
	from transaction a
	left join asset b on a.asset_id = b.id
	left join category c on b.category_id = c.id
	left join sub_category e on b.sub_category_id = e.id
	where a.id = ? `
	model := transaction.ModelGetDetail{}
	rs := r.dbx.QueryRowx(sql1, ID)
	err := rs.StructScan(&model)
	if err != nil {
		return nil, err
	}

	items, err := r.GetDetailItems(ID)
	if err != nil {
		return nil, err
	}

	model.AssetItems = items

	return &model, nil

}
func (r *transactionRepository) GetDetailItems(ID int64) ([]transaction.ModelAssetItem, error) {

	sql1 := ` select a.asset_item_id,
	ifnull(b.serial_number,0) as serial_number,
	ifnull(b.location_id,'') as location_id,
	ifnull(b.detail,'') as detail,
	ifnull(b.buying_date,'') as buying_date,
	ifnull(b.calculated_prices,'') as calculated_prices,
	ifnull(b.conditions,'') as conditions,
	ifnull(b.contact,'') as contact,
	ifnull(b.created_at,'') as created_at,
	ifnull(b.prices,'') as prices,
	ifnull(b.image_url,'') as image_url,
	ifnull(b.started_warranty,'') as started_warranty,
	ifnull(b.updated_at,'') as updated_at,
	ifnull(b.warranty_date,'') as warranty_date,
	ifnull(b.warranty_month,'') as warranty_month
	from items a 
	left join asset_item b on a.asset_item_id = b.id where a.transaction_id = ?`

	models := []transaction.ModelAssetItem{}
	rs, err := r.dbx.Queryx(sql1, ID)
	if err != nil {
		return nil, err
	}
	for rs.Next() {

		model := transaction.ModelAssetItem{}

		err = rs.StructScan(&model)

		if err != nil {
			return nil, err
		}

		models = append(models, model)

	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil

}

func (r *transactionRepository) GetItemsID(ID int64) (int64, error) {

	sql1 := ` select a.asset_item_id
	from items a 
	left join asset_item b on a.asset_item_id = b.id where a.transaction_id = ?`

	model := transaction.ModelAssetItem{}
	rs := r.dbx.QueryRowx(sql1, ID)
	err := rs.Scan(&model.ID)
	if err != nil {
		return 0, nil
	}

	return model.ID, nil

}

func (r *transactionRepository) QRcodeID(Items string) (interface{}, error) {

	barcode := ""
	pathlink := "https://asset.nopadol.net/RepairRequest/"
	trimmed := strings.Trim(Items, "[]")
	strings := strings.Split(trimmed, ",")
	path := make([]string, len(strings))

	sql1 := `select b.barcode from asset_item a
	left join asset b on a.asset_id = b.id
	where a.id = ` + strings[0] + ` `

	rs := r.dbx.QueryRowx(sql1)

	err := rs.Scan(&barcode)

	if err != nil {
		return nil, err
	}
	type qr struct {
		AssetItemID string `json:"asset_item_id"`
		Barcode     string `json:"barcode"`
		Paths       string `json:"path"`
	}
	model := qr{}
	models := []qr{}

	for i := range strings {

		path[i] = pathlink + fmt.Sprint(strings[i])
		model.Barcode = barcode
		model.Paths = path[i]
		model.AssetItemID = fmt.Sprint(strings[i])
		models = append(models, model)

	}

	return models, nil

}
func (r *transactionRepository) GetQtyTransaction(Usercode string) (*transaction.ModelQtyTransaction, error) {

	usercode := ""

	if Usercode != "" {
		usercode = " and user_code = '" + Usercode + "'"
	}

	qty := transaction.ModelQtyTransaction{}

	sql1 := ` select (select count(*) from transaction where process_id in (1,2) and transaction_status = 1 ` + usercode + `) as qty_1_2,
	(select count(*) from transaction where process_id = 3 and transaction_status = 1 ` + usercode + `) as qty_3 `

	fmt.Println(sql1)

	rs := r.dbx.QueryRowx(sql1)

	err := rs.Scan(&qty.QtyProcess1_2, &qty.QtyProcess3)

	if err != nil {
		return nil, err
	}

	return &qty, nil

}

func (r *transactionRepository) pushNotify(ID int64) (interface{}, error) {

	line, err := r.getNotifyRequest(ID)
	if err != nil {
		return nil, err
	}

	if line.NotifyStatus < 1 {
		loc, _ := time.LoadLocation("Asia/Bangkok")
		token := "ef3yuF71fwkVF8nyNadNla0SGRwJfidL8BJk4bfjY2U"
		layout := "Monday, 02-Jan-06 15:04"
		tsTime := time.Now().Add(time.Minute * 10).In(loc).Format(layout)

		massage := fmt.Sprintf("\nผู้ใช้งาน : %v\n%v : %v\nเมื่อ : %v\nจำนวน : %v\nตรวจสอบ : %v", line.Name, line.Process, line.AssetName, tsTime, line.ItemsQty, ID)

		c := linenotify.New()
		go c.NotifyMessage(token, massage)
		go r.updateNotifyStatus(ID)
	}

	return nil, nil
}

func (r *transactionRepository) getNotifyRequest(ID int64) (*transaction.ModelLineNotify, error) {

	sql1 := `select a.id,
	ifnull(b.name,'') as name,
	ifnull(a.user_code,'') as user_code,
	ifnull(a.items_qty,0) as items_qty,
	ifnull(c.name,'') as asset_name,
	ifnull(d.name,'') as process_name,
	ifnull(a.created_at,'') as created_at,
	ifnull(a.notify_status,'') as notify_status
	
	from transaction a 
	left join user b on a.user_code = b.user_code
	left join asset c on a.asset_id = c.id
	left join process d on a.process_id = d.id
	where a.id = ?`

	rs := r.dbx.QueryRowx(sql1, ID)

	model := transaction.ModelLineNotify{}

	err := rs.StructScan(&model)

	if err != nil {
		return nil, err
	}

	return &model, nil
}

func (r *transactionRepository) updateNotifyStatus(ID int64) (interface{}, error) {

	q := r.db.Update("transaction").Set("notify_status", 1).Where("id = ?", ID)
	_, err := q.Exec()
	if err != nil {
		return nil, err
	}
	return nil, nil
}

func (r *transactionRepository) acceptRequestAsset(ID int64, AssetID int64, Items []int64) (interface{}, error) {

	itemsqty := []int64{}
	var item int64 = 0
	var qty int = 0

	if len(Items) < 1 {
		err := errors.New("กรุณาระบุสินทรัพย์")
		return nil, err
	}

	for i := 0; i < len(Items); i++ {

		sql1 := ` select a.id from asset_item a where a.id = ` + fmt.Sprint(Items[i]) + ` and a.asset_id = ` + fmt.Sprint(AssetID) + ` and a.active = 1 and a.status = 1 and a.available = 1`

		rs := r.dbx.QueryRow(sql1)

		err := rs.Scan(&item)

		if err != nil {
			return nil, errors.New("สินทรัพย์ไม่ถูกต้อง")
		}

		itemsqty = append(itemsqty, item)

	}

	sql1 := ` select a.items_qty from transaction a where a.id = ` + fmt.Sprint(ID) + ` and a.asset_id = ` + fmt.Sprint(AssetID) + ` and a.transaction_status = 1`

	rs := r.dbx.QueryRow(sql1)

	err := rs.Scan(&qty)

	if err != nil {
		return nil, err
	}

	if qty != len(itemsqty) {
		return nil, errors.New("จำนวนสินทรัพย์ไม่ถูกต้อง")

	}

	go r.updateAvailable(AssetID, Items)
	go r.updateTransactionStatus2(ID)
	go r.InsertItemsTransaction(ID, Items)

	return nil, nil
}
func (r *transactionRepository) declineRequestAsset(ID int64, ErrorMeseage string) (interface{}, error) {

	q := r.db.Update("transaction").
		Set("transaction_status", 0).
		Set("response_message", ErrorMeseage).
		Set("updated_at", time.Now().Add(time.Minute*10)).
		Where("id = ?", ID)
	_, err := q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	return nil, nil
}

func (r *transactionRepository) updateAvailable(AssetID int64, Items []int64) (interface{}, error) {

	for i := 0; i < len(Items); i++ {
		q := r.db.Update("asset_item").
			Set("available", 0).
			Where("id = ?", Items[i])

		_, err := q.Exec()
		if err != nil {
			log.Println(err.Error())
			return nil, errors.New("ไม่พบสินทรัพย์")
		}
	}

	return nil, nil

}
func (r *transactionRepository) InsertItemsTransaction(ID int64, Items []int64) (interface{}, error) {

	for i := 0; i < len(Items); i++ {
		q := r.db.InsertInto("items").Values(map[string]interface{}{
			"transaction_id": ID,
			"asset_item_id":  Items[i],
			"process_id":     1,
			"status":         1,
		})

		_, err := q.Exec()
		if err != nil {
			log.Println(err.Error())
			return nil, errors.New("ไม่พบสินทรัพย์")
		}
	}

	return nil, nil

}
func (r *transactionRepository) InsertItemsTransactionRepair(ID int64, AssetItemID int64) (interface{}, error) {

	if AssetItemID < 0 {
		return nil, errors.New("สินทรัพย์ไม่ถูกต้อง")
	}

	q := r.db.InsertInto("items").Values(map[string]interface{}{
		"transaction_id": ID,
		"asset_item_id":  AssetItemID,
		"process_id":     3,
		"status":         1,
	})
	_, err := q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err

	}
	return nil, nil
}
func (r *transactionRepository) InsertItemsTransactionReturn(ID int64, AssetItemID int64) (interface{}, error) {

	if AssetItemID < 0 {
		return nil, errors.New("สินทรัพย์ไม่ถูกต้อง")
	}

	q := r.db.InsertInto("items").Values(map[string]interface{}{
		"transaction_id": ID,
		"asset_item_id":  AssetItemID,
		"process_id":     2,
		"status":         1,
	})
	_, err := q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err

	}
	return nil, nil
}

func (r *transactionRepository) updateTransactionStatus2(ID int64) (interface{}, error) {

	q := r.db.Update("transaction").
		Set("transaction_status", 2).
		Set("updated_at", time.Now().Add(time.Minute*10)).
		Where("id = ?", ID)
	_, err := q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	return nil, nil

}
func (r *transactionRepository) updateTransactionStatus4(ID int64) (interface{}, error) {

	q := r.db.Update("transaction").
		Set("transaction_status", 0).
		Set("updated_at", time.Now().Add(time.Minute*10)).
		Where("id = ?", ID)
	_, err := q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	return nil, nil

}

func (r *transactionRepository) getAssetActiveStatus(ID int64) (*transaction.ModelTransaction, error) {

	q := r.db.Select("id").From("asset").Where("id = ?", ID).And("active", 1)
	assetID, err := q.QueryRow()
	if err != nil {
		return nil, err
	}
	model := transaction.ModelTransaction{}
	err = assetID.Scan(&model.ID)
	if err != nil {
		return nil, err
	}
	if model.ID < 1 {
		return nil, errors.New("ไม่พบสินทรัพย์")
	}
	return nil, nil
}

func (r *transactionRepository) checkTransactionDIFFQty(AssetID int64, ItemsQty int64, ProcessID int64) (int64, error) {

	var count int64 = 0
	var sumary int64 = 0
	var remain int64 = 0

	if ItemsQty < 1 {
		return 0, errors.New("จำนวนไม่ถูกต้อง")
	}

	sql1 := ` select case
	when ` + fmt.Sprint(ItemsQty) + ` > (select count(*) from asset_item a1 where a1.asset_id = ` + fmt.Sprint(AssetID) + ` and a1.available = 1 and a1.active = 1 and a1.status = 1)
	then 0
	else 1 end as count`

	fmt.Println(sql1)
	rs := r.dbx.QueryRow(sql1)

	err := rs.Scan(&count)
	if err != nil {
		return 0, err
	}
	if count == 0 {
		return 0, errors.New("จำนวนไม่พอ")
	}

	sql2 := `select (select sum(items_qty) from transaction a1 where a1.asset_id = ` + fmt.Sprint(AssetID) + ` and a1.transaction_status = 1 and a1.process_id = ` + fmt.Sprint(ProcessID) + `) as sumary, (select count(*) from asset_item a1 where a1.asset_id = ` + fmt.Sprint(AssetID) + ` and a1.available = 1 and a1.active = 1 and a1.status = 1) as remain `

	rs2 := r.dbx.QueryRowx(sql2)

	err = rs2.Scan(&sumary, &remain)
	if err != nil {
		return 0, nil
	}

	remain = remain - sumary

	if ItemsQty > remain {
		return 0, errors.New("จำนวนไม่พอ")
	}

	return 0, nil
}
func (r *transactionRepository) checkTransactionRepairQty(AssetItemID int64) (int64, error) {
	var count int64 = 0
	sql1 := ` select case when (select count(*) from items a where a.asset_item_id = ` + fmt.Sprint(AssetItemID) + ` and a.process_id = 3 and a.status = 1) = (select count(*) from items a where a.asset_item_id = ` + fmt.Sprint(AssetItemID) + ` and a.process_id = 2 and a.status = 1) then 0 
	else 1 end as count `
	rs := r.dbx.QueryRow(sql1)
	err := rs.Scan(&count)
	if err != nil {
		return 0, err
	}
	if count == 0 {
		return 0, nil
	} else if count == 1 {
		return 0, errors.New("ไม่สามารถทำรายการได้")

	}
	return count, nil
}
func (r *transactionRepository) checkTransactionReturnQty(AssetItemID int64) (int64, error) {
	var count int64 = 0
	sql1 := ` select case when (select count(*) from items a where a.asset_item_id = ` + fmt.Sprint(AssetItemID) + ` and a.process_id = 2 and a.status = 1) = (select count(*) from items a where a.asset_item_id = ` + fmt.Sprint(AssetItemID) + ` and a.process_id = 3 and a.status = 1) then 0 
	else 1 end as count `
	rs := r.dbx.QueryRow(sql1)
	err := rs.Scan(&count)
	if err != nil {
		return 0, err
	}
	if count == 0 {
		return 0, nil
	} else if count == 1 {
		return 0, errors.New("ไม่สามารถทำรายการได้")

	}
	return count, nil
}

func (r *transactionRepository) createRequestAsset(model *transaction.ModelTransaction) (interface{}, error) {

	_, err := r.getAssetActiveStatus(model.AssetID)
	if err != nil {
		return nil, err
	}
	_, err = r.checkTransactionDIFFQty(model.AssetID, model.ItemsQty, 1)
	if err != nil {
		return nil, err
	}

	_q := r.db.InsertInto("location_pined").Values(map[string]interface{}{
		"user_code":  model.UserCode,
		"mark_name":  model.MarkName,
		"lat":        model.Lat,
		"lng":        model.Lng,
		"created_at": time.Now(),
	})

	result, err := _q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	locPinedID, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}

	q := r.db.
		InsertInto("transaction").
		Values(map[string]interface{}{
			"user_code":          model.UserCode,
			"asset_id":           model.AssetID,
			"items_qty":          model.ItemsQty,
			"location_pined_id":  locPinedID,
			"process_id":         1,
			"transaction_status": 1,
			"message":            model.Message,
			"created_at":         time.Now().Add(time.Minute * 10),
		})

	result, err = q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	tsID, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}

	go r.pushNotify(tsID)

	return nil, nil
}
func (r *transactionRepository) getAssetID(ID int64) (int64, error) {

	var id int64 = 0

	q := `select asset_id from asset_item where id = ` + fmt.Sprint(ID) + ` and status = 1 and active = 1`

	rs := r.dbx.QueryRow(q)
	err := rs.Scan(&id)

	if err != nil {
		return 0, err
	}
	return id, nil
}
func (r *transactionRepository) getAssetID0(ID int64) (int64, error) {

	var id int64 = 0

	q := `select asset_id from asset_item where id = ? and available = 0 and status = 1 and active = 1`

	rs := r.dbx.QueryRowx(q, ID)
	err := rs.Scan(&id)
	if err != nil {
		return 0, errors.New("ถอนไปแล้ว")
	}
	return id, nil
}
func (r *transactionRepository) getName(name string, positionName string) (string, string, error) {

	var gotname string = ""
	var usercode string = ""
	var posID int64 = 0
	var pID int64 = 0

	q := `select ifnull(a.name,'') as gotname,
				 ifnull(a.user_code,'') as usercode,
				 ifnull(a.position_id,0) as posID from user a where a.name = ?`

	rs := r.dbx.QueryRowx(q, name)

	err := rs.Scan(&gotname, &usercode, &posID)

	if err != nil {
		createdName, createdUsercode, err := r.createNewUser(name, positionName)
		if err != nil {
			return "", "", err
		}

		return createdName, createdUsercode, nil
	}

	sql1 := `select case when (select a.id from position_name a where a.name = ?) = ` + fmt.Sprint(posID) + ` then 1 else 2 end as pID `

	rss := r.dbx.QueryRowx(sql1, positionName)

	err = rss.Scan(&pID)

	if pID == 2 {
		positionID, _ := r.createNewPosition(positionName)
		cc := r.db.Update("user").Set("position_id", positionID).Where("name = ?", name)

		_, err := cc.Exec()

		if err != nil {
			return "", "", err
		}

		return gotname, usercode, nil
	}

	return gotname, usercode, nil

}
func (r *transactionRepository) createNewPosition(positionName string) (int64, error) {
	_q := r.db.InsertInto("position_name").Values(map[string]interface{}{
		"name": positionName,
	})

	result, err := _q.Exec()
	if err != nil {
		log.Println(err.Error())
		return 0, err
	}

	lastID, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return lastID, nil
}

func (r *transactionRepository) createNewUser(name string, positionName string) (string, string, error) {

	tt := time.Now().Add(time.Hour * 99999)

	ss := tt.Format("2006-04-05")

	randomKey := strings.Replace(ss, "-", "", -1)

	UID := "UID696" + randomKey

	var posID int64 = 0

	sql1 := `select id as posID from position_name where name = ?`

	rs := r.dbx.QueryRowx(sql1, positionName)

	err := rs.Scan(&posID)

	if err != nil {
		positionID, _ := r.createNewPosition(positionName)
		posID = positionID
	}

	_q := r.db.InsertInto("user").Values(map[string]interface{}{
		"user_code":   UID,
		"username":    UID,
		"password":    1234,
		"position_id": posID,
		"role_id":     2,
		"name":        name,
		"active":      1,
	})

	_, err = _q.Exec()
	if err != nil {
		log.Println(err.Error())
		return "", "", err
	}

	return name, UID, nil

}

func (r *transactionRepository) createRequestAssetBypass(model *transaction.ModelTransactionBypass) (interface{}, error) {

	assetID, err := r.getAssetID(model.AssetItemID)

	if err != nil {
		return nil, err
	}

	_, err = r.getAssetActiveStatus(assetID)
	if err != nil {
		return nil, err
	}

	_, err = r.checkTransactionDIFFQty(assetID, model.ItemsQty, 1)
	if err != nil {
		return nil, err
	}
	_, usercode, _ := r.getName(model.Name, model.PositionName)

	_q := r.db.InsertInto("location_pined").Values(map[string]interface{}{
		"user_code":  usercode,
		"mark_name":  model.MarkName,
		"lat":        model.Lat,
		"lng":        model.Lng,
		"created_at": time.Now(),
	})

	result, err := _q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	locPinedID, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}

	q := r.db.
		InsertInto("transaction").
		Values(map[string]interface{}{
			"user_code":          usercode,
			"asset_id":           assetID,
			"items_qty":          1,
			"location_pined_id":  locPinedID,
			"process_id":         1,
			"transaction_status": 1,
			"message":            model.Message,
			"created_at":         time.Now().Add(time.Minute * 10),
			"confirmed_image":    model.ConfirmedImage,
		})

	result, err = q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	lastID, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}

	itemsqty := []int64{}

	itemsqty = append(itemsqty, model.AssetItemID)

	_, err = r.acceptRequestAsset(lastID, assetID, itemsqty)
	if err != nil {
		return nil, err
	}
	return nil, nil
}

func (r *transactionRepository) createRepairAsset(model *transaction.ModelTransaction) (interface{}, error) {

	_, err := r.checkTransactionRepairQty(model.AssetItemID)
	if err != nil {
		return nil, err
	}

	q := r.db.
		InsertInto("transaction").
		Values(map[string]interface{}{
			"user_code":          model.UserCode,
			"asset_id":           model.AssetID,
			"process_id":         3,
			"location_pined_id":  model.LocationPinedID,
			"transaction_status": 1,
			"items_qty":          1,
			"message":            model.Message,
			"reporter":           model.Reporter,
			"created_at":         time.Now().Add(time.Minute * 10),
		})

	result, err := q.Exec()
	if err != nil {
		log.Println(err.Error())
	}

	tsID, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}
	go r.InsertItemsTransactionRepair(tsID, model.AssetItemID)
	go r.pushNotify(tsID)

	return nil, nil
}

func (r *transactionRepository) listTransaction(ID int64, ProcessID int64, Usercode string) (interface{}, error) {

	usercode := ""

	if Usercode != "" {
		usercode = " and a.user_code = '" + Usercode + "'"
	}

	sql1 := ` select a.id,
	ifnull(a.user_code,0) as user_code,
	ifnull(a.asset_id,0) as asset_id,
	ifnull(b.name,'') as asset_name,
	ifnull((select x.asset_item_id
	from items x 
	where x.transaction_id = a.id),0) as asset_item_id,
	ifnull(c.name,'') as name,
	ifnull(a.process_id,0) as process_id,
	ifnull(a.location_pined_id,0) as location_pined_id,
	ifnull(a.message,'') as message,
	ifnull(d.mark_name,'') as mark_name,
	ifnull(a.response_message,'') as response_message,
	ifnull(a.created_at,'') as created_at,
	ifnull(a.updated_at,'') as updated_at,
	ifnull(a.notify_status,0) as notify_status,
	ifnull(a.items_qty,0) as items_qty,
	ifnull(a.transaction_status,0) as transaction_status,
	ifnull(a.reporter,'') as reporter,
	ifnull(a.confirmed_image,'') as confirmed_image
	from transaction a 
	left join asset b on a.asset_id = b.id
	left join location_pined d on a.location_pined_id = d.id
	left join user c on a.user_code = c.user_code 
	where a.process_id = ? ` + usercode + ` order by field(a.transaction_status,'1') desc,a.id desc `

	rs, err := r.dbx.Queryx(sql1, ProcessID)
	if err != nil {
		return nil, err
	}
	var wg sync.WaitGroup
	models := []transaction.ModelTransactionList{}
	for rs.Next() {
		wg.Add(1)
		model := transaction.ModelTransactionList{}
		err = rs.StructScan(&model)
		if err != nil {
			return nil, err
		}

		// items, _ := r.GetItemsID(model.ID)
		// model.Position = []transaction.ModelLocationPined{}
		// model.AssetItemID = items
		go r.getPositionRoutine(model.LocationPinedID, &models, model, &wg)
		// position, err := r.getPositionRoutine(model.LocationPinedID, &model.Position, &wg)
		// go r.getPositionRoutine(model.LocationPinedID, &models, model, &wg)
		// if err != nil {
		// 	return nil, err
		// }
		// model.Position = position

		// models = append(models, model)
	}
	wg.Wait()

	sort.SliceStable(models, func(p, q int) bool {
		return models[p].ID > models[q].ID
	})
	fmt.Println(" fininsh")
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil
}

func (r *transactionRepository) getPositionRoutine(ID int64, _models *[]transaction.ModelTransactionList, _model transaction.ModelTransactionList, wg *sync.WaitGroup) {

	defer wg.Done()
	sql1 := ` select ifnull(a.lat,'') as lat,
	ifnull(a.lng,'') as lng
	from location_pined a where a.id = ? `

	rs := r.dbx.QueryRowx(sql1, ID)
	// models := []transaction.ModelLocationPined{}

	model := transaction.ModelLocationPined{}

	err := rs.StructScan(&model)

	if err != nil {
		log.Println(err.Error())
		return
	}
	_model.Position = append(_model.Position, model)
	*_models = append(*_models, _model)
	return
}

func (r *transactionRepository) getPosition(ID int64) ([]transaction.ModelLocationPined, error) {

	sql1 := ` select ifnull(a.lat,'') as lat,
	ifnull(a.lng,'') as lng
	from location_pined a where a.id = ? `

	rs := r.dbx.QueryRowx(sql1, ID)

	models := []transaction.ModelLocationPined{}

	model := transaction.ModelLocationPined{}

	err := rs.StructScan(&model)

	if err != nil {
		return nil, err
	}

	models = append(models, model)

	return models, nil
}

func (r *transactionRepository) getAssetItemID(transactionID int64) ([]transaction.ModelAssetItem, error) {

	sql1 := `select a.asset_item_id from items a 
	left join transaction b on a.transaction_id = b.id where a.transaction_id = ? `

	rs, err := r.dbx.Queryx(sql1, transactionID)

	if err != nil {
		return nil, err
	}

	models := []transaction.ModelAssetItem{}
	for rs.Next() {

		model := transaction.ModelAssetItem{}

		err = rs.Scan(&model.ID)

		if err != nil {
			return nil, err
		}

		models = append(models, model)

	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil
}

func (r *transactionRepository) getTransactionRequest(ID int64) ([]transaction.ModelTransactionList, error) {

	sql1 := ` select a.id,
	ifnull(a.user_code,0) as user_code,
	ifnull(a.asset_id,0) as asset_id,
	ifnull(a.items_qty,0) as items_qty,
	ifnull(b.name,'') as asset_name,
	ifnull(b.description,'') as description,
	ifnull(c.name,'') as name,
	ifnull(a.process_id,0) as process_id,
	ifnull(a.location_pined_id,0) as location_pined_id,
	ifnull(a.message,'') as message,
	ifnull(a.response_message,'') as response_message,
	ifnull(a.created_at,'') as created_at,
	ifnull(a.updated_at,'') as updated_at,
	ifnull(a.notify_status,0) as notify_status,
	ifnull(a.transaction_status,0) as transaction_status,
	ifnull(a.reporter,'') as reporter
	from transaction a 
	left join asset b on a.asset_id = b.id
	left join user c on a.user_code = c.user_code 
	where a.id = ? and a.process_id = 1 order by a.id desc `

	rs := r.dbx.QueryRowx(sql1, ID)

	models := []transaction.ModelTransactionList{}

	model := transaction.ModelTransactionList{}
	err := rs.StructScan(&model)
	if err != nil {
		return nil, err
	}
	itemqty, err := r.getAssetItemID(model.ID)
	if err != nil {
		return nil, err
	}
	position, err := r.getPosition(model.LocationPinedID)

	if err != nil {
		return nil, err
	}

	model.Position = position
	model.Items = itemqty

	models = append(models, model)

	return models, nil
}
