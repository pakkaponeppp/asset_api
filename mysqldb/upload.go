package mysqldb

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/pakkaponeppp/asset_api/upload"
	"upper.io/db.v3/lib/sqlbuilder"
	"upper.io/db.v3/mysql"
)

//NewUploadRepository asset_api
func NewUploadRepository(Bdb *sqlx.DB) upload.Repository {

	bdb := Bdb.DB

	bidb, err := mysql.New(bdb)
	if err != nil {
		return nil
	}

	r := uploadRepository{bidb, Bdb}
	return &r
}

type uploadRepository struct {
	db  sqlbuilder.Database
	dbx *sqlx.DB
}
