package mysqldb

import (
	"database/sql"
	"errors"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/labstack/gommon/log"
	"gitlab.com/pakkaponeppp/asset_api/auth"
	"upper.io/db.v3/lib/sqlbuilder"
	"upper.io/db.v3/mysql"
)

type authRepository struct {
	dbx sqlbuilder.Database
	db  *sqlx.DB
}

//NewAuthRepository Mysql
func NewAuthRepository(Bdb *sqlx.DB) auth.Repository {

	bdb := Bdb.DB

	bidb, err := mysql.New(bdb)
	if err != nil {
		return nil
	}
	r := authRepository{bidb, Bdb}
	return &r
}

func (r *authRepository) FindUser(username string) (*auth.ModelAuth, error) {
	sql1 := `select   
	ifnull(a.user_code,'') as user_code,
	ifnull(a.password,'') as password,
	ifnull(a.name,'') as name,
	ifnull(a.username, '') as username,
	ifnull(a.active, 0) as active,
	ifnull(a.role_id,0) as role_id
	from user a where a.username = ?`

	rs, err := r.dbx.QueryRow(sql1, username)
	if err != nil {
		log.Error(err.Error())
		return nil, err
	}

	model := auth.ModelAuth{}
	err = rs.Scan(&model.UserCode, &model.Password, &model.Name, &model.Username, &model.Active, &model.RoleID)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("ไม่พบผู้ใช้งาน")
		}
		return nil, err
	}
	return &model, nil
}

func (r *authRepository) GetuserCodeRepo(userCode string) (*auth.ModelAuth, error) {

	resp, err := r.GetuserByCode(userCode)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
func (r *authRepository) CheckActiveByTokenRepo(tokenID string) (interface{}, error) {

	resp, err := r.GetActive(tokenID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *authRepository) GetActive(tokenID string) (bool, error) {

	var m struct {
		Active sql.NullInt64 `db:"active"`
	}

	sql1 := `select 
	ifnull(b.active,0) as active
	from user_access a 
	left join user b on a.user_code = b.user_code
	where a.access_token = ?`

	rs := r.db.QueryRowx(sql1, tokenID)

	err := rs.StructScan(&m)

	if err != nil {
		return false, err
	}

	if m.Active.Int64 == 0 {
		return false, auth.ErrInActive
	}

	return true, nil
}
func (r *authRepository) GetuserByCode(userCode string) (*auth.ModelAuth, error) {

	sql1 := `select 
	ifnull(a.username,'') as username,
	ifnull(a.password,'') as password,
	ifnull(a.user_code,'') as user_code,
	ifnull(a.active,0) as active,
	ifnull(a.name,'') as name
	from user a where a.user_code = ?`

	rs, err := r.dbx.QueryRow(sql1, userCode)
	if err != nil {
		log.Error(err.Error())
		return nil, err
	}

	model := auth.ModelAuth{}
	err = rs.Scan(&model.Username,
		&model.Password,
		&model.UserCode,
		&model.Active,
		&model.Name)
	if err != nil {
		if err == sql.ErrNoRows {

			return nil, errors.New("ไม่พบผู้ใชงาน")
		}
	}
	return &model, nil
}

func (r *authRepository) SaveTokenLogin(userCode string, token string) (interface{}, error) {
	user, err := r.GetuserByCode(userCode)
	if err != nil {
		return nil, err
	}
	q := r.dbx.
		InsertInto("user_access").
		Values(map[string]interface{}{
			"user_code":    user.UserCode,
			"name":         user.Name,
			"access_token": token,
			"access_time":  time.Now(),
		})
	_, err = q.Exec()
	if err != nil {
		return nil, err
	}
	return nil, nil
}

var dbname string = "user"

//GetToken sss
func (r *authRepository) GetToken(tokenID string) (*auth.Token, error) {

	var m struct {
		ID sql.NullInt64 `db:"id"`
		// Active sql.NullInt64 `db:"active"`
		// UserID     sql.NullInt64  `db:"user_id"`
		UserCode sql.NullString `db:"user_code"`
		// UserName   sql.NullString `db:"username"`
		Name       sql.NullString `db:"name"`
		Token      sql.NullString `db:"token"`
		AccessTime *time.Time     `db:"access_time"`
	}

	sql1 := `select ifnull(a.id,0) as id,
			ifnull(b.user_code,'') as user_code,
			ifnull(a.name,'') as name,
			ifnull(a.access_token,'') as token,
			a.access_time
			from user_access a 
			left join user b on a.user_code = b.user_code 
			where a.access_token = ? `

	rows := r.db.QueryRowx(sql1, tokenID)
	err := rows.StructScan(&m)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, auth.ErrTokenNotFound
		}
		return nil, err
	}

	expireTime := time.Now().Add(-(1 * 24 * time.Hour))

	if m.AccessTime.Before(expireTime) {
		return nil, auth.ErrTokenExpired
	}

	tk := auth.Token{ID: tokenID}

	// if m.UserID.Valid {
	// 	tk.UserID = m.UserID.Int64
	// } else {
	// 	tk.UserID = -1
	// }
	// if m.UserName.Valid {
	// 	tk.UserName = m.UserName.String
	// }

	if m.UserCode.Valid {
		tk.UserCode = m.UserCode.String
	}
	if m.Name.Valid {
		tk.Name = m.Name.String
	}

	return &tk, nil
}
