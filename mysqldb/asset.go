package mysqldb

import (
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/pakkaponeppp/asset_api/asset"
	"upper.io/db.v3/lib/sqlbuilder"
	"upper.io/db.v3/mysql"
)

//NewAssetRepository asset_api
func NewAssetRepository(Bdb *sqlx.DB) asset.Repository {

	bdb := Bdb.DB

	bidb, err := mysql.New(bdb)
	if err != nil {
		return nil
	}

	r := assetRepository{bidb, Bdb}
	return &r
}

type assetRepository struct {
	db  sqlbuilder.Database
	dbx *sqlx.DB
}

var loc, _ = time.LoadLocation("Asia/Bangkok")

////////////////////////////////////////////////////       CREATE        ////////////////////////////////////////////////////

//CREATE asset

func (r *assetRepository) CreateAssetRepo(model *asset.ModelAsset) (interface{}, error) {

	resp, err := r.CreateAsset(model)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
func (r *assetRepository) CreateAsset(model *asset.ModelAsset) (interface{}, error) {

	CheckedPercent := 0.0

	if model.Percents <= 100 {
		CheckedPercent = model.Percents
	} else {
		return nil, errors.New("ตัวเลขไม่ถูกต้อง ")
	}

	q := r.db.
		InsertInto("asset").
		Values(map[string]interface{}{
			"name":            model.AssetName,
			"category_id":     model.CategoryID,
			"sub_category_id": model.SubCategoryID,
			"barcode":         model.Barcode,
			"description":     model.Description,
			"url":             model.URL,
			"types":           model.Types,
			"percents":        CheckedPercent,
			"created_at":      time.Now().In(loc).Add(time.Minute * 10),
			"created_by":      model.UserCode,
		})
	result, err := q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	lastID, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}

	return lastID, nil
}

//--------------------------------------------------------------------------------------------------------------------------//

//CREATE asset_item
func (r *assetRepository) CreateAssetItemRepo(model *asset.ModelAssetItem) (interface{}, error) {

	resp, err := r.CreateAssetItem(model)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
func (r *assetRepository) GetTypes(ID int64) (*asset.ModelAsset, error) {

	sql1 := `select a.types as types from asset a where a.id = ?`

	rs := r.dbx.QueryRowx(sql1, ID)

	model := asset.ModelAsset{}

	err := rs.Scan(&model.Types)

	if err != nil {
		return nil, err
	}

	return &model, nil

}

func (r *assetRepository) CreateAssetItem(model *asset.ModelAssetItem) (interface{}, error) {

	var StartedWarranty interface{}
	var WarrantyDate interface{}
	SerialNumber := ""

	checktype, err := r.GetTypes(model.AssetID)
	if err != nil {
		return nil, err
	}

	if model.SerialNumber != "" && checktype.Types == 1 {

		SerialNumber = model.SerialNumber

	} else if model.SerialNumber != "" && checktype.Types != 1 {
		err := errors.New("Type ไม่ถูกต้อง")
		return nil, err
	} else if model.SerialNumber == "" && checktype.Types == 1 {
		err := errors.New("กรุณากรอก Serial Number")
		return nil, err
	}

	if model.StartedWarranty == "" {
		StartedWarranty = nil
		WarrantyDate = nil
	} else {
		StartedWarranty = model.StartedWarranty
		WarrantyDate = model.WarrantyDate
	}

	q := r.db.
		InsertInto("asset_item").
		Values(map[string]interface{}{
			"asset_id":          model.AssetID,
			"location_id":       model.LocationID,
			"status":            model.Status,
			"available":         model.Available,
			"serial_number":     SerialNumber,
			"detail":            model.Detail,
			"created_by":        model.Usercode,
			"prices":            model.Prices,
			"calculated_prices": model.CalculatedPrices,
			"buying_date":       model.BuyingDate,
			"warranty_month":    model.WarrantyMonth,
			"started_warranty":  StartedWarranty,
			"contact":           model.Contact,
			"warranty_date":     WarrantyDate,
			"conditions":        model.Conditions,
			"image_url":         model.ImageURL,
			"created_at":        time.Now().In(loc).Add(time.Minute * 10),
		})
	result, err := q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	resp, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}
	return resp, nil
}

//--------------------------------------------------------------------------------------------------------------------------//

//CREATE category
func (r *assetRepository) CreateAssetCategoryRepo(model *asset.ModelCategory) (interface{}, error) {
	_, err := r.CreateAssetCategory(model)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
func (r *assetRepository) CreateAssetCategory(model *asset.ModelCategory) (interface{}, error) {

	q := r.db.
		InsertInto("category").
		Values(map[string]interface{}{
			"name": model.CategoryName,
		})
	_, err := q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	return nil, nil
}

//--------------------------------------------------------------------------------------------------------------------------//

//CREATE sub_category

func (r *assetRepository) CreateAssetSubCategoryRepo(model *asset.ModelCategory) (interface{}, error) {
	_, err := r.CreateAssetSubCategory(model)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
func (r *assetRepository) CreateAssetSubCategory(model *asset.ModelCategory) (interface{}, error) {

	q := r.db.
		InsertInto("sub_category").
		Values(map[string]interface{}{
			"category_id": model.CategoryID,
			"sname":       model.SubCategoryName,
		})
	_, err := q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	return nil, nil
}

//--------------------------------------------------------------------------------------------------------------------------//

////////////////////////////////////////////////////       UPDATE        ////////////////////////////////////////////////////

//UPDATE asset

func (r *assetRepository) UpdateAssetRepo(req *asset.ModelAsset) (interface{}, error) {
	_, err := r.UpdateAsset(req)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
func (r *assetRepository) UpdateAsset(req *asset.ModelAsset) (interface{}, error) {

	CheckedPercent := 0.0

	if req.Percents <= 100.00 {
		CheckedPercent = req.Percents
	} else {
		return nil, errors.New("ตัวเลขไม่ถูกต้อง ")
	}

	q := r.db.Update("asset").
		Set("name", req.AssetName).
		Set("category_id", req.CategoryID).
		Set("sub_category_id", req.SubCategoryID).
		Set("description", req.Description).
		Set("percents", CheckedPercent).
		Set("url", req.URL).
		Set("edited_time", time.Now().In(loc).Add(time.Minute*10)).
		Set("edited_by", req.UserCode).
		Where("id = ?", req.ID)
	_, err := q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	return nil, nil
}

//--------------------------------------------------------------------------------------------------------------------------//

//UPDATE asset_item

func (r *assetRepository) UpdateAssetItemRepo(req *asset.ModelAssetItem) (interface{}, error) {
	_, err := r.UpdateAssetItem(req)
	if err != nil {
		return nil, err
	}

	return nil, nil
}
func (r *assetRepository) UpdateAssetItem(model *asset.ModelAssetItem) (interface{}, error) {

	var StartedWarranty interface{} = nil
	var WarrantyDate interface{} = nil

	checktype, err := r.GetTypes(model.AssetID)

	if err != nil {
		return nil, err
	}

	if checktype.Types != 1 {
		model.SerialNumber = ""

	}

	if model.StartedWarranty == "" {
		StartedWarranty = nil
		WarrantyDate = nil
	} else {
		StartedWarranty = model.StartedWarranty
		WarrantyDate = model.WarrantyDate
	}

	q := r.db.Update("asset_item").
		Set("location_id", model.LocationID).
		Set("serial_number", model.SerialNumber).
		Set("updated_at", time.Now().In(loc).Add(time.Minute*10)).
		Set("warranty_month", model.WarrantyMonth).
		Set("detail", model.Detail).
		Set("contact", model.Contact).
		Set("prices", model.Prices).
		Set("calculated_prices", model.CalculatedPrices).
		Set("buying_date", model.BuyingDate).
		Set("warranty_date", WarrantyDate).
		Set("warranty_month", model.WarrantyMonth).
		Set("started_warranty", StartedWarranty).
		Set("conditions", model.Conditions).
		Set("edited_by", model.EditedBy).
		Set("image_url", model.ImageURL).
		Where("id = ?", model.ID)

	_, err = q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	return nil, nil
}

//--------------------------------------------------------------------------------------------------------------------------//

//UPDATE category

func (r *assetRepository) UpdateAssetCategoryRepo(req *asset.ModelCategory) (interface{}, error) {
	_, err := r.UpdateAssetCategory(req)
	if err != nil {
		return nil, err
	}

	return nil, nil
}
func (r *assetRepository) UpdateAssetCategory(req *asset.ModelCategory) (interface{}, error) {

	q := r.db.Update("category").
		Set("name", req.CategoryName).
		Where("id = ?", req.ID)
	_, err := q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	return nil, nil
}

//--------------------------------------------------------------------------------------------------------------------------//

//UPDATE sub_category

func (r *assetRepository) UpdateAssetSubCategoryRepo(req *asset.ModelCategory) (interface{}, error) {
	_, err := r.UpdateAssetSubCategory(req)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
func (r *assetRepository) UpdateAssetSubCategory(req *asset.ModelCategory) (interface{}, error) {

	q := r.db.Update("sub_category").
		Set("sname", req.SubCategoryName).
		Where("id = ?", req.ID)

	_, err := q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	return nil, nil
}

//--------------------------------------------------------------------------------------------------------------------------//

//UPDATE status 0

func (r *assetRepository) UpdateAssetStatusRepo(req *asset.ModelAssetItem) (interface{}, error) {
	_, err := r.UpdateAssetStatus(req)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
func (r *assetRepository) UpdateAssetStatus(req *asset.ModelAssetItem) (interface{}, error) {

	q := r.db.Update("asset_item").
		Set("status", 0).
		Where("id = ?", req.ID)

	_, err := q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	return nil, nil
}

////////////////////////////////////////////////////       LIST        ////////////////////////////////////////////////////

//LIST category
func (r *assetRepository) ListAssetCategoryRepo(ID int64) (interface{}, error) {
	resp, err := r.ListAssetCategory(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *assetRepository) ListAssetCategory(ID int64) ([]asset.ModelCategory, error) {

	sql1 := ` select a.id,
	ifnull(a.name,'') as name
	from category a where a.active = 1`

	rs, err := r.dbx.Queryx(sql1)

	if err != nil {

		return nil, err
	}
	models := []asset.ModelCategory{}
	for rs.Next() {
		model := asset.ModelCategory{}
		err = rs.Scan(&model.ID, &model.CategoryName)
		if err != nil {
			return nil, err
		}
		models = append(models, model)
	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil
}

//--------------------------------------------------------------------------------------------------------------------------//

//LIST location

func (r *assetRepository) ListAssetLocationRepo(ID int64) (interface{}, error) {
	resp, err := r.ListAssetLocation(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *assetRepository) ListAssetLocation(ID int64) ([]asset.ModelLocation, error) {
	sql1 := ` select a.id,
	ifnull(a.name,'') as name
	from location a`

	rs, err := r.dbx.Queryx(sql1)

	if err != nil {

		return nil, err
	}
	models := []asset.ModelLocation{}
	for rs.Next() {
		model := asset.ModelLocation{}
		err = rs.Scan(&model.ID, &model.LocationName)
		if err != nil {
			return nil, err
		}
		models = append(models, model)
	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil
}

//--------------------------------------------------------------------------------------------------------------------------//

//LIST subcategory_id

func (r *assetRepository) ListAssetSubCategoryRepo(ID int64) (interface{}, error) {
	resp, err := r.ListAssetSubCategory(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *assetRepository) ListAssetSubCategory(ID int64) ([]asset.ModelCategory, error) {
	sql1 := ` select a.id,
	ifnull(a.sname,'') as sname,
	ifnull(a.category_id,'') as category_id,
	ifnull(b.name,'') as name from sub_category a
	inner join category b on a.category_id = b.id
	where b.id = ? and a.active = 1`

	rs, err := r.dbx.Queryx(sql1, ID)

	if err != nil {

		return nil, err
	}
	models := []asset.ModelCategory{}
	for rs.Next() {
		model := asset.ModelCategory{}
		err = rs.StructScan(&model)
		if err != nil {
			return nil, err
		}
		models = append(models, model)
	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil
}

//--------------------------------------------------------------------------------------------------------------------------//

//LIST asset

func (r *assetRepository) ListAssetRepo(ID int64) (interface{}, error) {
	resp, err := r.ListAsset(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *assetRepository) ListAsset(ID int64) ([]asset.ModelAsset, error) {

	sql1 := ` select a.id,
	ifnull(a.name,'') as name,
	ifnull(a.category_id,'') as category_id,
	ifnull(a.sub_category_id,'') as sub_category_id,
	ifnull(a.description,'') as description,
	ifnull(a.barcode,'') as barcode,
	ifnull(a.url,'') as url,
	ifnull(b.name,'') as category_name,
	ifnull(c.sname,'') as sub_category_name,
	ifnull(a.percents,'') as percents,

	( select count(*) from  asset_item asset where asset.asset_id = a.id and asset.active = 1 and asset.status = 1) as qty,
	( select count(*) from  asset_item asset where asset.asset_id = a.id and asset.active = 1 and asset.status = 0) as qty_damage
	
	from asset a 
	left join category b on a.category_id = b.id
	left join sub_category c on a.sub_category_id = c.id where a.active = 1`

	rs, err := r.dbx.Queryx(sql1)

	if err != nil {

		return nil, err
	}

	models := []asset.ModelAsset{}
	for rs.Next() {
		model := asset.ModelAsset{}
		err = rs.StructScan(&model)

		if err != nil {
			return nil, err
		}
		models = append(models, model)
	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil
}
func (r *assetRepository) ListAssetUserRepo(ID int64) (interface{}, error) {
	resp, err := r.ListAssetUser(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *assetRepository) ListAssetUser(ID int64) ([]asset.ModelAsset, error) {

	var sumary int = 0

	sql1 := ` select a.id,
	ifnull(a.name,'') as name,
	ifnull(a.category_id,'') as category_id,
	ifnull(a.sub_category_id,'') as sub_category_id,
	ifnull(a.description,'') as description,
	ifnull(a.barcode,'') as barcode,
	ifnull(a.url,'') as url,
	ifnull(b.name,'') as category_name,
	ifnull(c.sname,'') as sub_category_name,
	ifnull(a.percents,'') as percents,

	( select count(*) from  asset_item asset where asset.asset_id = a.id and asset.active = 1 and asset.status = 1) as qty,
	( select count(*) from  asset_item asset where asset.asset_id = a.id and asset.available = 1 and asset.active = 1 and asset.status = 1 ) as qty_available,
	( select count(*) from  asset_item asset where asset.asset_id = a.id and asset.active = 1 and asset.status = 0) as qty_damage
	
	from asset a 
	
	left join category b on a.category_id = b.id
	left join sub_category c on a.sub_category_id = c.id where a.active = 1`

	rs, err := r.dbx.Queryx(sql1)
	if err != nil {

		return nil, err
	}

	models := []asset.ModelAsset{}
	for rs.Next() {
		model := asset.ModelAsset{}
		err = rs.StructScan(&model)
		if err != nil {
			return nil, err
		}
		sql2 := `select ifnull((select sum(items_qty) from transaction a1 where a1.asset_id = ` + fmt.Sprint(model.ID) + ` and a1.transaction_status = 1 and a1.process_id = 1 ),0) as sumary `
		rs2 := r.dbx.QueryRowx(sql2)
		err = rs2.Scan(&sumary)
		if err != nil {
			return nil, err
		}
		model.Remain = model.QtyAvailable - sumary
		if model.Remain > 0 {
			models = append(models, model)
		}
		sumary = 0
	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil
}

//--------------------------------------------------------------------------------------------------------------------------//

//LIST asset status 0

func (r *assetRepository) ListAssetDamageRepo(ID int64) (interface{}, error) {
	resp, err := r.ListAssetDamage(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *assetRepository) ListAssetDamage(ID int64) ([]asset.ModelAsset, error) {

	sql1 := ` select a.id,
	ifnull(a.name,'') as name,
	ifnull(a.category_id,'') as category_id,
	ifnull(a.sub_category_id,'') as sub_category_id,
	ifnull(a.description,'') as description,
	ifnull(a.barcode,'') as barcode,
	ifnull(a.url,'') as url,
	ifnull(b.name,'') as category_name,
	ifnull(c.sname,'') as sub_category_name,
	ifnull(a.percents,'') as percents,

	( select count(*) from  asset_item asset where asset.asset_id = a.id and asset.active = 1 and asset.status = 0) as qty
	
	from asset a 
	left join category b on a.category_id = b.id
	left join sub_category c on a.sub_category_id = c.id where a.active = 1 and  ( select count(asset_id) from  asset_item asset where asset.asset_id = a.id and asset.active = 1 and asset.status = 0) > 0`

	rs, err := r.dbx.Queryx(sql1)

	if err != nil {

		return nil, err
	}

	models := []asset.ModelAsset{}
	for rs.Next() {
		model := asset.ModelAsset{}
		err = rs.StructScan(&model)

		if err != nil {
			return nil, err
		}

		models = append(models, model)

	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil
}

//LIST asset item

func (r *assetRepository) ListAssetItemRepo(ID int64) (interface{}, error) {
	resp, err := r.ListAssetItem(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *assetRepository) ListAssetItem(ID int64) ([]asset.ModelAssetItem, error) {

	sql1 := ` select a.id,
		ifnull(b.name,'') as name,
		ifnull(a.serial_number,'') as serial_number,
		ifnull(a.available,'') as available
		from asset_item a
		left join asset b on a.asset_id = b.id where b.id = ?`

	rs, err := r.dbx.Queryx(sql1, ID)

	if err != nil {

		return nil, err
	}

	models := []asset.ModelAssetItem{}
	for rs.Next() {
		model := asset.ModelAssetItem{}
		err = rs.StructScan(&model)

		if err != nil {
			return nil, err
		}
		models = append(models, model)
	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil
}

//--------------------------------------------------------------------------------------------------------------------------//
//LIST asset by asset_id

func (r *assetRepository) ListAvailableAssetItemIDRepo(ID int64) (interface{}, error) {
	resp, err := r.ListAvailableAssetID(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *assetRepository) ListAvailableAssetID(ID int64) ([]asset.ModelAssetItem, error) {

	sql1 := ` select a.id,
		ifnull(b.name,'') as name,
		ifnull(b.description,'') as description,
		ifnull(c.id,'') as location_id,
		ifnull(c.name,'') as location,
		ifnull(a.detail,'') as detail,
		ifnull(d.name,'') as category_name,
		ifnull(e.sname,'') as sub_category_name,
		ifnull(a.serial_number,'') as serial_number,
		ifnull(a.started_warranty,'') as started_warranty,
		ifnull(a.warranty_month,'') as warranty_month,
		ifnull(a.warranty_date,'') as warranty_date,
		ifnull(a.conditions,'') as conditions,
		ifnull(a.available,0) as available
		from asset_item a
		left join asset b on a.asset_id = b.id
		left join location c on a.location_id = c.id
		left join category d on b.category_id = d.id
		left join sub_category e on b.sub_category_id = e.id
		where b.id = ? and a.available = 1 and b.active = 1 and a.active = 1 and a.status = 1 `

	rs, err := r.dbx.Queryx(sql1, ID)

	if err != nil {
		return nil, err
	}

	models := []asset.ModelAssetItem{}
	for rs.Next() {
		model := asset.ModelAssetItem{}
		err = rs.StructScan(&model)
		if err != nil {
			return nil, err
		}
		if model.StartedWarranty != "" {
			warranty, err := r.CalWarranty(model.StartedWarranty, model.WarrantyMonth)
			if err != nil {
				return nil, err
			}
			model.Warranty = warranty
		}

		models = append(models, model)
	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil
}

//--------------------------------------------------------------------------------------------------------------------------//
//LIST asset by asset_id

func (r *assetRepository) ListAssetItemDamageRepo(ID int64) (interface{}, error) {
	resp, err := r.ListAssetItemDamage(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *assetRepository) ListAssetItemDamage(ID int64) ([]asset.ModelAssetItem, error) {

	sql1 := ` select a.id,
		ifnull(b.name,'') as name,
		ifnull(a.serial_number,'') as serial_number,
		ifnull(a.conditions,'') as conditions,
		ifnull(c.name,'') as location,
		ifnull(a.started_warranty,'') as started_warranty,
		ifnull(a.warranty_month,'') as warranty_month,
		ifnull(a.available,'') as available
		

		from asset_item a
		left join location c on a.location_id = c.id
		left join asset b on a.asset_id = b.id where b.id = ? and a.status = 0 and a.active = 1`

	rs, err := r.dbx.Queryx(sql1, ID)

	models := []asset.ModelAssetItem{}
	for rs.Next() {
		model := asset.ModelAssetItem{}
		err = rs.StructScan(&model)

		if err != nil {
			return nil, err
		}
		if model.StartedWarranty != "" {
			warranty, err := r.CalWarranty(model.StartedWarranty, model.WarrantyMonth)
			if err != nil {
				return nil, err
			}
			model.Warranty = warranty
		}
		models = append(models, model)
	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil
}

//--------------------------------------------------------------------------------------------------------------------------//
//LIST asset by asset_id

func (r *assetRepository) ListUnavailableAssetItemIDRepo(ID int64) (interface{}, error) {
	resp, err := r.ListUnavailableAssetID(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *assetRepository) ListUnavailableAssetID(ID int64) ([]asset.ModelAssetItem, error) {

	sql1 := ` select a.id,
		ifnull(b.name,'') as name,
		ifnull(b.description,'') as description,
		ifnull(c.id,'') as location_id,
		ifnull(c.name,'') as location,
		ifnull(a.detail,'') as detail,
		ifnull(d.name,'') as category_name,
		ifnull(e.sname,'') as sub_category_name,
		ifnull(a.serial_number,'') as serial_number,
		ifnull(a.started_warranty,'') as started_warranty,
		ifnull(a.warranty_date,'') as warranty_date,
		ifnull(a.warranty_month,'') as warranty_month,
		ifnull(a.conditions,'') as conditions,
		ifnull(a.available,0) as available
		from asset_item a
		left join asset b on a.asset_id = b.id
		left join location c on a.location_id = c.id
		left join category d on b.category_id = d.id
		left join sub_category e on b.sub_category_id = e.id
		where b.id = ? and a.available = 0 and b.active = 1 and a.active = 1 and a.status = 1`

	rs, err := r.dbx.Queryx(sql1, ID)

	models := []asset.ModelAssetItem{}
	for rs.Next() {
		model := asset.ModelAssetItem{}
		err = rs.StructScan(&model)

		if err != nil {
			return nil, err
		}
		users, err := r.GetUsers(model.ID)
		if err != nil {
			return nil, err
		}
		model.Username = users

		if model.StartedWarranty != "" {
			warranty, err := r.CalWarranty(model.StartedWarranty, model.WarrantyMonth)
			if err != nil {
				return nil, err
			}
			model.Warranty = warranty
		}

		models = append(models, model)
	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil
}

//--------------------------------------------------------------------------------------------------------------------------//
//GET location by id

func (r *assetRepository) GETLocationIDRepo(ID int64) (interface{}, error) {
	resp, err := r.GETLocationID(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *assetRepository) GETLocationID(ID int64) (*asset.ModelLocation, error) {

	sql1 := `select a.id,
	ifnull(a.name,'') as name from location a where a.id = ?`

	rs := r.dbx.QueryRowx(sql1, ID)

	model := asset.ModelLocation{}

	err := rs.StructScan(&model)

	if err != nil {

		return nil, err
	}

	return &model, nil
}

//--------------------------------------------------------------------------------------------------------------------------//
//GET asset by id

func (r *assetRepository) GETAssetIDRepo(ID int64) (interface{}, error) {
	resp, err := r.GETAssetID(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *assetRepository) GETAssetID(ID int64) (*asset.ModelAsset, error) {

	sql1 := ` select a.id,
		ifnull(a.name,'') as name,
		ifnull(a.description,'') as description,
		ifnull(a.barcode,'') as barcode,
		ifnull(a.url,'') as url,
		ifnull(b.name,'') as category_name,
		ifnull(c.sname,'') as sub_category_name,
		ifnull(a.types,'') as types,
		ifnull(a.percents,0) as percents,
		( select count(*) from  asset_item asset where asset.asset_id = a.id and asset.active = 1 ) as qty,
		( select count(*) from  asset_item a1 where a1.asset_id = a.id and a1.available = 1 and a1.active = 1 and a1.status = 1 ) as qty_available,
		( select count(*) from  asset_item a0 where a0.asset_id = a.id and a0.available = 0 and a0.active = 1 and a0.status = 1 ) as qty_unavailable,
		( select count(*) from  asset_item asset where asset.asset_id = a.id and asset.active = 1 and asset.status = 0) as qty_damage
		from asset a
		left join category b on a.category_id = b.id
		left join sub_category c on a.sub_category_id = c.id
		where a.id = ? and a.active = 1`

	rs := r.dbx.QueryRowx(sql1, ID)

	model := asset.ModelAsset{}

	err := rs.StructScan(&model)

	if err != nil {
		err = errors.New("ไม่พบสินทรัพย์")
		return nil, err
	}

	return &model, nil
}

//--------------------------------------------------------------------------------------------------------------------------//
//GET asset item by asset_id

func (r *assetRepository) GETAssetItemIDRepo(ID int64) (interface{}, error) {
	resp, err := r.GETAssetItemID(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *assetRepository) GetPosition(ID int64) ([]asset.ModelLocationPined, error) {

	sql1 := `select ifnull(c.lat,'') as lat,
	ifnull(c.lng,'') as lng
	from items a
	left join transaction b on a.transaction_id = b.id
	left join location_pined c on b.location_pined_id = c.id
	where a.asset_item_id = ? `

	rs := r.dbx.QueryRowx(sql1, ID)

	models := []asset.ModelLocationPined{}

	model := asset.ModelLocationPined{}

	err := rs.StructScan(&model)

	if err != nil {
		return nil, err
	}

	models = append(models, model)

	return models, nil
}
func (r *assetRepository) GetUsers(ID int64) ([]asset.Users, error) {

	sql1 := `select ifnull(c.name,'') as name,
	ifnull(d.name,'') as pos_name
	from items a
	left join transaction b on a.transaction_id = b.id
	left join user c on b.user_code = c.user_code
	left join position_name d on c.position_id = d.id
	where a.asset_item_id = ? and a.process_id = 1 and a.status = 1 order by a.id desc limit 1 `

	rs := r.dbx.QueryRowx(sql1, ID)

	models := []asset.Users{}

	model := asset.Users{}

	err := rs.StructScan(&model)

	if err != nil {
		return nil, err
	}
	posname := "(" + model.PosName + ")"
	model.Name = model.Name + posname

	models = append(models, model)

	return models, nil
}

func (r *assetRepository) GETAssetItemID(ID int64) ([]asset.ModelAssetItem, error) {

	sql1 := ` select a.id,
		ifnull(b.name,'') as name,
		ifnull(b.description,'') as description,
		ifnull(b.barcode,'') as barcode,
		ifnull(b.category_id,'') as category_id,
		ifnull(b.sub_category_id,'') as sub_category_id,
		ifnull(b.percents,'') as percents,
		ifnull(e.name,'') as category_name,
		ifnull(d.sname,'') as sub_category_name,
		ifnull(a.asset_id,'') as asset_id,
		ifnull(c.name,'') as location,
		ifnull(a.status,'') as status,
		ifnull(a.available,'') as available,
		ifnull(a.serial_number,'') as serial_number,
		ifnull(a.created_at,'') as created_at,
		ifnull(a.detail,'') as detail,
		ifnull(a.warranty_month,0) as warranty_month,
		ifnull(a.buying_date,'') as buying_date,
		ifnull(a.contact,'') as contact,
		ifnull(a.prices,0) as prices,
		ifnull(a.calculated_prices,0) as calculated_prices,
		ifnull(a.conditions,0) as conditions,
		ifnull(a.warranty_date,'') as warranty_date,
		ifnull(a.started_warranty,'') as started_warranty,
		ifnull(a.image_url,'') as image_url,
		ifnull(a.created_by,'') as created_by,
		ifnull(a.edited_by,'') as edited_by,
		ifnull(c.id,'') as location_id,
		ifnull(a.updated_at,'') as updated_at
		from asset_item a 
		left join location c on a.location_id = c.id
		left join asset b on a.asset_id = b.id 
		left join category e on b.category_id = e.id 
		left join sub_category d on b.sub_category_id = d.id 
		where a.id = ?`

	rs := r.dbx.QueryRowx(sql1, ID)

	models := []asset.ModelAssetItem{}

	model := asset.ModelAssetItem{}

	err := rs.StructScan(&model)

	if err != nil {
		return nil, err
	}

	now := time.Now()
	layout := "2006-01-02"
	warranty, err := time.Parse(layout, model.WarrantyDate)
	diff := warranty.Sub(now)
	monthleft := int(diff.Hours() / 24 / 30)
	daysleft := int(diff.Hours() / 24)
	hoursleft := diff.Hours()

	if hoursleft <= -24 && daysleft < 1 && monthleft < 1 {
		model.Warranty = "หมดประกันแล้ว"
	} else if hoursleft < 0 && daysleft < 1 && monthleft < 1 {
		model.Warranty = "หมดประกันวันนี้"
	} else if hoursleft < 24 && daysleft < 1 && monthleft < 1 {
		model.Warranty = "1 วัน "
	} else if hoursleft > 24 && daysleft >= 1 && monthleft < 1 {
		model.Warranty = strconv.Itoa(daysleft+1) + " วัน "
	} else if hoursleft > 24 && daysleft > 29 && monthleft < 12 {
		model.Warranty = strconv.Itoa(monthleft) + " เดือน "
	} else {
		yearlefts := strconv.Itoa(monthleft / 12)
		monthlefts := strconv.Itoa(monthleft % 12)
		if monthlefts == "0" {
			monthlefts = ""
		} else {
			monthlefts = monthlefts + " เดือน "
		}
		model.Warranty = yearlefts + " ปี " + monthlefts
	}

	checkavailable := model.Available
	if checkavailable != 1 {
		position, err := r.GetPosition(model.ID)
		if err != nil {
			return nil, err
		}
		users, err := r.GetUsers(model.ID)
		if err != nil {
			return nil, err
		}
		model.Username = users
		model.Position = position
	} else {
		model.Position = nil
	}

	models = append(models, model)

	return models, nil
}

//GET category by id
func (r *assetRepository) GETAssetCategoryIDRepo(ID int64) (interface{}, error) {
	resp, err := r.GETAssetCategoryID(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *assetRepository) GETAssetCategoryID(ID int64) (*asset.ModelCategory, error) {
	sql1 := ` select a.id,
	ifnull(a.name,'') as name
	from category a where a.id = ?`

	rs := r.dbx.QueryRowx(sql1, ID)

	model := asset.ModelCategory{}

	err := rs.StructScan(&model)

	if err != nil {
		err = errors.New("ไม่พบสินทรัพย์")
		return nil, err
	}

	return &model, nil
}

//GET subcategory_id
func (r *assetRepository) GETAssetSubCategoryIDRepo(ID int64) (interface{}, error) {
	resp, err := r.GETAssetSubCategoryID(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *assetRepository) GETAssetSubCategoryID(ID int64) (*asset.ModelCategory, error) {
	sql1 := `select a.id,
	ifnull(a.category_id,'') as category_id,
	ifnull(a.sname,'') as sname
	from sub_category a where a.id = ?`

	rs := r.dbx.QueryRowx(sql1, ID)

	model := asset.ModelCategory{}

	err := rs.StructScan(&model)

	if err != nil {
		err = errors.New("ไม่พบสินทรัพย์")
		return nil, err
	}

	return &model, nil
}

//---------------------------------------------------------DELETE-----------------------------------------------------------//

//DELETE asset
func (r *assetRepository) DeleteAssetIDRepo(ID int64) (interface{}, error) {
	_, err := r.DeleteAssetID(ID)
	if err != nil {
		return nil, err
	}
	return nil, nil

}

func (r *assetRepository) DeleteAssetID(ID int64) (interface{}, error) {

	q := r.db.Update("asset").
		Set("active", 0).
		Where("id = ?", ID)
	_, err := q.Exec()
	if err != nil {
		err = errors.New("รายการนี้ถูกลบไปแล้ว")
		return nil, err
	}
	return nil, nil
}

//--------------------------------------------------------------------------------------------------------------------------//
//DELETE Category
func (r *assetRepository) DeleteCategoryRepo(ID int64) error {
	go r.DeleteCategory(ID)

	return nil

}

func (r *assetRepository) DeleteCategory(ID int64) error {

	q := r.db.Update("category").
		Set("active", 0).
		Where("id = ?", ID)
	_, err := q.Exec()
	if err != nil {
		err = errors.New("รายการนี้ถูกลบไปแล้ว")
		return err
	}
	return nil
}

//DELETE SubCategory
func (r *assetRepository) DeleteSubCategoryRepo(ID int64) error {
	go r.DeleteSubCategory(ID)
	return nil

}

func (r *assetRepository) DeleteSubCategory(ID int64) error {

	q := r.db.Update("sub_category").
		Set("active", 0).
		Where("id = ?", ID)
	_, err := q.Exec()
	if err != nil {
		err = errors.New("รายการนี้ถูกลบไปแล้ว")
		return err
	}
	return nil
}

//DELETE asset_item
func (r *assetRepository) DeleteAssetItemRepo(ID int64) error {
	go r.DeleteAssetItem(ID)

	return nil

}

func (r *assetRepository) DeleteAssetItem(ID int64) error {

	q := r.db.Update("asset_item").
		Set("active", 0).
		Where("id = ?", ID)
	_, err := q.Exec()
	if err != nil {
		err = errors.New("รายการนี้ถูกลบไปแล้ว")
		return err
	}
	return nil
}

func (r *assetRepository) SearchAssetByCategoryRepo(categoryid int64, subcategoryid int64, status int64) (interface{}, error) {

	var search string = ""
	var Status string = ""
	if subcategoryid != 0 {
		search = " and c.id = " + fmt.Sprintln(subcategoryid)
	}

	if status == 0 {
		Status = " and (select count(asset_id) from  asset_item asset where asset.asset_id = a.id and asset.active = 1 and asset.status = 0) > 0  "
	} else if status == 1 {
		Status = ""
	}

	sql1 := `select a.id,
	ifnull(a.name,'') as name,
	ifnull(a.category_id,'') as category_id,
	ifnull(a.sub_category_id,'') as sub_category_id,
	ifnull(a.description,'') as description,
	ifnull(a.barcode,'') as barcode,
	ifnull(a.url,'') as url,
	( select count(asset_id) from  asset_item asset where asset.asset_id = a.id and asset.active = 1 and asset.status = ` + fmt.Sprintln(status) + `) as qty,
	( select count(*) from  asset_item asset where asset.asset_id = a.id and asset.active = 1 and asset.status = 0) as qty_damage,
	ifnull(b.name,'') as category_name,
	ifnull(c.sname,'') as sub_category_name,
	ifnull(a.active,0) as active,
	ifnull(a.percents,'') as percents
	from asset a 
	left join category b on b.id = a.category_id 
	left join sub_category c on c.id = a.sub_category_id
	where b.id = ` + fmt.Sprintln(categoryid) + search + Status + ` and a.active = 1 `

	log.Println(sql1)
	rs, err := r.db.Query(sql1)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	models := []asset.ModelAsset{}

	for rs.Next() {
		model := asset.ModelAsset{}
		err = rs.Scan(&model.ID,
			&model.AssetName,
			&model.CategoryID,
			&model.SubCategoryID,
			&model.Description,
			&model.Barcode,
			&model.URL,
			&model.Qty,
			&model.QtyDamage,
			&model.CategoryName,
			&model.SubCategoryName,
			&model.Active,
			&model.Percents)

		if err != nil {

			return nil, err
		}
		models = append(models, model)

	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil
}
func (r *assetRepository) UserSearchAssetByCategoryRepo(categoryid int64, subcategoryid int64, status int64) (interface{}, error) {
	var sumary int = 0
	var search string = ""
	var Status string = ""
	if subcategoryid != 0 {
		search = " and c.id = " + fmt.Sprintln(subcategoryid)
	}

	if status == 0 {
		Status = " and (select count(asset_id) from  asset_item asset where asset.asset_id = a.id and asset.active = 1 and asset.status = 0) > 0  "
	} else if status == 1 {
		Status = ""
	}

	sql1 := `select a.id,
	ifnull(a.name,'') as name,
	ifnull(a.category_id,'') as category_id,
	ifnull(a.sub_category_id,'') as sub_category_id,
	ifnull(a.description,'') as description,
	ifnull(a.barcode,'') as barcode,
	ifnull(a.url,'') as url,
	( select count(asset_id) from  asset_item asset where asset.asset_id = a.id and asset.active = 1 and asset.status = ` + fmt.Sprintln(status) + `) as qty,
	( select count(*) from  asset_item asset where asset.asset_id = a.id and asset.available = 1 and asset.active = 1 and asset.status = 1 ) as qty_available,
	( select count(*) from transaction a1 where a1.asset_id = a.id and a1.transaction_status = 1 ) as qty_ts,
	ifnull(b.name,'') as category_name,
	ifnull(c.sname,'') as sub_category_name,
	ifnull(a.active,0) as active,
	ifnull(a.percents,'') as percents
	from asset a 
	left join category b on b.id = a.category_id 
	left join sub_category c on c.id = a.sub_category_id
	where b.id = ` + fmt.Sprintln(categoryid) + search + Status + ` and a.active = 1 `

	log.Println(sql1)
	rs, err := r.db.Query(sql1)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	models := []asset.ModelAsset{}

	for rs.Next() {
		model := asset.ModelAsset{}
		err = rs.Scan(&model.ID,
			&model.AssetName,
			&model.CategoryID,
			&model.SubCategoryID,
			&model.Description,
			&model.Barcode,
			&model.URL,
			&model.Qty,
			&model.QtyAvailable,
			&model.QtyTransaction,
			&model.CategoryName,
			&model.SubCategoryName,
			&model.Active,
			&model.Percents)

		if err != nil {

			return nil, err
		}
		sql2 := `select (select sum(items_qty) from transaction a1 where a1.asset_id = ` + fmt.Sprint(model.ID) + ` and a1.transaction_status = 1 and a1.process_id = 1 ) as sumary `
		rs2 := r.dbx.QueryRowx(sql2)
		err = rs2.Scan(&sumary)
		model.Remain = model.QtyAvailable - sumary
		if model.Remain > 0 {
			models = append(models, model)
		}
		sumary = 0

	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil
}

func (r *assetRepository) CountAssetRepo(active int64, search string) (int64, error) {
	var TypeSearch string = ""
	var count int64 = 0
	if active == 0 {
		TypeSearch = " a.active = 0 and "
	} else if active == 1 {
		TypeSearch = " a.active = 1 and "
	} else {
		TypeSearch = " "
	}
	sql1 := `select count(*) as count
		from  asset a   
		where` + TypeSearch + ` (` + search + `)`
	rs := r.dbx.QueryRow(sql1)

	err := rs.Scan(&count)
	if err != nil {

		return 0, err
	}
	return count, nil
}

func (r *assetRepository) ListAssetSearchRepo(active int64, search string, limit int64, status int64) (interface{}, error) {
	var TypeSearch string = ""
	var OrderBy string = ""
	var Status string = ""
	if active == 0 {
		TypeSearch = " a.active = 0 and "
		OrderBy = "order by a.id asc,  a.active "
	} else if active == 1 {
		TypeSearch = " a.active = 1 and "

		OrderBy = "order by a.id asc, a.active "
	} else {
		TypeSearch = " "

		OrderBy = "order by a.id asc, a.active "
	}

	if status == 0 {
		Status = "( select count(asset_id) from  asset_item asset where asset.asset_id = a.id and asset.active = 1 and asset.status = 0) > 0 and "
	} else if status == 1 {
		Status = ""
	}
	sql1 := `
	select a.id,
	ifnull(a.name,'') as name,
	ifnull(a.category_id,'') as category_id,
	ifnull(a.sub_category_id,'') as sub_category_id,
	ifnull(a.description,'') as description,
	ifnull(a.barcode,'') as barcode,
	ifnull(a.url,'') as url,
	( select count(asset_id) from  asset_item asset where asset.asset_id = a.id and asset.active = 1 and asset.status = ` + fmt.Sprintln(status) + `) as qty,
	( select count(*) from  asset_item asset where asset.asset_id = a.id and asset.active = 1 and asset.status = 0) as qty_damage,
	ifnull(b.name,'') as category_name,
	ifnull(c.sname,'') as sub_category_name,
	ifnull(a.active,0) as active,
	ifnull(a.percents,'') as percents
	
	from asset a 
	left join category b on b.id = a.category_id
	left join sub_category c on c.id = a.sub_category_id
	
	
	where  ` + Status + TypeSearch + `(` + search + `) ` + OrderBy + ` limit ` + fmt.Sprintln(limit)
	log.Println(sql1)
	rs, err := r.dbx.Query(sql1)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	models := []asset.ModelAsset{}

	for rs.Next() {
		model := asset.ModelAsset{}
		err = rs.Scan(&model.ID,
			&model.AssetName,
			&model.CategoryID,
			&model.SubCategoryID,
			&model.Description,
			&model.Barcode,
			&model.URL,
			&model.Qty,
			&model.QtyDamage,
			&model.CategoryName,
			&model.SubCategoryName,
			&model.Active,
			&model.Percents)

		if err != nil {

			return nil, err
		}

		models = append(models, model)

	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil
}
func (r *assetRepository) UserSearchAssetRepo(active int64, search string, limit int64, status int64) (interface{}, error) {

	var sumary int = 0
	var TypeSearch string = ""
	var OrderBy string = ""
	var Status string = ""
	if active == 0 {
		TypeSearch = " a.active = 0 and "
		OrderBy = "order by a.id asc,  a.active "
	} else if active == 1 {
		TypeSearch = " a.active = 1 and "

		OrderBy = "order by a.id asc, a.active "
	} else {
		TypeSearch = " "

		OrderBy = "order by a.id asc, a.active "
	}

	if status == 0 {
		Status = "( select count(asset_id) from  asset_item asset where asset.asset_id = a.id and asset.active = 1 and asset.status = 0) > 0 and "
	} else if status == 1 {
		Status = ""
	}
	sql1 := `
	select a.id,
	ifnull(a.name,'') as name,
	ifnull(a.category_id,'') as category_id,
	ifnull(a.sub_category_id,'') as sub_category_id,
	ifnull(a.description,'') as description,
	ifnull(a.barcode,'') as barcode,
	ifnull(a.url,'') as url,
	( select count(asset_id) from  asset_item asset where asset.asset_id = a.id and asset.active = 1 and asset.status = ` + fmt.Sprintln(status) + `) as qty,
	( select count(*) from  asset_item asset where asset.asset_id = a.id and asset.available = 1 and asset.active = 1 and asset.status = 1 ) as qty_available,
	( select count(*) from transaction a1 where a1.asset_id = a.id and a1.transaction_status = 1 ) as qty_ts,
	ifnull(b.name,'') as category_name,
	ifnull(c.sname,'') as sub_category_name,
	ifnull(a.active,0) as active,
	ifnull(a.percents,'') as percents
	
	from asset a 
	left join category b on b.id = a.category_id
	left join sub_category c on c.id = a.sub_category_id
	
	
	where  ` + Status + TypeSearch + `(` + search + `) ` + OrderBy + ` limit ` + fmt.Sprintln(limit)
	log.Println(sql1)
	rs, err := r.dbx.Query(sql1)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	models := []asset.ModelAsset{}

	for rs.Next() {
		model := asset.ModelAsset{}
		err = rs.Scan(&model.ID,
			&model.AssetName,
			&model.CategoryID,
			&model.SubCategoryID,
			&model.Description,
			&model.Barcode,
			&model.URL,
			&model.Qty,
			&model.QtyAvailable,
			&model.QtyTransaction,
			&model.CategoryName,
			&model.SubCategoryName,
			&model.Active,
			&model.Percents)

		if err != nil {

			return nil, err
		}

		sql2 := `select (select sum(items_qty) from transaction a1 where a1.asset_id = ` + fmt.Sprint(model.ID) + ` and a1.transaction_status = 1 and a1.process_id = 1 ) as sumary `
		rs2 := r.dbx.QueryRowx(sql2)
		err = rs2.Scan(&sumary)
		model.Remain = model.QtyAvailable - sumary
		if model.Remain > 0 {
			models = append(models, model)
		}
		sumary = 0

		// remain, _ := r.GetRemain(model.ID, model.Remain, model.QtyAvailable)

		// if remain > 0 {
		// 	models = append(models, model)
		// }

	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil
}

func (r *assetRepository) GetRemain(ID int64, Remain int, QtyAvailable int) (int, error) {
	sumary := 0
	sql2 := `select (select sum(items_qty) from transaction a1 where a1.asset_id = ` + fmt.Sprint(ID) + ` and a1.transaction_status = 1 and a1.process_id = 1 ) as sumary `
	rs2 := r.dbx.QueryRowx(sql2)
	err := rs2.Scan(&sumary)
	if err != nil {
		return 0, err
	}
	Remain = QtyAvailable - sumary
	if Remain > 0 {
		return Remain, nil
	}
	sumary = 0
	return sumary, nil
}
func (r *assetRepository) ListByCategoryRepo(model *asset.ModelCategory) (interface{}, error) {
	resp, err := r.ListCategoryBar(model)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *assetRepository) ListSubCats(ID int64) ([]asset.ModelSubCat, error) {

	sql1 := ` select a.id,
	ifnull(b.name,'') as category_name,
	ifnull(a.sname,'') as sub_category_name,
	ifnull(b.id,'') as category_id

	from sub_category a
	left join category b on a.category_id = b.id
	where b.id = ? and a.active = 1 `

	rs, err := r.dbx.Queryx(sql1, ID)
	if err != nil {
		return nil, err
	}
	models := []asset.ModelSubCat{}
	for rs.Next() {

		model := asset.ModelSubCat{}
		err = rs.Scan(&model.ID,
			&model.CategoryName,
			&model.SubCateName,
			&model.CategoryID,
		)

		if err != nil {
			return nil, err
		}

		models = append(models, model)

	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil
}

func (r *assetRepository) ListCategoryBar(model *asset.ModelCategory) (interface{}, error) {

	sql1 := ` select a.id,
	ifnull(a.name,'') as name
	from category a where a.active = 1 `

	rs, err := r.db.Query(sql1)
	models := []asset.ModelCategory{}
	for rs.Next() {
		model := asset.ModelCategory{}
		err = rs.Scan(&model.ID,
			&model.CategoryName,
		)
		if err != nil {
			return nil, err
		}

		sub, err := r.ListSubCats(model.ID)
		if err != nil {
			return nil, err
		}
		model.SubCats = sub

		models = append(models, model)
	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()

	return models, nil
}

func (r *assetRepository) CalWarrantyRepo(startdate string, month int) (interface{}, error) {

	layout := "2006-01-02"

	Caldate, err := time.Parse(layout, startdate)

	if err != nil {
		return nil, err
	}

	EndDate := Caldate.AddDate(0, month, 0)

	EndDateFormated := EndDate.Format(layout)

	return EndDateFormated, nil
}
func (r *assetRepository) CalWarranty(startdate string, month int) (string, error) {

	layout := "2006-01-02"

	Caldate, err := time.Parse(layout, startdate)

	if err != nil {
		return "", err
	}

	EndDate := Caldate.AddDate(0, month, 0)

	EndDateFormated := EndDate.Format(layout)

	now := time.Now()
	warranty, err := time.Parse(layout, EndDateFormated)
	diff := warranty.Sub(now)
	monthleft := int(diff.Hours() / 24 / 30)
	daysleft := int(diff.Hours() / 24)
	hoursleft := diff.Hours()
	_warranty := ""

	if hoursleft <= -24 && daysleft < 1 && monthleft < 1 {
		_warranty = "หมดประกันแล้ว"
	} else if hoursleft < 0 && daysleft < 1 && monthleft < 1 {
		_warranty = "หมดประกันวันนี้"
	} else if hoursleft < 24 && daysleft < 1 && monthleft < 1 {
		_warranty = "1 วัน "
	} else if hoursleft > 24 && daysleft >= 1 && monthleft < 1 {
		_warranty = strconv.Itoa(daysleft+1) + " วัน "
	} else if hoursleft > 24 && daysleft > 29 && monthleft < 12 {
		_warranty = strconv.Itoa(monthleft) + " เดือน "
	} else {
		yearlefts := strconv.Itoa(monthleft / 12)
		monthlefts := strconv.Itoa(monthleft % 12)
		if monthlefts == "0" {
			monthlefts = ""
		} else {
			monthlefts = monthlefts + " เดือน "
		}
		_warranty = yearlefts + " ปี " + monthlefts
	}

	return _warranty, nil
}

func (r *assetRepository) CalPriceRepo(buyingdate string, percents float64, prices float64) (interface{}, error) {

	now := time.Now()
	layout := "2006-01-02"
	startdate, err := time.Parse(layout, buyingdate)
	if err != nil {
		return nil, err
	}
	diff := startdate.Sub(now)
	pastdays := int(diff.Hours()) / -24
	pastmonth := pastdays / 30
	pastyear := pastmonth / 12

	calpercents := percents * (float64(pastyear))
	calprices := prices - (prices * float64(calpercents/100))

	type calpr struct {
		Percent float64 `json:"percent"`
		Priced  float64 `json:"priced"`
	}

	resp := calpr{}

	if calpercents < 100 {
		resp.Percent = calpercents
	} else if calpercents >= 100 {
		resp.Percent = 100
	}

	if calprices > 0 {
		resp.Priced = calprices
	} else if calprices < 0 {
		resp.Priced = 0
	}

	return resp, nil

}

func (r *assetRepository) SearchAssetItemRepo(assetid int64, search string) (interface{}, error) {

	sql1 := `select a.id,
		ifnull(c.id,'') as location_id,
		ifnull(b.name,'') as name,
		ifnull(c.name,'') as location,
		ifnull(a.serial_number,'') as serial_number,
		ifnull(a.conditions,0) as conditions,
		ifnull(a.available,'') as available
		from asset_item a
		left join asset b on a.asset_id = b.id
		left join location c on a.location_id = c.id 
		where b.id = ? and a.available = 1 and b.active = 1 and a.active = 1 and a.status = 1 and (` + search + `) `

	log.Println(sql1)
	rs, err := r.dbx.Queryx(sql1, assetid)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	models := []asset.ModelAssetItem{}

	for rs.Next() {
		model := asset.ModelAssetItem{}
		err = rs.StructScan(&model)

		if err != nil {

			return nil, err
		}

		models = append(models, model)

	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil
}
func (r *assetRepository) CountAssetItemForDashboardRepo(ID int64) (interface{}, error) {

	resp, err := r.CountAssetItemForDashboard(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil

}

func (r *assetRepository) CountAssetItemForDashboard(ID int64) (interface{}, error) {

	sql1 := `select (select count(*) from asset_item where active = 1) as qty_all,
					(select count(*) from asset_item where active = 1 and available = 1 and status = 1) as qty_available,
					(select count(*) from asset_item where active = 1 and available = 0) as qty_unavailable,
					(select count(*) from asset_item where active = 1 and status = 0) as qty_damage,
					(select count(*) from transaction where process_id = 1) as qty_transaction_request_all,
					(select count(*) from transaction where process_id = 1 and transaction_status = 1) as qty_transaction_request,
					(select count(*) from transaction where process_id = 2 ) as qty_transaction_return_all,
					(select count(*) from transaction where process_id = 2 and transaction_status = 1) as qty_transaction_return,
					(select count(*) from transaction where process_id = 3 ) as qty_transaction_report_all,
					(select count(*) from transaction where process_id = 3 and transaction_status = 1) as qty_transaction_report `

	dashboard := asset.DashBoard{}

	qtyitem := asset.QtyDashboard{}

	rs := r.dbx.QueryRowx(sql1)

	err := rs.StructScan(&qtyitem)
	if err != nil {
		return nil, err
	}

	qm, err := r.CountQtyEachMonth(ID)
	if err != nil {
		return nil, err
	}

	dashboard.Value = qm
	dashboard.QtyItem = append(dashboard.QtyItem, qtyitem)

	return dashboard, nil
}

func (r *assetRepository) CountQtyEachMonth(Year int64) (_value [12]int64, err error) {

	sql1 := `select count(*) as value ,ifnull(DATE_FORMAT(a.created_at,"%Y-%m"),'') as month,
	ifnull(DATE_FORMAT(a.created_at,"%m"),'') as month_int from transaction a where ifnull(DATE_FORMAT(a.created_at,"%Y"),'') = ` + fmt.Sprint(Year) + `
group by ifnull(DATE_FORMAT(a.created_at,"%Y-%m"),''),ifnull(DATE_FORMAT(a.created_at,"%m"),'')`

	rs, err := r.dbx.Queryx(sql1)

	if err != nil {
		return _value, err
	}

	for rs.Next() {

		model := asset.DashBoardMount{}

		err = rs.StructScan(&model)
		if err != nil {
			return _value, err
		}
		_value[model.MonthInt-1] = model.Value
	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return _value, nil
}

func (r *assetRepository) ListUserRepo(ID int64) (interface{}, error) {

	resp, err := r.ListUser(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (r *assetRepository) ListUser(ID int64) (interface{}, error) {

	sql1 := `select a.id,
ifnull(a.user_code,'') as user_code,
ifnull(a.role_id,0) as role_id,
ifnull(a.position_id,0) as position_id,
ifnull(b.name,'') as position_name,
ifnull(a.username,'') as username,
ifnull(a.password,'') as password,
ifnull(a.active,0) as active,
ifnull(a.name,'') as name
from user a left join position_name b on a.position_id = b.id `

	rs, err := r.dbx.Queryx(sql1)
	models := []asset.User{}
	if err != nil {
		return nil, err
	}

	for rs.Next() {
		model := asset.User{}
		err = rs.StructScan(&model)

		if err != nil {
			return nil, err
		}

		var istrue bool

		if model.Active == 1 {
			istrue = true
		} else if model.Active == 0 {
			istrue = false
		}

		model.IsActive = istrue

		models = append(models, model)

	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil

}

func (r *assetRepository) AddUserRepo(model *asset.User) (interface{}, error) {

	resp, err := r.AddUser(model)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (r *assetRepository) UpdateUserRepo(model *asset.User) (interface{}, error) {

	resp, err := r.UpdateUser(model)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (r *assetRepository) ListUserPositionRepo(ID int64) (interface{}, error) {

	resp, err := r.ListUserPosition(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (r *assetRepository) ListUserNameRepo(ID int64) (interface{}, error) {

	resp, err := r.ListUserName(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (r *assetRepository) AssetDamageToAssetRepo(ID int64, Usercode string) (interface{}, error) {
	resp, err := r.AssetDamageToAsset(ID, Usercode)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (r *assetRepository) ManageUserRepo(Usercode string, Status int64) (interface{}, error) {
	resp, err := r.ManageUserActive(Usercode, Status)
	if err != nil {
		return nil, err
	}
	return resp, nil

}
func (r *assetRepository) GetUserDetailRepo(Usercode string) (interface{}, error) {
	resp, err := r.GetUserDetail(Usercode)
	if err != nil {
		return nil, err
	}
	return resp, nil

}

func (r *assetRepository) ManageUserActive(Usercode string, Status int64) (interface{}, error) {

	switch Status {
	case 1:
		go r.ManageUserActive1(Usercode)
	case 0:
		go r.ManageUserActive0(Usercode)
	}

	return nil, nil

}
func (r *assetRepository) GetUserDetail(Usercode string) (*asset.UserStruct, error) {

	var poID int64 = 0

	sql1 := `select a.id,
	ifnull(a.user_code,'') as user_code,
	ifnull(a.position_id,0) as position_id,
	ifnull(a.username,'') as username,
	ifnull(a.password,'') as password,
	ifnull(a.name,'') as name
	from user a
	where a.user_code = ?
	`

	model := asset.UserStruct{}
	rs := r.dbx.QueryRowx(sql1, Usercode)

	err := rs.Scan(&model.ID, &model.Usercode, &poID, &model.Username, &model.Password, &model.Name)
	if err != nil {
		return nil, err
	}

	positionSt, err := r.GetPositionStruct(poID)

	if err != nil {
		return nil, err
	}
	RoleSt, err := r.GetRoleStruct(Usercode)

	if err != nil {
		return nil, err
	}

	model.Position = positionSt
	model.Role = RoleSt

	return &model, nil

}

func (r *assetRepository) GetPositionStruct(ID int64) (model asset.Position, err error) {

	sql1 := `select id,ifnull(name,'') as position_name from position_name where id = ?`

	models := asset.Position{}

	rs := r.dbx.QueryRowx(sql1, ID)

	errss := rs.StructScan(&models)

	if errss != nil {
		return model, err
	}

	return models, nil

}
func (r *assetRepository) GetRoleStruct(Usercode string) (model asset.Role, err error) {

	sql1 := `select role_id from user where user_code = ?`

	rs := r.dbx.QueryRowx(sql1, Usercode)

	errss := rs.StructScan(&model)

	fmt.Println()

	if errss != nil {
		return model, err
	}

	if model.RoleID == 1 {
		model.Name = "แอดมิน"
	} else if model.RoleID == 2 {
		model.Name = "ผู้ใช้งาน"
	} else {
		return model, err
	}

	return model, nil

}

func (r *assetRepository) ManageUserActive1(Usercode string) error {

	q := r.db.Update("user").Set("active", 1).
		Where("user_code = ?", Usercode)
	_, err := q.Exec()
	if err != nil {
		return err
	}

	return nil

}
func (r *assetRepository) ManageUserActive0(Usercode string) error {

	q := r.db.Update("user").Set("active", 0).
		Where("user_code = ?", Usercode)
	_, err := q.Exec()
	if err != nil {
		return err
	}

	return nil

}
func (r *assetRepository) AssetDamageToAsset(ID int64, Usercode string) (interface{}, error) {

	q := r.db.Update("asset_item").Set("status", 1).
		Set("edited_by", Usercode).
		Set("updated_at", time.Now().Add(time.Minute*10)).
		Where("id = ?", ID)
	_, err := q.Exec()
	if err != nil {
		return nil, err
	}

	return nil, nil

}
func (r *assetRepository) ListUserPosition(ID int64) (interface{}, error) {

	sql1 := `select a.id,
ifnull(a.name,'') as position_name from position_name a
 `

	rs, err := r.dbx.Queryx(sql1)
	models := []asset.Position{}
	if err != nil {
		return nil, err
	}

	for rs.Next() {
		model := asset.Position{}
		err = rs.StructScan(&model)

		if err != nil {
			return nil, err
		}

		models = append(models, model)

	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil

}
func (r *assetRepository) ListUserName(ID int64) (interface{}, error) {

	sql1 := `select a.id,
ifnull(a.name,'') as name from user a
 `

	rs, err := r.dbx.Queryx(sql1)
	models := []asset.NameList{}
	if err != nil {
		return nil, err
	}

	for rs.Next() {
		model := asset.NameList{}
		err = rs.StructScan(&model)

		if err != nil {
			return nil, err
		}

		models = append(models, model)

	}
	defer func() {
		err := rs.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	return models, nil

}
func (r *assetRepository) AddUser(model *asset.User) (interface{}, error) {

	tt := time.Now().Add(time.Hour * 99999)

	ss := tt.Format("2006-04-05")

	randomKey := strings.Replace(ss, "-", "", -1)

	UID := "UID696" + randomKey

	q := r.db.
		InsertInto("user").
		Values(map[string]interface{}{
			"user_code":   UID,
			"username":    model.Username,
			"password":    model.Password,
			"position_id": model.PositionID,
			"role_id":     model.RoleID,
			"name":        model.Name,
			"active":      1,
		})
	_, err := q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	return nil, nil

}
func (r *assetRepository) UpdateUser(model *asset.User) (interface{}, error) {

	q := r.db.Update("user").
		Set("name", model.Name).
		Set("position_id", model.PositionID).
		Set("role_id", model.RoleID).
		Set("username", model.Username).
		Set("password", model.Password).
		Where("user_code = ?", model.Usercode)
	_, err := q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	return nil, nil

}
