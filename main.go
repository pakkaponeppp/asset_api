package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"

	"cloud.google.com/go/storage"
	"github.com/acoshift/middleware"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"gitlab.com/pakkaponeppp/asset_api/asset"
	"gitlab.com/pakkaponeppp/asset_api/auth"
	"gitlab.com/pakkaponeppp/asset_api/mysqldb"
	"gitlab.com/pakkaponeppp/asset_api/report"
	"gitlab.com/pakkaponeppp/asset_api/transaction"
	"gitlab.com/pakkaponeppp/asset_api/upload"
	"google.golang.org/api/option"
)

// PRODUCT

var (
	mysql   *sqlx.DB
	dbHost  = "venus.nopadol.com"
	dbUser  = "intern"
	dbName  = "intern"
	dbPort  = "3306"
	dbPass  = "intern"
	appPort = "3000"
)

//DEV

// var (
// 	mysql   *sqlx.DB
// 	dbHost  = "192.168.0.70"
// 	dbUser  = "intern"
// 	dbName  = "intern"
// 	dbPort  = "3306"
// 	dbPass  = "intern"
// 	appPort = "3001"
// )

// ConnectDB : Connect to intern db
func ConnectDB(user string, pass string, dbName string, host string, port string) (db *sqlx.DB, err error) {
	dsn := user + ":" + pass + "@tcp(" + host + ":" + port + ")/" + dbName + "?parseTime=true&loc=Asia%2FBangkok&charset=utf8"
	fmt.Println(dsn, "DBName =", host, dbName, "Port =", appPort)
	fmt.Println("OK NUMBER ONE !")
	db, err = sqlx.Connect("mysql", dsn)
	if err != nil {
		fmt.Println("sql error =", err)
		return nil, err
	}
	db.Exec("use " + dbName)
	return db, err
}

func getPort() string {
	var port = os.Getenv("PORT")
	if port == "" {
		port = appPort
		fmt.Println(" PORT " + port)
	}
	return ":" + port
}

func main() {

	mysqlCon, err := ConnectDB(dbUser, dbPass, dbName, dbHost, dbPort)

	mysql = mysqlCon

	authRepo := mysqldb.NewAuthRepository(mysql)
	authService, err := auth.NewService(authRepo)
	must(err)
	assetRepo := mysqldb.NewAssetRepository(mysql)
	assetService, err := asset.NewService(assetRepo)
	must(err)
	transactionRepo := mysqldb.NewTransactionRepository(mysql)
	transactionService, err := transaction.NewService(transactionRepo)
	must(err)
	reportRepo := mysqldb.NewReportRepository(mysql)
	reportService, err := report.NewService(reportRepo)
	must(err)
	storageClient, err := storage.NewClient(context.Background(), option.WithCredentialsFile("sck/Ckey.json"))
	must(err)
	uploadRepo := mysqldb.NewUploadRepository(mysql)
	uploadService, err := upload.NewService(storageClient, uploadRepo, "np-storage-it.appspot.com")
	must(err)

	mux := http.NewServeMux()
	mux.HandleFunc("/", healthCheckHandler)
	mux.Handle("/asset_api/", http.StripPrefix("/asset_api", asset.MakeHandler(assetService)))
	mux.Handle("/transaction_api/", http.StripPrefix("/transaction_api", transaction.MakeHandler(transactionService)))
	mux.Handle("/upload/", http.StripPrefix("/upload", upload.MakeHandler(uploadService)))
	mux.Handle("/report/", http.StripPrefix("/report", report.MakeHandler(reportService)))
	mux.Handle("/auth/", http.StripPrefix("/auth", auth.MakeHandler(authService)))

	corsConfig := middleware.CORSConfig{
		AllowAllOrigins: true,
		AllowMethods: []string{
			http.MethodGet,
			http.MethodPost,
			http.MethodPut,
			http.MethodPatch,
			http.MethodDelete,
			http.MethodOptions,
			http.MethodHead,
		},

		AllowHeaders: []string{
			"Content-Type",
			"Authorization",
			"Token",
			"Access-Token",
			"X-Access-Token",
			"Access-Control-Allow-Origin",
			"Access-Control-Allow-Headers",
			"Accept",
			"Content-Length",
			"Accept-Encoding",
			"X-CSRF-Token",
			"Origin",
			"X-Requested-With",
		},
	}

	h := auth.MakeMiddleware(authService)(mux)
	h = middleware.CORS(corsConfig)(h)
	h = middleware.AddHeader("Content-Type", "application/json; charset=utf-8")(h)
	// go runtick()
	http.ListenAndServe(getPort(), h)

}

// func runtick() {

// 	ticker := time.NewTicker(time.Second)
// 	for {
// 		select {

// 		case t := <-ticker.C:
// 			fmt.Println("Current time: ", t)
// 		}
// 	}

// }

func healthCheckHandler(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)

	fmt.Fprintf(w, "OK NUMBER 1!")
}

func must(err error) {
	if err != nil {
		fmt.Println("Error:", err)
		log.Fatal(err)
	}
}
