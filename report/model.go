package report

//ModelCreateCheckedReport model for report
type ModelCreateCheckedReport struct {
	ID         int64   `json:"id" db:"id"`
	ReportName string  `json:"report_name" db:"report_name"`
	Usercode   int64   `json:"user_code" db:"user_code"`
	CreateAt   string  `json:"created_at" db:"created_at"`
	ItemsID    int64   `json:"items_id" db:"items_id"`
	ListItem   []Items `json:"list_items" db:"list_items"`
}

//Items model list items
type Items struct {
	ID          int64  `json:"id" db:"id"`
	ReportID    int64  `json:"report_id" db:"report_id"`
	AssetItemID int64  `json:"asset_item_id" db:"asset_item_id"`
	Checked     bool   `json:"checked" db:"checked"`
	Message     string `json:"message" db:"message"`
}
