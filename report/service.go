package report

//Service report
type Service interface {
	CreateReportService(modelReport *ModelCreateCheckedReport) (interface{}, error)
}

type service struct {
	repo Repository
}

// NewService creates new auth service
func NewService(repo Repository) (Service, error) {
	s := service{repo}
	return &s, nil
}
func (s *service) CreateReportService(modelReport *ModelCreateCheckedReport) (interface{}, error) {
	resp, err := s.repo.CreateReportRepo(modelReport)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
