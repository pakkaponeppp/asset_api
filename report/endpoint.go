package report

import "context"

func makeCreateReportEndpoint(s Service) interface{} {
	type request ModelCreateCheckedReport

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		modelReport := ModelCreateCheckedReport{
			ReportName: req.ReportName,
			Usercode:   req.Usercode,
			CreateAt:   req.CreateAt,
			ListItem:   req.ListItem,
		}

		resp, err := s.CreateReportService(&modelReport)

		if err != nil {
			return &response{Result: "false", Message: err.Error()}, nil
		}

		return &response{
			Result:  "success",
			Message: "สำเร็จ",
			Data:    resp,
		}, nil
	}

}
