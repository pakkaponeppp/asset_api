package report

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/pakkaponeppp/asset_api/auth"
)

type errorResponse struct {
	Response string `json:"response"`
	Message  string `json:"message"`
}

var (
	errMethodNotAllowed = errors.New("invoice: method not allowed")
)

// MakeHandler for ticket domain
func MakeHandler(s Service) http.Handler {

	// m := hrpc.Manager{
	// 	Validate:     true,
	// 	Decoder:      requestDecoder,
	// 	Encoder:      responseEncoder,
	// 	ErrorEncoder: errorEncoder,
	// }
	mux := http.NewServeMux()

	return (mux)
}

// MakeAccountHandler require token
func MakeAccountHandler(s Service) http.Handler {
	// m := hrpc.Manager{
	// 	Validate:     true,
	// 	Decoder:      requestDecoder,
	// 	Encoder:      responseEncoder,
	// 	ErrorEncoder: errorEncoder,
	// }
	mux := http.NewServeMux()

	return mustLogin(s)(mux)
}
func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Content-Type", "application/json")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

}
func mustLogin(s Service) func(http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			user := auth.GetUserAccess(r.Context())
			if user == nil {
				errorEncoder(w, r, auth.ErrTokenNotFound)
				return
			}
			enableCors(&w)
			h.ServeHTTP(w, r)

		})
	}
}

func jsonDecoder(r *http.Request, v interface{}) error {

	return json.NewDecoder(r.Body).Decode(v)
}

func jsonEncoder(w http.ResponseWriter, status int, v interface{}) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(status)

	return json.NewEncoder(w).Encode(v)
}

func responseEncoder(w http.ResponseWriter, r *http.Request, v interface{}) {
	fmt.Println("v =", v)

	jsonEncoder(w, http.StatusOK, v)
}

func errorEncoder(w http.ResponseWriter, r *http.Request, err error) {
	encoder := jsonEncoder

	var status = http.StatusOK

	fmt.Println("Error Encode = ", err.Error())
	switch err.Error() {

	default:
		status = http.StatusOK
	}

	encoder(w, status, &errorResponse{"false", err.Error()})
}

func requestDecoder(r *http.Request, v interface{}) error {
	if r.Method != http.MethodPost {
		return errMethodNotAllowed
	}
	fmt.Println("v =", r)
	return jsonDecoder(r, v)
}
