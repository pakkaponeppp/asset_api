package auth

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"

	"github.com/acoshift/hrpc/v3"
)

var (
	errMethodNotAllowed = errors.New("auth: method not allowed")
	errForbidden        = errors.New("auth: forbidden")
	errMultiToken       = errors.New("auth: MultiToken")
	errBadRequest       = errors.New("auth: bad request body")
	errUnauthorized     = errors.New("Unauthorized")
)

type errorResponse struct {
	Response string `json:"respones"`
	Message  string `json:"message"`
	Error    string `json:"error"`
}

// MakeMiddleware creates new auth middleware
func MakeMiddleware(s Service) func(http.Handler) http.Handler {

	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			tokenID := r.Header.Get("Access-Token")

			if len(tokenID) == 0 {
				fmt.Println("auth Login No Token : ")
				h.ServeHTTP(w, r)
				return
			} else if len(tokenID) > 0 {

				fmt.Println("auth.transport token DT: ", tokenID)

				_, err := s.CheckActiveByTokenService(tokenID)

				if err != nil {
					errorEncoder(w, r, err)
					return
				}

				tk, err := s.GetToken(tokenID)
				if err != nil {
					errorEncoder(w, r, err)
					return
				}
				ctx := r.Context()
				ctx = context.WithValue(ctx, keyToken{}, tk)
				r = r.WithContext(ctx)
				h.ServeHTTP(w, r)
			}
		})
	}
}

//MakeHandler auth service
func MakeHandler(s Service) http.Handler {

	m := hrpc.Manager{
		Validate:     true,
		Decoder:      requestDecoder,
		Encoder:      responseEncoder,
		ErrorEncoder: errorEncoder,
	}

	mux := http.NewServeMux()

	mux.Handle("/signin", m.Handler(MakeSigninEndpoint(s)))

	return mustLogin(s)(mux)
}

func mustLogin(s Service) func(http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			enableCors(&w)
			h.ServeHTTP(w, r)
		})
	}
}
func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Content-Type", "application/json")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set(
		"Access-Control-Allow-Headers",
		"Accept, Access-Token,X-Access-Token, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, access-control-allow-origin",
	)
}

func jsonDecoder(r *http.Request, v interface{}) error {
	if err := json.NewDecoder(r.Body).Decode(v); err != nil {
		log.Printf("[Auth] API request: %+v\n", v)
		log.Println(err.Error())

		return errBadRequest
	}
	return nil
}

func jsonEncoder(w http.ResponseWriter, status int, v interface{}) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type,Token,Access-Token,x-access-token")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.WriteHeader(status)
	log.Printf("[Auth] API response %d: %+v\n", status, v)
	return json.NewEncoder(w).Encode(v)
}

func requestDecoder(r *http.Request, v interface{}) error {
	if r.Method != http.MethodPost {
		return errMethodNotAllowed
	}
	return jsonDecoder(r, v)
}

func responseEncoder(w http.ResponseWriter, r *http.Request, v interface{}) {
	jsonEncoder(w, http.StatusOK, v)
}

func errorEncoder(w http.ResponseWriter, r *http.Request, err error) {
	encoder := jsonEncoder
	status := http.StatusInternalServerError
	fmt.Println("auth transport ,check error : ", err.Error())
	if err == errUnauthorized {
		fmt.Println("year equal to ...")
	} else {
		fmt.Println("not ...")
	}

	switch err {
	case errMethodNotAllowed:
		status = http.StatusMethodNotAllowed
	case errForbidden:
		status = http.StatusForbidden
	case errBadRequest:
		status = http.StatusBadRequest
	case errors.New("Unauthorized"):
		status = http.StatusUnauthorized
	case ErrTokenExpired:
		status = http.StatusUnauthorized
	case ErrTokenNotFound:
		status = http.StatusUnauthorized
	}

	if r.Method == http.MethodOptions {
		encoder(w, http.StatusNoContent, nil)
	} else {
		encoder(w, status, &errorResponse{"false", "เกิดข้อผิดพลาด กรุณาติดต่อ Admin ", err.Error()})
	}
}
