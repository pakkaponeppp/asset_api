package auth

import (
	"errors"
	"regexp"
)

//ModelAuth struct
type ModelAuth struct {
	Username string `json:"username"`
	Password string `json:"password"`
	UserCode string `json:"user_code"`
	Name     string `json:"name"`
	RoleID   string `json:"role_id"`
	Active   int64  `json:"active"`
}

//Token ss
type Token struct {
	ID       string
	UserID   int64
	UserName string
	UserCode string
	Name     string
	Token    string
}

//Repository auth
type Repository interface {
	SaveTokenLogin(username string, token string) (interface{}, error)
	FindUser(username string) (*ModelAuth, error)
	CheckActiveByTokenRepo(tokenID string) (interface{}, error)
	GetuserCodeRepo(userCode string) (*ModelAuth, error)
	GetToken(tokenID string) (*Token, error)
}

// Errors
var (
	ErrInActive         = errors.New("this user is not active")
	ErrTokenNotFound    = errors.New("auth: token not found")
	ErrTokenExpired     = errors.New("auth: token expired")
	ErrBadFormat        = errors.New("invalid format")
	ErrUnresolvableHost = errors.New("unresolvable host")
	emailRegexp         = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
)
