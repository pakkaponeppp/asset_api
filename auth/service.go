package auth

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"github.com/gofrs/uuid"
)

// "errors"
// "strings"

// "github.com/gofrs/uuid"

type service struct {
	auths Repository
}
type (
	keyToken struct{}
)

//Service sss
type Service interface {
	SigninService(username string, password string) (interface{}, error)
	SwitchFindUser(username string) (*ModelAuth, error)
	GetToken(tokenID string) (*Token, error)
	CheckActiveByTokenService(tokenID string) (interface{}, error)
}

//NewService sss
func NewService(repo Repository) (Service, error) {
	s := service{repo}
	return &s, nil
}

//GetUserAccess SSS
func GetUserAccess(ctx context.Context) *Token {
	x, ok := ctx.Value(keyToken{}).(*Token)
	if !ok {
		return nil
	}
	return x
}

func (s *service) CheckActiveByTokenService(tokenID string) (interface{}, error) {

	resp, err := s.auths.CheckActiveByTokenRepo(tokenID)
	if err != nil {
		return nil, err
	}

	return resp, nil

}

//SwitchFindUser Ser
func (s *service) SwitchFindUser(username string) (*ModelAuth, error) {

	user, err := s.auths.FindUser(username)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *service) SigninService(username string, password string) (interface{}, error) {

	if username == "" {
		return nil, errors.New("กรุกรอก Username")
	}

	cc, err := s.SwitchFindUser(username)
	if err != nil {
		return nil, err
	}
	if password != cc.Password {
		return nil, errors.New("ชื่อผู้ใช้ หรือ รหัสผ่านไม่ถูกต้อง กรุณาลองอีกครั้ง")
	}
	if cc.Active != 1 {
		return nil, errors.New("ไม่เปิดใช้งาน")
	}
	// if cc.RoleID != "1" {
	// 	return nil, errors.New("ยังไม่เปิดใช้งาน")
	// }

	newUUID, err := uuid.NewV4()

	if err != nil {
		return nil, err
	}
	randomKey := strings.Replace(newUUID.String(), "-", "", -1)
	_, err = s.auths.SaveTokenLogin(cc.UserCode, randomKey)
	if err != nil {
		return nil, err
	}
	Account := map[string]string{
		"token":     randomKey,
		"username":  cc.Username,
		"name":      cc.Name,
		"role_id":   cc.RoleID,
		"user_code": cc.UserCode,
	}
	return &Account, nil

}

func (s *service) GetToken(tokenID string) (*Token, error) {
	if len(tokenID) == 0 {
		return nil, nil
	}
	tk, err := s.auths.GetToken(tokenID)
	fmt.Println(tk)
	if err != nil {
		return nil, err
	}
	return tk, nil
}
