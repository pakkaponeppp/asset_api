package auth

import (
	"context"
	"fmt"
)

//MakeSigninEndpoint E
func MakeSigninEndpoint(s Service) interface{} {
	type request struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}
	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {
		fmt.Println("begin endpoint.makeSigninEndpoint")

		resp, err := s.SigninService(req.Username, req.Password)

		if err != nil {
			fmt.Println("makeSigninEndpoint error", err.Error())
			return &response{Result: "failed", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "เข้าสู่ระบบสำเร็จ",
			Data:    resp,
		}, nil
	}
}
