module gitlab.com/pakkaponeppp/asset_api

go 1.16

require (
	cloud.google.com/go/storage v1.13.0
	github.com/acoshift/hrpc/v3 v3.2.0
	github.com/acoshift/middleware v0.4.3
	github.com/chai2010/webp v1.1.0
	github.com/disintegration/imaging v1.6.2
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/google/uuid v1.2.0
	github.com/h2non/filetype v1.1.1
	github.com/jmoiron/sqlx v1.3.1
	github.com/labstack/gommon v0.3.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/utahta/go-linenotify v0.4.2
	google.golang.org/api v0.40.0
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22 // indirect
	upper.io/db.v3 v3.8.0+incompatible
)
