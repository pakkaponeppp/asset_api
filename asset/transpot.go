package asset

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/acoshift/hrpc/v3"
	"gitlab.com/pakkaponeppp/asset_api/auth"
)

type errorResponse struct {
	Response string `json:"response"`
	Message  string `json:"message"`
}

var (
	errMethodNotAllowed = errors.New("invoice: method not allowed")
)

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Content-Type", "application/json")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

}

// MakeHandler for ticket domain
func MakeHandler(s Service) http.Handler {

	m := hrpc.Manager{
		Validate:     true,
		Decoder:      requestDecoder,
		Encoder:      responseEncoder,
		ErrorEncoder: errorEncoder,
	}
	mux := http.NewServeMux()

	mux.Handle("/get/asset/item/id/bp", m.Handler(makeGETAssetItemIDBypass(s)))
	mux.Handle("/list/position", m.Handler(makeListUserPosition(s)))
	mux.Handle("/list/name", m.Handler(makeListUserName(s)))
	mux.Handle("/", m.Handler(MakeAccountHandler(s)))

	return (mux)
}

// MakeAccountHandler require token
func MakeAccountHandler(s Service) http.Handler {
	m := hrpc.Manager{
		Validate:     true,
		Decoder:      requestDecoder,
		Encoder:      responseEncoder,
		ErrorEncoder: errorEncoder,
	}
	mux := http.NewServeMux()

	//CREATE//
	mux.Handle("/create/asset", m.Handler(makeCreateAsset(s)))
	mux.Handle("/create/asset/item", m.Handler(makeCreateAssetItem(s)))
	mux.Handle("/create/category", m.Handler(makeCreateAssetCategory(s)))
	mux.Handle("/create/subcategory", m.Handler(makeCreateAssetSubCategory(s)))

	//UPDATE//
	mux.Handle("/update/asset", m.Handler(makeUpdateAsset(s)))
	mux.Handle("/update/item", m.Handler(makeUpdateAssetItem(s)))
	mux.Handle("/update/category", m.Handler(makeUpdateAssetCategory(s)))
	mux.Handle("/update/subcategory", m.Handler(makeUpdateAssetSubCategory(s)))
	mux.Handle("/update/status", m.Handler(makeUpdateAssetStatus(s)))
	mux.Handle("/update/asset/damage", m.Handler(makeAssetDamageToAsset(s)))
	//LIST//
	mux.Handle("/list/asset", m.Handler(makeListAsset(s)))
	mux.Handle("/list/asset/user", m.Handler(makeListAssetUser(s)))
	mux.Handle("/list/asset/damage", m.Handler(makeListAssetDamage(s)))
	mux.Handle("/list/asset/item/damage", m.Handler(makeListAssetItemDamage(s)))
	mux.Handle("/list/asset/item/available", m.Handler(makeListAvailalbeAssetItemID(s)))
	mux.Handle("/list/asset/item/unavailable", m.Handler(makeListUnavailableAssetItemID(s)))
	mux.Handle("/list/location", m.Handler(makeListAssetLocation(s)))
	mux.Handle("/list/category", m.Handler(makeListAssetCategory(s)))
	mux.Handle("/list/subcategory", m.Handler(makeListAssetSubCategory(s)))
	mux.Handle("/list/categorybar", m.Handler(makeListAssetByCategory(s)))
	//GET//
	mux.Handle("/get/asset/id", m.Handler(makeGETAssetID(s)))
	mux.Handle("/get/location/id", m.Handler(makeGETLocationID(s)))
	mux.Handle("/get/asset/item/id", m.Handler(makeGETAssetItemID(s)))
	mux.Handle("/get/category/id", m.Handler(makeGETAssetCategoryID(s)))
	mux.Handle("/get/subcategory/id", m.Handler(makeGETAssetSubCategoryID(s)))
	//DELETE//
	mux.Handle("/delete/asset/id", m.Handler(makeDeleteAssetID(s)))
	mux.Handle("/delete/category/id", m.Handler(makeDeleteCategory(s)))
	mux.Handle("/delete/subcategory/id", m.Handler(makeDeleteSubCategory(s)))
	mux.Handle("/delete/asset/item/id", m.Handler(makeDeleteAssetItem(s)))
	//ADD-ON

	mux.Handle("/search/asset", m.Handler(makeSearchAssetEndpoint(s)))
	mux.Handle("/search/asset/user", m.Handler(makeUserSearchAssetEndpoint(s)))
	mux.Handle("/search/asset/item", m.Handler(makeSearchAssetItem(s)))
	mux.Handle("/calwarrantydate", m.Handler(makeCalWarranty(s)))
	mux.Handle("/calprices", m.Handler(makeCalPrices(s)))
	mux.Handle("/search/asset/category", m.Handler(makeSearchAssetByCategory(s)))
	mux.Handle("/search/asset/category/user", m.Handler(makeUserSearchAssetByCategory(s)))

	mux.Handle("/create/user", m.Handler(makeAddUser(s)))

	mux.Handle("/update/user/active", m.Handler(makeManageUserStatus(s)))
	mux.Handle("/list/user", m.Handler(makeListUser(s)))
	mux.Handle("/get/user", m.Handler(makeGetUserDetail(s)))
	mux.Handle("/update/user", m.Handler(makeUpdateUser(s)))
	mux.Handle("/dashboard/count/assetitem", m.Handler(makeCountAssetItemForDashboard(s)))

	return mustLogin(s)(mux)
}

func mustLogin(s Service) func(http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			user := auth.GetUserAccess(r.Context())
			if user == nil {
				errorEncoder(w, r, auth.ErrTokenNotFound)
				return
			}
			enableCors(&w)
			h.ServeHTTP(w, r)

		})
	}
}

func jsonDecoder(r *http.Request, v interface{}) error {
	return json.NewDecoder(r.Body).Decode(v)
}

func jsonEncoder(w http.ResponseWriter, status int, v interface{}) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(status)
	return json.NewEncoder(w).Encode(v)
}

func responseEncoder(w http.ResponseWriter, r *http.Request, v interface{}) {
	fmt.Println("v =", v)
	jsonEncoder(w, http.StatusOK, v)
}

func errorEncoder(w http.ResponseWriter, r *http.Request, err error) {
	encoder := jsonEncoder

	var status = http.StatusOK

	fmt.Println("Error Encode = ", err.Error())
	switch err.Error() {

	default:
		status = http.StatusOK
	}

	encoder(w, status, &errorResponse{"false", err.Error()})
}

func requestDecoder(r *http.Request, v interface{}) error {
	if r.Method != http.MethodPost {
		return errMethodNotAllowed
	}
	fmt.Println("v =", r)
	return jsonDecoder(r, v)
}
