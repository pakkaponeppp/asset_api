package asset

//ModelAsset Model
type ModelAsset struct {
	ID              int64   `json:"id" db:"id"`
	AssetName       string  `json:"name,omitempty" db:"name"`
	CategoryID      int64   `json:"category_id,omitempty" db:"category_id"`
	SubCategoryID   int64   `json:"sub_category_id,omitempty" db:"sub_category_id"`
	Description     string  `json:"description,omitempty" db:"description"`
	Barcode         string  `json:"barcode,omitempty" db:"barcode"`
	URL             string  `json:"url,omitempty" db:"url"`
	Qty             int     `json:"qty" db:"qty"`
	QtyDamage       int     `json:"qty_damage" db:"qty_damage"`
	QtyAvailable    int     `json:"qty_available" db:"qty_available"`
	QtyTransaction  int     `json:"qty_ts" db:"qty_ts"`
	QtyUnavailable  int     `json:"qty_unavailable" db:"qty_unavailable"`
	CategoryName    string  `json:"category_name,omitempty" db:"category_name"`
	SubCategoryName string  `json:"sub_category_name,omitempty" db:"sub_category_name"`
	Active          int     `json:"active" db:"active"`
	Types           int     `json:"types" db:"types"`
	Percents        float64 `json:"percents" db:"percents"`
	UserCode        string  `json:"user_code" db:"user_code"`
	Remain          int     `json:"remain" db:"remain"`
}

//ModelAssetItem Model
type ModelAssetItem struct {
	ID               int64                `json:"id" db:"id"`
	AssetName        string               `json:"name" db:"name"`
	AssetID          int64                `json:"asset_id" db:"asset_id"`
	Usercode         string               `json:"user_code,omitempty" db:"user_code"`
	LocationID       int64                `json:"location_id" db:"location_id"`
	CategoryID       int64                `json:"category_id" db:"category_id"`
	SubCategoryID    int64                `json:"sub_category_id" db:"sub_category_id"`
	LocationName     string               `json:"location" db:"location"`
	Description      string               `json:"description" db:"description"`
	Percents         float64              `json:"percents" db:"percents"`
	Status           int64                `json:"status" db:"status"`
	Barcode          string               `json:"barcode" db:"barcode"`
	CategoryName     string               `json:"category_name" db:"category_name"`
	SubCategoryName  string               `json:"sub_category_name" db:"sub_category_name"`
	Available        int64                `json:"available" db:"available"`
	SerialNumber     string               `json:"serial_number" db:"serial_number"`
	Detail           string               `json:"detail" db:"detail"`
	Contact          string               `json:"contact" db:"contact"`
	CreatedAt        string               `json:"created_at" db:"created_at"`
	UpdateAt         string               `json:"updated_at" db:"updated_at"`
	BuyingDate       string               `json:"buying_date" db:"buying_date"`
	StartedWarranty  string               `json:"started_warranty" db:"started_warranty"`
	WarrantyMonth    int                  `json:"warranty_month" db:"warranty_month"`
	WarrantyDate     string               `json:"warranty_date" db:"warranty_date"`
	Warranty         string               `json:"warranty" db:"warranty"`
	Conditions       int64                `json:"conditions" db:"conditions"`
	Prices           float64              `json:"prices" db:"prices"`
	CalculatedPrices float64              `json:"calculated_prices" db:"calculated_prices"`
	Position         []ModelLocationPined `json:"position"`
	Username         []Users              `json:"username"`
	EditedBy         string               `json:"edited_by,omitempty" db:"edited_by"`
	CreatedBy        string               `json:"created_by,omitempty" db:"created_by"`
	ImageURL         string               `json:"image_url" db:"image_url"`
}

//Users model
type Users struct {
	Name    string `json:"name,omitempty" db:"name"`
	PosName string `json:"pos_name,omitempty" db:"pos_name"`
}

//ModelCategory Model
type ModelCategory struct {
	ID              int64         `json:"id,omitempty" db:"id"`
	CategoryID      int64         `json:"category_id,omitempty" db:"category_id"`
	CategoryName    string        `json:"name,omitempty" db:"name"`
	SubCategoryName string        `json:"sname,omitempty" db:"sname"`
	SubCats         []ModelSubCat `json:"subs,omitempty"`
}

//ModelSubCat Model
type ModelSubCat struct {
	ID           int64  `json:"id,omitempty" db:"id"`
	CategoryID   int64  `json:"category_id,omitempty" db:"category_id"`
	SubCateName  string `json:"sub_category_name,omitempty" db:"sub_category_name"`
	CategoryName string `json:"category_name,omitempty" db:"category_name"`
}

//ModelLocation Model
type ModelLocation struct {
	ID           int64  `json:"id,omitempty" db:"id"`
	LocationName string `json:"name,omitempty" db:"name"`
}

//ModelLocationPined Model
type ModelLocationPined struct {
	Latitude   float64 `json:"lat,omitempty" db:"lat"`
	Longtitude float64 `json:"lng,omitempty" db:"lng"`
}

//DashBoard Model
type DashBoard struct {
	QtyItem []QtyDashboard `json:"qty_item,omitempty" db:"qty_item"`
	Value   [12]int64      `json:"value,omitempty" db:"value"`
}

//DashBoardMount Model
type DashBoardMount struct {
	Value    int64  `json:"value" db:"value"`
	Month    string `json:"month" db:"month"`
	MonthInt int64  `json:"month_int" db:"month_int"`
}

//User Model
type User struct {
	ID           int64  `json:"id" db:"id"`
	Usercode     string `json:"user_code" db:"user_code"`
	RoleID       int64  `json:"role_id" db:"role_id"`
	PositionID   int64  `json:"position_id" db:"position_id"`
	PositionName string `json:"position_name" db:"position_name"`
	Username     string `json:"username" db:"username"`
	Password     string `json:"password" db:"password"`
	Name         string `json:"name" db:"name"`
	Active       int64  `json:"active" db:"active"`
	IsActive     bool   `json:"is_active"`
}

//UserStruct Model
type UserStruct struct {
	ID       int64    `json:"id" db:"id"`
	Usercode string   `json:"user_code" db:"user_code"`
	Role     Role     `json:"role" db:"role"`
	Position Position `json:"position" db:"position"`
	Username string   `json:"username" db:"username"`
	Password string   `json:"password" db:"password"`
	Name     string   `json:"name" db:"name"`
	Active   int64    `json:"active" db:"active"`
	IsActive bool     `json:"is_active"`
}

//Position Model
type Position struct {
	ID           int64  `json:"id" db:"id"`
	PositionName string `json:"position_name" db:"position_name"`
}

//NameList Model
type NameList struct {
	ID   int64  `json:"id" db:"id"`
	Name string `json:"name" db:"name"`
}

//Role Model
type Role struct {
	RoleID int64  `json:"id" db:"role_id"`
	Name   string `json:"name" db:"name"`
}

//QtyDashboard Model
type QtyDashboard struct {
	QtyAll                   int64 `json:"qty_all" db:"qty_all"`
	QtyAvailable             int64 `json:"qty_available" db:"qty_available"`
	QtyUnavailable           int64 `json:"qty_unavailable" db:"qty_unavailable"`
	QtyDamage                int64 `json:"qty_damage" db:"qty_damage"`
	QtyTransactionRequestAll int64 `json:"qty_transaction_request_all" db:"qty_transaction_request_all"`
	QtyTransactionRequest    int64 `json:"qty_transaction_request" db:"qty_transaction_request"`
	QtyTransactionReturnAll  int64 `json:"qty_transaction_return_all" db:"qty_transaction_return_all"`
	QtyTransactionReturn     int64 `json:"qty_transaction_return" db:"qty_transaction_return"`
	QtyTransactionReportAll  int64 `json:"qty_transaction_report_all" db:"qty_transaction_report_all"`
	QtyTransactionReport     int64 `json:"qty_transaction_report" db:"qty_transaction_report"`
}
