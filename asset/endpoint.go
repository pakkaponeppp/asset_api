package asset

import (
	"context"
)

//-----------------------------CREATE--------------------------------//

func makeCreateAsset(s Service) interface{} {

	type request ModelAsset

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		model := ModelAsset{
			AssetName:     req.AssetName,
			CategoryID:    req.CategoryID,
			SubCategoryID: req.SubCategoryID,
			Barcode:       req.Barcode,
			Description:   req.Description,
			URL:           req.URL,
			Types:         req.Types,
			Percents:      req.Percents,
			UserCode:      req.UserCode,
		}

		resp, err := s.CreateAssetService(&model)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}

		return &response{
			Result:  "success",
			Message: "เพิ่มข้อมูลสำเร็จ",
			Data:    resp,
		}, nil
	}
}

func makeCreateAssetItem(s Service) interface{} {

	type request ModelAssetItem

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		model := ModelAssetItem{
			AssetID:          req.AssetID,
			LocationID:       req.LocationID,
			Status:           req.Status,
			Available:        req.Available,
			SerialNumber:     req.SerialNumber,
			Detail:           req.Detail,
			Contact:          req.Contact,
			BuyingDate:       req.BuyingDate,
			WarrantyMonth:    req.WarrantyMonth,
			WarrantyDate:     req.WarrantyDate,
			Conditions:       req.Conditions,
			Prices:           req.Prices,
			CalculatedPrices: req.CalculatedPrices,
			StartedWarranty:  req.StartedWarranty,
			ImageURL:         req.ImageURL,
			Usercode:         req.Usercode,
		}

		resp, err := s.CreateAssetItemService(&model)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "เพิ่มข้อมูลสำเร็จ",
			Data:    resp,
		}, nil
	}
}

func makeCreateAssetCategory(s Service) interface{} {

	type request ModelCategory

	type response struct {
		Result  string `json:"response"`
		Message string `json:"message"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		model := ModelCategory{
			CategoryName: req.CategoryName,
		}

		_, err := s.CreateAssetCategoryService(&model)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "เพิ่มข้อมูลสำเร็จ",
		}, nil
	}
}

func makeCreateAssetSubCategory(s Service) interface{} {

	type request ModelCategory

	type response struct {
		Result  string `json:"response"`
		Message string `json:"message"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		model := ModelCategory{
			CategoryID:      req.CategoryID,
			SubCategoryName: req.SubCategoryName,
		}

		_, err := s.CreateAssetSubCategoryService(&model)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "เพิ่มข้อมูลสำเร็จ",
		}, nil
	}
}

//-----------------------------UPDATE--------------------------------//

func makeUpdateAsset(s Service) interface{} {

	type request ModelAsset

	type response struct {
		Result  string `json:"response"`
		Message string `json:"message"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		model := ModelAsset{
			ID:            req.ID,
			AssetName:     req.AssetName,
			CategoryID:    req.CategoryID,
			SubCategoryID: req.SubCategoryID,
			Description:   req.Description,
			URL:           req.URL,
			Percents:      req.Percents,
			UserCode:      req.UserCode,
		}

		_, err := s.UpdateAssetService(&model)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แก้ไขข้อมูลสำเร็จ",
		}, nil
	}
}

func makeUpdateAssetItem(s Service) interface{} {

	type request ModelAssetItem

	type response struct {
		Result  string `json:"response"`
		Message string `json:"message"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		model := ModelAssetItem{
			ID:               req.ID,
			AssetID:          req.AssetID,
			LocationID:       req.LocationID,
			SerialNumber:     req.SerialNumber,
			Detail:           req.Detail,
			WarrantyDate:     req.WarrantyDate,
			Conditions:       req.Conditions,
			Contact:          req.Contact,
			BuyingDate:       req.BuyingDate,
			Prices:           req.Prices,
			CalculatedPrices: req.CalculatedPrices,
			StartedWarranty:  req.StartedWarranty,
			WarrantyMonth:    req.WarrantyMonth,
			EditedBy:         req.EditedBy,
			ImageURL:         req.ImageURL,
		}

		_, err := s.UpdateAssetItemService(&model)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แก้ไขข้อมูลสำเร็จ",
		}, nil
	}
}

func makeUpdateAssetCategory(s Service) interface{} {

	type request ModelCategory

	type response struct {
		Result  string `json:"response"`
		Message string `json:"message"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		model := ModelCategory{
			ID:           req.ID,
			CategoryName: req.CategoryName,
		}

		_, err := s.UpdateAssetCategoryService(&model)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แก้ไขข้อมูลสำเร็จ",
		}, nil
	}
}

func makeUpdateAssetSubCategory(s Service) interface{} {

	type request ModelCategory

	type response struct {
		Result  string `json:"response"`
		Message string `json:"message"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		model := ModelCategory{
			ID:              req.ID,
			SubCategoryName: req.SubCategoryName,
		}

		_, err := s.UpdateAssetSubCategoryService(&model)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แก้ไขข้อมูลสำเร็จ",
		}, nil
	}
}
func makeUpdateAssetStatus(s Service) interface{} {

	type request ModelAssetItem

	type response struct {
		Result  string `json:"response"`
		Message string `json:"message"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		model := ModelAssetItem{
			ID: req.ID,
		}

		_, err := s.UpdateAssetStatusService(&model)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แก้ไขข้อมูลสำเร็จ",
		}, nil
	}
}

//-----------------------------LIST--------------------------------//

func makeListAsset(s Service) interface{} {

	type request struct {
		ID int64 `json:"id"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.ListAssetService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}
func makeListAssetUser(s Service) interface{} {

	type request struct {
		ID int64 `json:"id"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.ListAssetUserService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}
func makeListAssetDamage(s Service) interface{} {

	type request struct {
		ID int64 `json:"id"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.ListAssetDamageService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}
func makeListAssetItemDamage(s Service) interface{} {

	type request struct {
		ID int64 `json:"id"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.ListAssetItemDamageService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}

func makeListAssetItem(s Service) interface{} {

	type request struct {
		ID int64 `json:"id"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.ListAssetItemService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}

func makeListAvailalbeAssetItemID(s Service) interface{} {

	type request struct {
		ID int64 `json:"id"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.ListAvailableAssetItemIDService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}

func makeListUnavailableAssetItemID(s Service) interface{} {

	type request struct {
		ID int64 `json:"id"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.ListUnavailableAssetItemIDService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}

func makeListAssetCategory(s Service) interface{} {

	type request struct {
		ID int64 `json:"id"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.ListAssetCategoryService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}

func makeListAssetLocation(s Service) interface{} {

	type request struct {
		ID int64 `json:"id"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.ListAssetLocationService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}
func makeListUserPosition(s Service) interface{} {

	type request struct {
		ID int64 `json:"id"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.ListUserPositionService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}
func makeListUserName(s Service) interface{} {

	type request struct {
		ID int64 `json:"id"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.ListUserNameService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}

func makeListAssetSubCategory(s Service) interface{} {

	type request struct {
		ID int64 `json:"id"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.ListAssetSubCategoryService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}

//-----------------------------GET--------------------------------//

func makeGETAssetSubCategoryID(s Service) interface{} {

	type request struct {
		ID int64 `json:"id"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.GETAssetSubCategoryIDService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}

func makeGETAssetID(s Service) interface{} {

	type request struct {
		ID int64 `json:"id"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.GETAssetIDService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}

func makeGETAssetCategoryID(s Service) interface{} {

	type request struct {
		ID int64 `json:"id"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.GETAssetCategoryIDService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}

func makeGETAssetItemID(s Service) interface{} {

	type request struct {
		ID int64 `json:"id"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.GETAssetItemIDService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}
func makeGETAssetItemIDBypass(s Service) interface{} {

	type request struct {
		ID int64 `json:"id"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.GETAssetItemIDService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}

func makeGETLocationID(s Service) interface{} {

	type request struct {
		ID int64 `json:"id"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.GETLocationIDService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แสดงข้อมูล",
			Data:    resp,
		}, nil
	}
}

//-----------------------------DELETE--------------------------------//

func makeDeleteAssetID(s Service) interface{} {

	type request struct{ ID int64 }

	type response struct {
		Result  string `json:"response"`
		Message string `json:"message"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		_, err := s.DeleteAssetIDService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "ลบข้อมูลสำเร็จ",
		}, nil
	}
}
func makeDeleteCategory(s Service) interface{} {

	type request struct{ ID int64 }

	type response struct {
		Result  string `json:"response"`
		Message string `json:"message"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		err := s.DeleteCategoryService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "ลบข้อมูลสำเร็จ",
		}, nil
	}
}
func makeDeleteSubCategory(s Service) interface{} {

	type request struct{ ID int64 }

	type response struct {
		Result  string `json:"response"`
		Message string `json:"message"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		err := s.DeleteSubCategoryService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "ลบข้อมูลสำเร็จ",
		}, nil
	}
}
func makeDeleteAssetItem(s Service) interface{} {

	type request struct{ ID int64 }

	type response struct {
		Result  string `json:"response"`
		Message string `json:"message"`
	}
	return func(ctx context.Context, req *request) (*response, error) {

		err := s.DeleteAssetItemService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "ลบข้อมูลสำเร็จ",
		}, nil
	}
}

func makeListAssetByCategory(s Service) interface{} {
	type request ModelCategory

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}

	return func(ctx context.Context, req *request) (*response, error) {

		model := ModelCategory{ID: req.ID}

		resp, err := s.ListByCategoryService(&model)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "ค้นหาข้อมูล",
			Data:    resp,
		}, nil
	}

}
func makeSearchAssetByCategory(s Service) interface{} {
	type request struct {
		CategoryID    int64 `json:"category_id"`
		SubCategoryID int64 `json:"sub_category_id"`
		Status        int64 `json:"status"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}

	return func(ctx context.Context, req *request) (*response, error) {
		resp, err := s.SearchAssetByCategoryService(req.CategoryID, req.SubCategoryID, req.Status)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "ค้นหาข้อมูล",
			Data:    resp,
		}, nil
	}

}
func makeUserSearchAssetByCategory(s Service) interface{} {
	type request struct {
		CategoryID    int64 `json:"category_id"`
		SubCategoryID int64 `json:"sub_category_id"`
		Status        int64 `json:"status"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}

	return func(ctx context.Context, req *request) (*response, error) {
		resp, err := s.UserSearchAssetByCategoryService(req.CategoryID, req.SubCategoryID, req.Status)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "ค้นหาข้อมูล",
			Data:    resp,
		}, nil
	}

}

func makeSearchAssetEndpoint(s Service) interface{} {
	type request struct {
		Keyword string `json:"keyword"`
		Active  int64  `json:"active"`
		Status  int64  `json:"status"`
		Limit   int64  `json:"limit"`
	}
	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
		Length  int64       `json:"length"`
	}
	return func(ctx context.Context, req *request) (*response, error) {
		len, resp, err := s.ListSearchAssetService(req.Active, req.Keyword, req.Status, req.Limit)
		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "ค้นหาข้อมูล",
			Data:    resp,
			Length:  len,
		}, nil
	}
}
func makeUserSearchAssetEndpoint(s Service) interface{} {
	type request struct {
		Keyword string `json:"keyword"`
		Active  int64  `json:"active"`
		Status  int64  `json:"status"`
		Limit   int64  `json:"limit"`
	}
	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
		Length  int64       `json:"length"`
	}
	return func(ctx context.Context, req *request) (*response, error) {
		len, resp, err := s.UserSearchAssetService(req.Active, req.Keyword, req.Status, req.Limit)
		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "ค้นหาข้อมูล",
			Data:    resp,
			Length:  len,
		}, nil
	}
}

func makeCalWarranty(s Service) interface{} {
	type request struct {
		Startdate string `json:"start_date"`
		Month     int    `json:"month"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}

	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.CalWarrantyService(req.Startdate, req.Month)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "สำเร็จ",
			Data:    resp,
		}, nil
	}

}
func makeCalPrices(s Service) interface{} {
	type request struct {
		BuyingDate string  `json:"buyingdate"`
		Percents   float64 `json:"percents"`
		Prices     float64 `json:"prices"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}

	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.CalPriceService(req.BuyingDate, req.Percents, req.Prices)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "สำเร็จ",
			Data:    resp,
		}, nil
	}

}
func makeSearchAssetItem(s Service) interface{} {
	type request struct {
		AssetID int64  `json:"id"`
		Keyword string `json:"keyword"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}

	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.SearchAssetItemService(req.AssetID, req.Keyword)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "สำเร็จ",
			Data:    resp,
		}, nil
	}

}
func makeCountAssetItemForDashboard(s Service) interface{} {
	type request struct {
		ID int64
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}

	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.CountAssetItemForDashboardService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "สำเร็จ",
			Data:    resp,
		}, nil
	}

}
func makeAssetDamageToAsset(s Service) interface{} {
	type request struct {
		ID       int64
		Usercode string `json:"user_code"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}

	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.AssetDamageToAssetService(req.ID, req.Usercode)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "สำเร็จ",
			Data:    resp,
		}, nil
	}

}
func makeListUser(s Service) interface{} {
	type request struct {
		ID int64
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}

	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.ListUserService(req.ID)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "สำเร็จ",
			Data:    resp,
		}, nil
	}

}
func makeAddUser(s Service) interface{} {
	type request User

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}

	return func(ctx context.Context, req *request) (*response, error) {

		model := User{
			RoleID:     req.RoleID,
			PositionID: req.PositionID,
			Username:   req.Username,
			Password:   req.Password,
			Name:       req.Name}

		resp, err := s.AddUserService(&model)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "สำเร็จ",
			Data:    resp,
		}, nil
	}

}
func makeUpdateUser(s Service) interface{} {
	type request User

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}

	return func(ctx context.Context, req *request) (*response, error) {

		model := User{
			Usercode:   req.Usercode,
			RoleID:     req.RoleID,
			PositionID: req.PositionID,
			Username:   req.Username,
			Password:   req.Password,
			Name:       req.Name}

		resp, err := s.UpdateUserService(&model)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "สำเร็จ",
			Data:    resp,
		}, nil
	}

}
func makeGetUserDetail(s Service) interface{} {
	type request struct {
		Usercode string `json:"user_code"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}

	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.GetUserDetailService(req.Usercode)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "สำเร็จ",
			Data:    resp,
		}, nil
	}

}
func makeManageUserStatus(s Service) interface{} {
	type request struct {
		Usercode string `json:"user_code"`
		Status   int64  `json:"status"`
	}

	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}

	return func(ctx context.Context, req *request) (*response, error) {

		resp, err := s.ManageUserService(req.Usercode, req.Status)

		if err != nil {

			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "สำเร็จ",
			Data:    resp,
		}, nil
	}

}
