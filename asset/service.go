package asset

import "strings"

//Service asset interface
type Service interface {
	CreateAssetService(model *ModelAsset) (interface{}, error)
	CreateAssetItemService(model *ModelAssetItem) (interface{}, error)
	CreateAssetCategoryService(model *ModelCategory) (interface{}, error)
	CreateAssetSubCategoryService(model *ModelCategory) (interface{}, error)

	//-----------------------------------------------------------------------//

	UpdateAssetService(model *ModelAsset) (interface{}, error)
	UpdateAssetItemService(model *ModelAssetItem) (interface{}, error)
	UpdateAssetCategoryService(model *ModelCategory) (interface{}, error)
	UpdateAssetSubCategoryService(model *ModelCategory) (interface{}, error)
	UpdateAssetStatusService(model *ModelAssetItem) (interface{}, error)

	//-----------------------------------------------------------------------//

	ListAssetService(ID int64) (interface{}, error)
	ListAssetUserService(ID int64) (interface{}, error)
	ListAssetDamageService(ID int64) (interface{}, error)
	ListAssetItemDamageService(ID int64) (interface{}, error)
	ListAssetItemService(ID int64) (interface{}, error)
	ListAvailableAssetItemIDService(ID int64) (interface{}, error)
	ListUnavailableAssetItemIDService(ID int64) (interface{}, error)
	ListAssetLocationService(ID int64) (interface{}, error)
	ListAssetCategoryService(ID int64) (interface{}, error)
	ListAssetSubCategoryService(ID int64) (interface{}, error)

	//-----------------------------------------------------------------------//

	GETAssetIDService(ID int64) (interface{}, error)
	GETLocationIDService(ID int64) (interface{}, error)
	GETAssetItemIDService(ID int64) (interface{}, error)
	GETAssetCategoryIDService(ID int64) (interface{}, error)
	GETAssetSubCategoryIDService(ID int64) (interface{}, error)

	//-----------------------------------------------------------------------//

	DeleteAssetIDService(ID int64) (interface{}, error)
	DeleteCategoryService(ID int64) error
	DeleteSubCategoryService(ID int64) error
	DeleteAssetItemService(ID int64) error
	//-----------------------------------------------------------------------//

	ListSearchAssetService(active int64, keyword string, status int64, limit int64) (int64, interface{}, error)
	SearchAssetByCategoryService(categoryid int64, subcategoryid int64, status int64) (interface{}, error)
	UserSearchAssetByCategoryService(categoryid int64, subcategoryid int64, status int64) (interface{}, error)
	ListByCategoryService(model *ModelCategory) (interface{}, error)
	CalPriceService(buyingdate string, percents float64, prices float64) (interface{}, error)
	CalWarrantyService(startdate string, month int) (interface{}, error)
	SearchAssetItemService(assetid int64, keyword string) (interface{}, error)
	UserSearchAssetService(active int64, keyword string, status int64, limit int64) (int64, interface{}, error)
	CountAssetItemForDashboardService(ID int64) (interface{}, error)
	AssetDamageToAssetService(ID int64, Usercode string) (interface{}, error)
	ListUserService(ID int64) (interface{}, error)
	GetUserDetailService(Usercode string) (interface{}, error)
	AddUserService(model *User) (interface{}, error)
	UpdateUserService(model *User) (interface{}, error)
	ManageUserService(Usercode string, Status int64) (interface{}, error)
	ListUserPositionService(ID int64) (interface{}, error)
	ListUserNameService(ID int64) (interface{}, error)
}
type service struct {
	repo Repository
}

// NewService creates new auth service
func NewService(repo Repository) (Service, error) {
	s := service{repo}
	return &s, nil
}

//-------------------------------------CREATE-----------------------------------------------------------------//

func (s *service) CreateAssetService(model *ModelAsset) (interface{}, error) {

	resp, err := s.repo.CreateAssetRepo(model)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
func (s *service) CreateAssetItemService(model *ModelAssetItem) (interface{}, error) {

	resp, err := s.repo.CreateAssetItemRepo(model)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
func (s *service) CreateAssetCategoryService(model *ModelCategory) (interface{}, error) {

	_, err := s.repo.CreateAssetCategoryRepo(model)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
func (s *service) CreateAssetSubCategoryService(model *ModelCategory) (interface{}, error) {

	_, err := s.repo.CreateAssetSubCategoryRepo(model)
	if err != nil {
		return nil, err
	}
	return nil, nil
}

//-------------------------------------UPDATE-----------------------------------------------------------------//
func (s *service) UpdateAssetService(model *ModelAsset) (interface{}, error) {

	_, err := s.repo.UpdateAssetRepo(model)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
func (s *service) UpdateAssetItemService(model *ModelAssetItem) (interface{}, error) {

	_, err := s.repo.UpdateAssetItemRepo(model)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
func (s *service) UpdateAssetCategoryService(model *ModelCategory) (interface{}, error) {

	_, err := s.repo.UpdateAssetCategoryRepo(model)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
func (s *service) UpdateAssetSubCategoryService(model *ModelCategory) (interface{}, error) {

	_, err := s.repo.UpdateAssetSubCategoryRepo(model)
	if err != nil {
		return nil, err
	}
	return nil, nil
}

func (s *service) UpdateAssetStatusService(model *ModelAssetItem) (interface{}, error) {

	_, err := s.repo.UpdateAssetStatusRepo(model)
	if err != nil {
		return nil, err
	}
	return nil, nil
}

//-------------------------------------LIST-----------------------------------------------------------------//

func (s *service) ListAssetService(ID int64) (interface{}, error) {

	data, err := s.repo.ListAssetRepo(ID)
	if err != nil {
		return nil, err
	}
	return data, nil
}
func (s *service) ListAssetUserService(ID int64) (interface{}, error) {

	data, err := s.repo.ListAssetUserRepo(ID)
	if err != nil {
		return nil, err
	}
	return data, nil
}
func (s *service) ListAssetDamageService(ID int64) (interface{}, error) {

	data, err := s.repo.ListAssetDamageRepo(ID)
	if err != nil {
		return nil, err
	}
	return data, nil
}
func (s *service) ListAssetItemDamageService(ID int64) (interface{}, error) {

	data, err := s.repo.ListAssetItemDamageRepo(ID)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *service) ListAssetItemService(ID int64) (interface{}, error) {

	data, err := s.repo.ListAssetItemRepo(ID)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *service) ListAvailableAssetItemIDService(ID int64) (interface{}, error) {

	data, err := s.repo.ListAvailableAssetItemIDRepo(ID)
	if err != nil {
		return nil, err
	}
	return data, nil
}
func (s *service) ListUnavailableAssetItemIDService(ID int64) (interface{}, error) {

	data, err := s.repo.ListUnavailableAssetItemIDRepo(ID)
	if err != nil {
		return nil, err
	}
	return data, nil
}
func (s *service) ListAssetLocationService(ID int64) (interface{}, error) {

	data, err := s.repo.ListAssetLocationRepo(ID)
	if err != nil {
		return nil, err
	}
	return data, nil
}
func (s *service) ListAssetCategoryService(ID int64) (interface{}, error) {

	data, err := s.repo.ListAssetCategoryRepo(ID)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *service) ListAssetSubCategoryService(ID int64) (interface{}, error) {

	data, err := s.repo.ListAssetSubCategoryRepo(ID)
	if err != nil {
		return nil, err
	}
	return data, nil
}

//--------------------------------------------------GET-----------------------------------------------------------------//

func (s *service) GETLocationIDService(ID int64) (interface{}, error) {

	data, err := s.repo.GETLocationIDRepo(ID)
	if err != nil {
		return nil, err
	}
	return data, nil
}
func (s *service) GETAssetIDService(ID int64) (interface{}, error) {

	data, err := s.repo.GETAssetIDRepo(ID)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *service) GETAssetItemIDService(ID int64) (interface{}, error) {

	data, err := s.repo.GETAssetItemIDRepo(ID)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *service) GETAssetCategoryIDService(ID int64) (interface{}, error) {

	data, err := s.repo.GETAssetCategoryIDRepo(ID)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *service) GETAssetSubCategoryIDService(ID int64) (interface{}, error) {

	data, err := s.repo.GETAssetSubCategoryIDRepo(ID)
	if err != nil {
		return nil, err
	}
	return data, nil
}

//--------------------------------------------------DELETE-----------------------------------------------------------------//

func (s *service) DeleteAssetIDService(ID int64) (interface{}, error) {

	_, err := s.repo.DeleteAssetIDRepo(ID)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
func (s *service) DeleteCategoryService(ID int64) error {

	err := s.repo.DeleteCategoryRepo(ID)
	if err != nil {
		return err
	}
	return nil
}
func (s *service) DeleteSubCategoryService(ID int64) error {

	err := s.repo.DeleteSubCategoryRepo(ID)
	if err != nil {
		return err
	}
	return nil
}
func (s *service) DeleteAssetItemService(ID int64) error {

	err := s.repo.DeleteAssetItemRepo(ID)
	if err != nil {
		return err
	}
	return nil
}

func (s *service) SearchAssetByCategoryService(categoryid int64, subcategoryid int64, status int64) (interface{}, error) {

	resp, err := s.repo.SearchAssetByCategoryRepo(categoryid, subcategoryid, status)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (s *service) UserSearchAssetByCategoryService(categoryid int64, subcategoryid int64, status int64) (interface{}, error) {

	resp, err := s.repo.UserSearchAssetByCategoryRepo(categoryid, subcategoryid, status)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (s *service) ListSearchAssetService(active int64, keyword string, status int64, limit int64) (int64, interface{}, error) {

	var search string = ""
	words := strings.Fields(keyword)
	if len(words) > 0 {
		likebarcode := ` a.barcode like '%` + words[0] + `%'`
		likeusername := ` a.name like '%` + words[0] + `%'`

		for i := range words {
			if i > 0 {
				likebarcode += ` and a.barcode like '%` + words[i] + `%'`
				likeusername += ` and a.name like '%` + words[i] + `%'`
			}
		}
		search = likebarcode + ` or ` + likeusername
	} else {
		likebarcode := ` a.barcode like '%` + keyword + `%'`
		likeusername := ` a.name like '%` + keyword + `%'`

		search = likebarcode + ` or ` + likeusername
	}
	resp, err := s.repo.ListAssetSearchRepo(active, search, limit, status)
	if err != nil {
		return 0, nil, err
	}
	len, err := s.repo.CountAssetRepo(active, search)
	if err != nil {
		return 0, nil, err
	}
	return len, resp, nil

}
func (s *service) UserSearchAssetService(active int64, keyword string, status int64, limit int64) (int64, interface{}, error) {

	var search string = ""
	words := strings.Fields(keyword)
	if len(words) > 0 {
		likebarcode := ` a.barcode like '%` + words[0] + `%'`
		likeusername := ` a.name like '%` + words[0] + `%'`

		for i := range words {
			if i > 0 {
				likebarcode += ` and a.barcode like '%` + words[i] + `%'`
				likeusername += ` and a.name like '%` + words[i] + `%'`
			}
		}
		search = likebarcode + ` or ` + likeusername
	} else {
		likebarcode := ` a.barcode like '%` + keyword + `%'`
		likeusername := ` a.name like '%` + keyword + `%'`

		search = likebarcode + ` or ` + likeusername
	}
	resp, err := s.repo.UserSearchAssetRepo(active, search, limit, status)
	if err != nil {
		return 0, nil, err
	}
	len, err := s.repo.CountAssetRepo(active, search)
	if err != nil {
		return 0, nil, err
	}
	return len, resp, nil

}

func (s *service) ListByCategoryService(model *ModelCategory) (interface{}, error) {

	resp, err := s.repo.ListByCategoryRepo(model)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
func (s *service) CalWarrantyService(startdate string, month int) (interface{}, error) {

	resp, err := s.repo.CalWarrantyRepo(startdate, month)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
func (s *service) CalPriceService(buyingdate string, percents float64, prices float64) (interface{}, error) {

	resp, err := s.repo.CalPriceRepo(buyingdate, percents, prices)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (s *service) SearchAssetItemService(assetid int64, keyword string) (interface{}, error) {

	var search string = ""
	words := strings.Fields(keyword)
	if len(words) > 0 {
		likeid := ` a.id like '%` + words[0] + `%'`
		likeserialnumber := ` a.serial_number like '%` + words[0] + `%'`

		for i := range words {
			if i > 0 {
				likeid += ` and a.id like '%` + words[i] + `%'`
				likeserialnumber += ` and a.serial_number like '%` + words[i] + `%'`
			}
		}
		search = likeid + ` or ` + likeserialnumber
	} else {
		likeid := ` a.id like '%` + keyword + `%'`
		likeserialnumber := ` a.serial_number like '%` + keyword + `%'`

		search = likeid + ` or ` + likeserialnumber
	}
	resp, err := s.repo.SearchAssetItemRepo(assetid, search)
	if err != nil {
		return nil, err
	}

	return resp, nil

}

func (s *service) CountAssetItemForDashboardService(ID int64) (interface{}, error) {

	resp, err := s.repo.CountAssetItemForDashboardRepo(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
func (s *service) ListUserService(ID int64) (interface{}, error) {

	resp, err := s.repo.ListUserRepo(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
func (s *service) AddUserService(model *User) (interface{}, error) {

	resp, err := s.repo.AddUserRepo(model)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
func (s *service) UpdateUserService(model *User) (interface{}, error) {

	resp, err := s.repo.UpdateUserRepo(model)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
func (s *service) ListUserPositionService(ID int64) (interface{}, error) {

	resp, err := s.repo.ListUserPositionRepo(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
func (s *service) ListUserNameService(ID int64) (interface{}, error) {

	resp, err := s.repo.ListUserNameRepo(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
func (s *service) GetUserDetailService(Usercode string) (interface{}, error) {

	resp, err := s.repo.GetUserDetailRepo(Usercode)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
func (s *service) AssetDamageToAssetService(ID int64, Usercode string) (interface{}, error) {

	resp, err := s.repo.AssetDamageToAssetRepo(ID, Usercode)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
func (s *service) ManageUserService(Usercode string, Status int64) (interface{}, error) {

	resp, err := s.repo.ManageUserRepo(Usercode, Status)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
