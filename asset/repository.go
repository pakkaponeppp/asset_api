package asset

//Repository asset interface
type Repository interface {
	CreateAssetRepo(model *ModelAsset) (interface{}, error)
	CreateAssetItemRepo(model *ModelAssetItem) (interface{}, error)
	CreateAssetCategoryRepo(model *ModelCategory) (interface{}, error)
	CreateAssetSubCategoryRepo(model *ModelCategory) (interface{}, error)
	//-------------------------------------------------------------------------------------------//
	UpdateAssetRepo(req *ModelAsset) (interface{}, error)
	UpdateAssetItemRepo(req *ModelAssetItem) (interface{}, error)
	UpdateAssetCategoryRepo(req *ModelCategory) (interface{}, error)
	UpdateAssetSubCategoryRepo(req *ModelCategory) (interface{}, error)
	UpdateAssetStatusRepo(req *ModelAssetItem) (interface{}, error)
	//-------------------------------------------------------------------------------------------//
	ListAssetRepo(ID int64) (interface{}, error)
	ListAssetUserRepo(ID int64) (interface{}, error)
	ListAssetDamageRepo(ID int64) (interface{}, error)
	ListAssetItemDamageRepo(ID int64) (interface{}, error)
	ListAssetItemRepo(ID int64) (interface{}, error)
	ListAvailableAssetItemIDRepo(ID int64) (interface{}, error)
	ListUnavailableAssetItemIDRepo(ID int64) (interface{}, error)
	ListAssetLocationRepo(ID int64) (interface{}, error)
	ListAssetCategoryRepo(ID int64) (interface{}, error)
	ListAssetSubCategoryRepo(ID int64) (interface{}, error)
	//-------------------------------------------------------------------------------------------//
	GETAssetIDRepo(ID int64) (interface{}, error)
	GETLocationIDRepo(ID int64) (interface{}, error)
	GETAssetItemIDRepo(ID int64) (interface{}, error)
	GETAssetCategoryIDRepo(ID int64) (interface{}, error)
	GETAssetSubCategoryIDRepo(ID int64) (interface{}, error)
	//-------------------------------------------------------------------------------------------//
	DeleteAssetIDRepo(ID int64) (interface{}, error)
	DeleteCategoryRepo(ID int64) error
	DeleteSubCategoryRepo(ID int64) error
	DeleteAssetItemRepo(ID int64) error

	ListAssetSearchRepo(active int64, search string, limit int64, status int64) (interface{}, error)
	SearchAssetByCategoryRepo(categoryid int64, subcategoryid int64, status int64) (interface{}, error)
	CountAssetItemForDashboardRepo(ID int64) (interface{}, error)
	ListUserRepo(ID int64) (interface{}, error)
	AddUserRepo(model *User) (interface{}, error)
	UpdateUserRepo(model *User) (interface{}, error)
	ListUserPositionRepo(ID int64) (interface{}, error)
	ListUserNameRepo(ID int64) (interface{}, error)
	GetUserDetailRepo(Usercode string) (interface{}, error)
	AssetDamageToAssetRepo(ID int64, Usercode string) (interface{}, error)
	ManageUserRepo(Usercode string, Status int64) (interface{}, error)
	UserSearchAssetByCategoryRepo(categoryid int64, subcategoryid int64, status int64) (interface{}, error)
	CountAssetRepo(status int64, search string) (int64, error)
	ListByCategoryRepo(model *ModelCategory) (interface{}, error)
	CalWarrantyRepo(startdate string, month int) (interface{}, error)
	CalPriceRepo(buyingdate string, percents float64, prices float64) (interface{}, error)
	SearchAssetItemRepo(assetid int64, keyword string) (interface{}, error)
	UserSearchAssetRepo(active int64, keyword string, status int64, limit int64) (interface{}, error)
}
